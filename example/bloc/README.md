# GEANT 3.21 example

![Movie](../../doc/movie.gif)

This is based on the original
[example](https://github.com/vmc-project/geant3/blob/master/examples/gexam1.F)
from the GEANT 3.21 distribution (see also
[here](https://vmc-project.github.io/geant.pdf#page=25) and
[here](http://ftp.riken.go.jp/cernlib/download/2005_source/src/geant321/examples/gexam1/)).

The simulated detector is an electromagnetic calorimeter.  

- At the top is the volume `ECAL` build by
  [`ECalBuilder`](ECalBuilder.cxx). 
  
- Inside that volume are two `LEAK` volumes, built by
  [`LeakBuilder`](LeakBuilder.cxx).  These are volumes of air that
  will contain the absorbers 
  
- Also in the `ECAL` value is the `LATR` volume
  ([`LatrBuilder`](LatrBuilder.cxx)) - again an air volume that will
  contain the tracker. 
  
- The volume `BLOC` consist of lead glass that and is further
  subdivided into types (`RTUB`) and rings (`RING`).  This is the
  active detector part build by [`BlocBuilder`](BlocBuilder.cxx) and
  the hits are recorded by [`BlocSimulator`](BlocSimulator.cxx). 
  
  
## To build 

	make -f bloc.mk
	
	
## To run 

	root Simulate.C
	
	
