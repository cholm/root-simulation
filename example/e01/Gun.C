//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/e01/Gun.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:02 2004
    @brief   Example event generator script  */
#ifndef __GUN_C__
#define __GUN_C__
#include <TGenerator.h>
#include <TClonesArray.h>
#include <TParticle.h>

//____________________________________________________________________
class Gun : public TGenerator
{
public:
  Gun() {}
  Int_t ImportParticles(TClonesArray* a, Option_t* option="")
  {
    a->Clear();
    
    //                      pdg   -- hierarchy -  -- momentum --  -- pol ---
    new ((*a)[0]) TParticle(0, 0, -1, -1, -1, -1, 1, 0,   0,   1, 0, 0, 0, 0);
    new ((*a)[1]) TParticle(0, 0, -1, -1, -1, -1, 1, 0.1, 0,   1, 0, 0, 0, 0);
    new ((*a)[2]) TParticle(0, 0, -1, -1, -1, -1, 1, 0,   0.1, 1, 0, 0, 0, 0);
    
    return 3;
  }
  TObjArray* ImportParticles(Option_t* option="")
  {
    ImportParticles(static_cast<TClonesArray*>(fParticles), option);
    return fParticles;
  }
};

#endif
//
// EOF
//


  
