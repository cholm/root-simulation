// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: BlocBuilder.h,v 1.1 2005-12-13 18:56:08 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/BlocBuilder.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Example::BLOC
*/
#ifndef Example_BlocBuilder
#define Example_BlocBuilder
#include <framework/Task.h>

namespace Example 
{
  /** A cylinder - 20 radiation lengths long, 10 radiation lengths in
      diameter.  The volume is further divided into fNR radial
      segments (concentric rings) and fNL longitudinal segments
      (tubes). 

      Note that this is _not_ a Simulation::Task but a
      Framework::Task, which is why we need to define `Exec`.
  */ 
  class BlocBuilder : public Framework::Task 
  {
  private:
    /** Number of radial segments (tubes) */
    Int_t  fNR;
    /** Number of longitudinal segments (rings) */
    Int_t  fNL;
  public:
    /** Construct a BLOC task 
	@param nr Number of radial segments (tubes)
	@param nl 
	@param useBgo If false use lead glass else use BGO as tracking
	medium  */
    BlocBuilder(Int_t nr=20, Int_t nl=20);
    /** @return  Number of radial segments (tubes) */
    Int_t GetNR() const { return fNR; }
    /** @return  Number of longitudinal segments (rings) */
    Int_t GetNL() const { return fNL; }
    /** Initialize the task.
	@param option Not used. */
    void Initialize(Option_t* option="");
    /** Register the task.
	@param option Not used. */
    void Register(Option_t* option="");
    void Exec(Option_t* option="") {}
    ClassDef(BlocBuilder,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
