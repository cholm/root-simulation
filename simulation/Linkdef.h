// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: Linkdef.h,v 1.12 2007-08-05 14:26:35 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Linkdef.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:24:27 2004
    @brief   Link specification for CINT
*/
#if !defined(__CINT__) and !defined(CLING)
#error Not for compilation
#endif

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace Simulation;
#pragma link C++ class     Simulation::Main+;
#pragma link C++ class     Simulation::Task+;
#pragma link C++ class     Simulation::Generator+;
#pragma link C++ class     Simulation::Stack+;
#pragma link C++ class     Simulation::Application+;
#pragma link C++ class     Simulation::TreeWriter+;
#pragma link C++ class     Simulation::TreeReader+;
#pragma link C++ class     Simulation::TrackSaver+;
#pragma link C++ class     Simulation::TrackReader+;
#pragma link C++ class     Simulation::Display+;
#pragma link C++ class     Simulation::ToggleButton-;
#pragma link C++ class     Simulation::Hit+;
#pragma link C++ class     Simulation::DefaultHit+;
#pragma link C++ class     Simulation::DefaultHitReader+;
#pragma link C++ class     Simulation::Single+;
#pragma link C++ class     Simulation::Fixed+;
#pragma link C++ class     Simulation::Cocktail+;
#pragma link C++ class     Simulation::Field+;
#pragma link C++ function  Simulation::GetDisplay();
//#pragma link C++ namespace Simulation::Util;
//#pragma link C++ class     Simulation::Util::MakeTask+;


//____________________________________________________________________ 
//  
// EOF
//
