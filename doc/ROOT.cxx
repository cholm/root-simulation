//
// : docroot,v 1.1 2004/12/16 01:24:23 cholm Exp $
//
//   ROOT generic simulation framework
//   Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License
//   as published by the Free Software Foundation; either version 2 of
//   the License, or (at your option) any later version.
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details.
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
//   MA 02111-1307  USA
//
//
/** @file   doc/ROOT.cxx
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  ROOT class documentation. */
/** @defgroup ROOT ROOT classes referenced */
/** @class TAttMarker ROOT.cxx <TAttMarker.h>
    @ingroup ROOT
    @brief ROOT class TAttMarker
    See also <a href='http://root.cern.ch/root/htmldoc/TAttMarker.html'>Documentation of TAttMarker</a>
 */
class TAttMarker {};

/** @class TButton ROOT.cxx <TButton.h>
    @ingroup ROOT
    @brief ROOT class TButton
    See also <a href='http://root.cern.ch/root/htmldoc/TButton.html'>Documentation of TButton</a>
 */
class TButton : public TObject {};


/** @class TGenerator ROOT.cxx <TGenerator.h>
    @ingroup ROOT
    @brief ROOT class TGenerator
    See also <a href='http://root.cern.ch/root/htmldoc/TGenerator.html'>Documentation of TGenerator</a>
 */
class TGenerator : public TNamed {};


/*  @class TObject ROOT.cxx <TObject.h>
    @ingroup ROOT
    @brief ROOT class TObject
    See also <a href='http://root.cern.ch/root/htmldoc/TObject.html'>Documentation of TObject</a>
 */
/** @class TVirtualMCApplication ROOT.cxx <TVirtualMCApplication.h>
    @ingroup ROOT
    @brief ROOT class TVirtualMCApplication
    See also <a href='http://root.cern.ch/root/htmldoc/TVirtualMCApplication.html'>Documentation of TVirtualMCApplication</a>
 */
class TVirtualMCApplication : public TNamed {};

/** @class TVirtualMCStack ROOT.cxx <TVirtualMCStack.h>
    @ingroup ROOT
    @brief ROOT class TVirtualMCStack
    See also <a href='http://root.cern.ch/root/htmldoc/TVirtualMCStack.html'>Documentation of TVirtualMCStack</a>
 */
class TVirtualMCStack : public TNamed {};

// EOF
