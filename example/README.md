# Examples 

## Content

- [`e01`][e01]
- [`e02`][e02]
- [`e03`][e03]
- [`muon`](muon) A simple example of a muon life-time measurement.
  The detector geometry and simulation tasks are defined as a single
  class which is compiled on-demand by ROOT's AcLic 
- [`bloc`](bloc) A more complicated example.  Geometry is built by
  dedicated classes and the simulation is done in a separate class.
  There is also an example of post processing the data generated in
  the simulation. 
  
  


