//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/e02/Config.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:02 2004
    @brief   Example configuration script  */
//____________________________________________________________________
void Config(Bool_t g3=true) 
{
  Simulation::Main*       main      = new Simulation::Main("E02", "E02");
  Simulation::Generator*  generator = new Simulation::Generator("Gen","");
  E02*                    dut       = new E02;
  Simulation::Display*    display   = new Simulation::Display;
  Simulation::TrackSaver* stack     = new Simulation::TrackSaver;
  Simulation::TreeWriter* writer    = new Simulation::TreeWriter("Hits", 
								 "hits.root");

  // Make generator
  generator->SetGenerator(new Gun());
  generator->SetVertex(0, 0, -dut->FullWorldLength()/2);

  // Magnetic field
  main->SetField(new Simulation::Field());
  
  // Define MC
  main->SetVMCConfig(g3 ?"G3Config.C" : "G4Config.C");
  
  // Add to main task 
  main->Add(generator);
  main->Add(dut);
  main->Add(display);
  main->Add(stack);
  main->Add(writer);
  
  if (!gROOT->IsBatch()) {
    TBrowser* b = new TBrowser("b", "Browser");
    Printf("\nSetup complete.\n\nUse context menu to execute simulation, "
	   "or type\n\n\tSimulation::Main::Instance()->Loop();\n\n"
	   "to generate one event");
    main->GetApplication()->GetStack()->SetKeepIntermediate(kTRUE);
  }
  else 
    main->GetApplication()->GetStack()->SetKeepIntermediate(kFALSE);
}


//____________________________________________________________________
//
// EOF
//
