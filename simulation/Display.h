// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/Display.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Simulation::Display
*/
#ifndef Simulation_Display
#define Simulation_Display
#include <simulation/Task.h>
#include <TButton.h>
#include <TList.h>
#ifdef G__DICTIONARY
#include <TCanvas.h>
#include <TPad.h>
#include <TSlider.h>
#include <TVirtualGeoTrack.h>
#include <TF1.h>
#endif
class TCanvas;
class TPad;
class TSlider;
class TVirtualGeoTrack;
class TF1;
class TPaveLabel;
class TLatex;

namespace Simulation 
{
  class Display;
    
  //------------------------------------------------------------------
  /** @class ToggleButton simulation/Display.h <simulation/Display.h>
      @ingroup simspecial_tasks
      @brief   A toggle button
  */
  class ToggleButton : public TButton
  {
  public:
    ToggleButton(const char* text,
		 Bool_t      down,
		 Double_t    llx,
		 Double_t    lly,
		 Double_t    urx,
		 Double_t    ury,
		 Display*    display,
		 Option_t*   option);
    void SetColor();
    void Clicked();
    Display* fDisplay;
    Bool_t   fIsDown;
    TString  fToggleOption;
  };
  //------------------------------------------------------------------
  /** @class Display simulation/Display.h <simulation/Display.h>
      @ingroup sim_special_tasks
      @brief Event display
    
      @image html display.png 
  */
  class Display : public Framework::Task // Simulation::Task
  {
  public:
    /** Bits to enable various buttons */
    enum EButtons {
      /** The `Draw Hits' button */
      kHits = 1,
      /** The `Draw Tracks' button */
      kTracks,
      /** The `Draw Hits & Tracks' button */
      kHitsTracks,
      /** The `Animate' button */
      kAnimate,
      /** The `Pan' button */
      kPan,
      /** The `Record' button */
      kRecord
    };
    
    /** Constructor 
	@param name    Name of display. 
	@param option  The drawing options. See SetOption */
    Display(const Char_t* name="display", 
	    const Char_t* option="Event display");
    /** Destructor */
    virtual ~Display();
    
    /** @return Get static instance */
    static Simulation::Display* Instance();
    /** Register with manager 
	@param option Not used. */
    virtual void Register(Option_t* option="");
    /** Plot one event
	@param option Which step we're in. */
    virtual void Exec(Option_t* option="");
    /** Toggle an option */
    virtual void ToggleOption(Option_t* option, Bool_t on);
    /** @param option Draw option to use  
	This can be a combinaion of the following: 
	<ul>
	<li> @c "A"  Animate tracks </li>
	<li> @c "P"  Pan and animate tracks </li>
	<li> @c "R"  Record pan and animate tracks to files (one per
	frame) - this can be joined to form an animated gif, or the
	like. </li>
	<li> @c "T"  Draw tracks </li>
	<li> @c "H"  Draw hits </li>
	</ul> */
    virtual void SetOption(Option_t* option="A") { fOption = option; } //*MENU*
    /** Draw current event 
	@param option What to draw - See SetOption. */
    virtual void Draw(Option_t* option="AHT");
    /** Draw the scene again with same options as before, for example
	after changing slider value. */
    virtual void DrawAgain();
    /** Continue to end of event. */
    virtual void Cont(); //*MENU*
    /** Enable a button 
	@param b Which button to enable - one of the enums in EButtons */
    virtual void EnableButton(EButtons b)  { SETBIT(fEnabled,b); }
    /** Disable a button 
	@param b Which button to disable - one of the enums in EButtons */
    virtual void DisableButton(EButtons b) { CLRBIT(fEnabled,b); }
    /** Set the width of the canvas (the height is automatically
	calculated 
	@param w Width in pixels */
    virtual void SetWidth(Int_t w) { fWidth = w; }
    /** Set the time interval displayed when drawing or animating tracks
	@param min Minimum time
	@param max Maximum time. */
    virtual void SetT(Double_t min=0, Double_t max=2e-8);
    /** Set the time scale function The function must evaluate x
	between 0 and 1 and map to the range 0 to 1 monotonically.
	This is not checked. The default is @f$x^2@f$.
	@param expr Expression. */
    virtual void SetTScale(const char* expr);
    /** Set the time scale function.  The function must evaluate x
	between 0 and 1 and map to the range 0 to 1 monotonically.
	This is not checked. The default is @f$x^2@f$.
	@param f Function. */
    virtual void SetTScale(const TF1& expr);
    /** Sett how many frames to use in the animations 
	@param nFrames Number of frames */
    virtual void SetNFrames(Int_t nFrames=100) { fNFrames = nFrames; }
    /** Set the zoom factor 
	@param n The zoom factor. */
    virtual void SetZoomFactor(Double_t n)  { fZoomFactor = n; }
    /** Zoom in or out.  Zoom step is 1.25
	@param d If negative zoom out, otherwise zoom in.  */
    virtual void Zoom(Int_t d);
    /** Move the object in the canvas.   
	@param d Direction.  One of 
	<ul>
	  <li> @c 'l' Left </li>
	  <li> @c 'i' Up </li>
	  <li> @c 'u' Down </li>
	  <li> @c 'h' Right </li>
	</ul> */
    virtual void Move(char d);
    /** Idle for specified amount of time to let canvas update before
	going on to the next frame of an animation */
    virtual void Idle();

    /** Draw a man at the given position.  Good for giving a scale of
	a geometry. 

	@image html man.png 
	@image html mangl.png 

	@param x X coordinate 
	@param y y-coordinate of the underside of the feet - the
	y-coordinate of the plane the man is standing on. 
	@param z Z coordinate
	@param color Color of the man. */
    virtual void DrawMan(Double_t x=0, Double_t y=0, 
			 Double_t z=-90, Int_t color=0) const; //*MENU*
    /** Execute an event corresponding to some mouse event 
	@param event The event code 
	@param px    X-coordinate where, in pixels, the even occurred
	@param py    Y-coordinate where, in pixels, the even occurred */
    virtual void  ExecuteEvent(Int_t event, Int_t px, Int_t py);
    /** Distance from this to cursor. 
	@param px X-coordinate where, in pixels, to measure to
	@param py Y-coordinate where, in pixels, to measure to
	@return Distance in pixels(?) */
    virtual Int_t DistancetoPrimitive(Int_t px, Int_t py);
    /** Add something to draw.  The string is interpreted by ROOT

	@param cmd  String to be executed
    */
    virtual void AddDraw(const char* cmd); //*MENU*
    /** Add something to draw. The object is drawn.

	@param o Object to draw
    */
    virtual void AddDraw(TObject* o);
  protected:
    /** Singleton */
    static Display* fgInstance;
    /** Canvas to draw in */
    TCanvas*      fCanvas;
    /** 3D view of event */
    TPad*         f3DView;
    /** Event number */
    TPaveLabel*   fLabel;
    /** Time */
    TLatex*       fTime;
    /** Bit mask of buttons to draw */
    Int_t        fEnabled;
    /** Button */
    TButton*      fContinue;
    /** Button */
    TButton*      fZoom;
    /** Button */
    TButton*      fUnZoom;
    /** Button */
    TButton*      fLeft;
    /** Button */
    TButton*      fRight;
    /** Button */
    TButton*      fUp;
    /** Button */
    TButton*      fDown;
    /** Button */
    ToggleButton* fDrawHits;
    /** Button */
    ToggleButton* fDrawTracks;
    /** Button */
    ToggleButton* fAnimate;
    /** Button */
    ToggleButton* fPan;
    /** Button */
    ToggleButton* fRecord;
    /** Flag when witing for user */
    Bool_t        fWait;
    /** Flag that we're doing the first draw */
    Bool_t        fFirst;
    /** Width of the canvas */
    Int_t         fWidth;
    /** The current minimum time */
    Double_t      fMinT;
    /** The current maximum time */
    Double_t      fMaxT;
    /** The number of frames to use */
    Int_t         fNFrames;
    /** Time slider */
    TSlider*      fSlider;
    /** Zoom factor */
    Double_t      fZoomFactor;
    /** Current latitude - phi */ 
    Double_t      fLatitude;
    /** Current longitude - theta */
    Double_t      fLongitude;
    /** Current roll - psi */ 
    Double_t      fPsi;
    /** Current `event' */ 
    Int_t         fEventNo;
    /** time Scaling function */ 
    TF1*          fTScale;
    /** Whether we're in Zoom mode */
    Bool_t        fZoomMode;
    /** X at lower left corner or range */
    Double_t      fX0;
    /** Y at lower left corner or range */
    Double_t      fY0;
    /** X at upper right corner or range */
    Double_t      fX1;
    /** Y at upper right corner or range */
    Double_t      fY1;
    /** X pixel of mark */
    Int_t         fXPixel;
    /** Y pixel of mark */
    Int_t         fYPixel;
    /** Old x pixel of mark */
    Int_t         fOldXPixel;
    /** Old y pixel of mark */
    Int_t         fOldYPixel;
    /** Whether we're drawing a box */
    Bool_t        fLineDrawn;
    /** Extra things to draw */
    TList         fExtraDraw;
    /** Extra things to draw */
    TList         fMarkers;
    
    /** Draw hits in a folder */
    virtual void DrawFolders();
    /** Draw hits in a folder 
	@param folder the folder to draw */
    virtual void DrawFolder(TFolder* folder);
    /** Draw a hit
	@param hit the hit to draw */
    virtual void DrawHit(TObject* hit);

    /** Setup the canvas if not alrady done so. */
    virtual void SetupCanvas();
    /** Setup the navigation button
	@param n The total number of buttons to display - including
	navigation buttons and action buttons */
    virtual void SetupNavigation(Int_t n);
    /** Setup the action buttons. 
	@param n The total number of buttons to display - including
	navigation buttons and action buttons
	@param m Number of buttons already drawn */
    virtual void SetupActions(Int_t n, Int_t m);
    /** Setup the time slider */
    virtual void SetupSlider();
    /** Get the current event number */
    virtual Int_t GetEventNo();
    /** Find the min/max time */
    virtual void FindT();
    /** Find the min/max time of the tracks */
    virtual void FindTTracks();
    /** Find the max/min time of track, and it's daughters
	@param track Track to investigate */
    virtual void FindT(TVirtualGeoTrack* track);
    /** Find the min/max time in all folders */
    virtual void FindTFolders();
    /** Find the min/max time of hits */
    virtual void FindT(TFolder* folder);
    /** Find the min/max time of hits */
    virtual void FindT(TObject* hit);
    /** Draw extra stuff */
    virtual void DrawExtra();
    
    ClassDef(Display,1);  
  };

  //------------------------------------------------------------------
  extern Display* GetDisplay();
}

#endif
  
