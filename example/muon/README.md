# Muon lifetime measurement 

This is a simple simulation of a muon lifetime measurement. 

It consist of 5 volumes in total 

- 2 scintilator slats on top 

- 1 aluminium slat in the middle 

- 2 scintilator slats on the bottom 

The scintilators are active, meaning we record hits in these.  The
idea is then to trigger on signals in either top or bottom pairs of
scintilators and then measure the time until a second coincidence in
either the top of bottom layer.  This is more or less what the
analysis class `Analyser` does (see below). 

The detector is defined in the class [`Muon`](Muon.C).  The
configuration of the simulation is in [`Config.C`](Config.C). The
configuration creates 

- the main steering object `Simulation::Main` 
- the event generator object `Simulation::Generator` and
  `Simulation::Fixed`. 
- the detector `Muon` 
- an event display `Simulation::Display` 
- a `Simulation::TrackSaver` task to save tracks to the output file,
  and 
- a `Simulation::TreeWriter` task to save the output to a `TTree`

The event generator `Simulation::Fixed` is configured to shoot
$`\mu^{-}`$ particles with a momentum in the range
$`[0.1,0.3]\mathrm{GeV/c}`$ and 100 particles per event. 

We add a `TLatex` object to be drawn in the event display. 

## To run 

The script [`Run.C`] can be used to run the simulation.  It AcLIC
compiles [`Muon.C`](Muon.C) and loads it, loads the configuration
[`Config.C`](Config.C) and then runs the simulation.

1000 events with GEANT 3.21 and no visualisation 

	root -l Run.C\(1000\)
	
10 events with GEANT 3.21 and event display 

	root -l Run.C\(10,1,1\)

1 event with GEANT 4 and no event display 

	root -l Run.C\(1,0\)

## Output 

Hits are written to `hits.root`. That contains a `TTree` with tracks
and hits from each event.   Both are stored in `TClonesArray`s.  The
tracks are `TParticle` objects, and the hits are
`Simulation::DefaultHits` objects.  These can conveniently be read by
the tasks `Simulation::TrackReader` and
`Simulation::DefaultHitReader`, respectively.

## Animation 

- Start an interactive run 

  	  root -l Run.C\(10,1,1\) 
	
- When find an event you like, adjust the time slider at the bottom to
  show the time period you are interested in.
- Then click *Animate* to ensure that the animation is OK.  If not,
  adjust the time slider appropriately. 
- Then click *Record*.  This will generate 100 GIFs in the working
  directory. 
- Open [Gimp](https://gimp.org)
- From the *File* menu select *Open as layers...* and select all the
  GIFs. 
- Once read in - which takes a little time - convert to RGB by
  selecting *Image->Mode->RGB* 
- The layers are in reverse order, so select *Layer->Stack->Reverse
  layer order* 
- Now do *File->Export as...* and put in either a GIF or Web Picture
  filename (ending in `.gif` or `.webp` respectively).  
- In the options dialog, select *As Animation* and adjust other
  options to your liking (for example $`100 \mu s`$ between frame),
  and then clikc *OK*
- Once this finishes - which does take a little time - you have your
  animation. 
  

## Analysis 

The class [`Analyser`](Analyser.C) reads in the hits and tracks
generated in the simulation and then looks for hits from electrons (or
positrons).  It then finds the parent muon hit and calculates the time
difference between the two hits.  This difference is histogrammed and
at the end of the analysis job, the life-time of the muon is extracted
by fitting the time-difference distribution.   The result is written
to the file `analysis.root`.

The script [`Analysis.C`](Analysis.C) configures the analysis job.  It
creates 

- the main `Framework::Main` steering object 
- a `Framework::TreeReader` task to read in the tree from file 
- a `Simulation::TrackReader` to read in the tracks from the tree and
  expose them in a `TFolder` 
- a `Simulation::DefaultHitReader` to read in the hits and expose them
  in a `TFolder`. 
- our `Analyser` object which get the information read in from the
  above tasks and then does the analysis. 
  
The script [`Analyse.C`](Analyse.C) can be used to run the analysis.
For example 

	root -l Analyse.C\(-1,1\)
	
to read all events (should be around 1000) and display the final
result.  To run in batch mode, do 

	root -l -b Analyse.C\(-1\)
	
To limit the analysis to 100 events, do 

	root -l -b Analyse.C\(100\)
	






