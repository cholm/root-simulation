//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/Cocktail.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Cocktail
*/
#include "simulation/Cocktail.h"
#include <TClonesArray.h>
#include <TVector3.h>
#include <TParticle.h>
#include <TParticlePDG.h>
#include <TDatabasePDG.h>
#include <TBrowser.h>
#include <iostream>
#define DEGRAD TMath::Pi() / 180.

//____________________________________________________________________
ClassImp(Simulation::Cocktail);

//____________________________________________________________________
Simulation::Cocktail::Cocktail() 
  : TGenerator("cocktail", "A simple cocktail particle generator")
{
  fParticles = new TClonesArray("TParticle");
}

//____________________________________________________________________
void
Simulation::Cocktail::Add(TGenerator* gen) 
{
  fGenerators.Add(gen);
  fStacks.Add(new TClonesArray("TParticle"));
}

//____________________________________________________________________
Int_t
Simulation::Cocktail::ImportParticles(TClonesArray* a, Option_t* option) 
{
  a->Clear();
  Int_t nTotal = 0;
  Int_t nGen   = fGenerators.GetEntriesFast();
  for (Int_t i = 0; i < nGen; i++) {
    TGenerator*   generator = static_cast<TGenerator*>(fGenerators.At(i));
    TClonesArray* stack     = static_cast<TClonesArray*>(fStacks.At(i));
    if (!stack || !generator) continue;
    generator->ImportParticles(stack, option);
    Int_t nparticles = stack->GetEntriesFast();
    for (Int_t j = 0; j < nparticles; j++) {
      TParticle* p = static_cast<TParticle*>(stack->At(j));
      if  (!p) continue;
      new ((*a)[nTotal]) TParticle(*p);
      nTotal++;
    }
  }
  return nTotal;
}


//____________________________________________________________________
TObjArray* 
Simulation::Cocktail::ImportParticles(Option_t* option) 
{
  ImportParticles(static_cast<TClonesArray*>(fParticles), option);
  return fParticles;
}


//____________________________________________________________________
void
Simulation::Cocktail::Browse(TBrowser* b) 
{
  b->Add(&fGenerators, "Generators");
  b->Add(&fStacks, "Stacks");
}

//____________________________________________________________________
void
Simulation::Cocktail::Print(Option_t* option) const
{
  std::cout << "Cocktail of generators: " << std::endl;
  for (Int_t i = 0; i < fGenerators.GetEntries(); i++) {
    TGenerator* gen = static_cast<TGenerator*>(fGenerators.At(i));
    if (!gen) continue;
    gen->Print(option);
  }
}

    
//____________________________________________________________________
//
// EOF
//
