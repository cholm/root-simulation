//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Generator.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Simulation::Generator
*/
#include "simulation/Generator.h"
#include <TGenerator.h>
#include <TVirtualMC.h>
#include <TVirtualMCStack.h>
#include <TVector3.h>
#include <TClonesArray.h>
#include <TParticle.h>
#include <TLorentzVector.h>
#include <TBrowser.h>

//____________________________________________________________________
ClassImp(Simulation::Generator);


//____________________________________________________________________
Simulation::Generator::Generator() 
  : Task("Generator", "Generator"), 
    fGenerator(0),
    fV(0,0,0,0),
    fSigmaV(0,0,0,0)
{}


//____________________________________________________________________
Simulation::Generator::Generator(const Char_t* name, const Char_t* title) 
  : Task(name, title), 
    fGenerator(0),
    fV(0,0,0,0),
    fSigmaV(0,0,0,0)
{
  fCache = new TClonesArray("TParticle");
}


//____________________________________________________________________
Simulation::Generator::Generator(const Char_t* name, TGenerator* g) 
  : Task(name, (g ? g->GetTitle() : "")), 
    fGenerator(g),
    fV(0,0,0,0),
    fSigmaV(0,0,0,0)
{
  fCache = new TClonesArray("TParticle");
}

//____________________________________________________________________
void
Simulation::Generator::Browse(TBrowser* b)
{
  Simulation::Task::Browse(b);
  if (fGenerator)   b->Add(fGenerator);
  b->Add(&fV);
  b->Add(&fSigmaV);
}
//____________________________________________________________________
void
Simulation::Generator::SetVertex(Double_t x, Double_t y,  
				 Double_t z, Double_t t)
{
  fV.SetXYZT(x,y,z,t);
}
//____________________________________________________________________
void
Simulation::Generator::SetVertexSigma(Double_t x, Double_t y,  
				      Double_t z, Double_t t)
{
  fSigmaV.SetXYZT(x,y,z,t);
}

//____________________________________________________________________
void
Simulation::Generator::MakeVertex(TLorentzVector& v)
{
  Double_t x = gRandom->Gaus(fV.X(),fSigmaV.X());
  Double_t y = gRandom->Gaus(fV.Y(),fSigmaV.Y());
  Double_t z = gRandom->Gaus(fV.Z(),fSigmaV.Z());
  Double_t t = gRandom->Gaus(fV.T(),fSigmaV.T());
  v.SetXYZT(x,y,z,t);
}
  
//____________________________________________________________________
void
Simulation::Generator::GeneratePrimaries() 
{
  Debug(16, "GeneratePrimaries", "Generating primaries");
  if (!fGenerator) {
    Error("GeneratePrimaries", "No TGenerator set in %s", GetName());
    return;
  }
  TVirtualMC* mc = TVirtualMC::GetMC();
  if (!mc) {
    Error("GeneratePrimaries", "No TVirtalMC defined");
    return;
  }
  TVirtualMCStack* mcStack = mc->GetStack();
  if (!mcStack) {
    Error("GeneratePrimaries", "No TVirtalMCStack defined in %s", 
	  mc->GetName());
    return;
  }
  TClonesArray* array = static_cast<TClonesArray*>(fCache);
  fGenerator->ImportParticles(array, "final");
  TLorentzVector v;
  MakeVertex(v);
  
  Int_t n = array->GetEntriesFast();
  Debug(16, "GeneratePrimaries", "Generated primaries: %d",n);
  for (Int_t i = 0; i  < n; i++) {
    TParticle* particle = static_cast<TParticle*>(array->At(i));
    TVector3 pol;
    particle->GetPolarisation(pol);
    Int_t ntr;
    mcStack->PushTrack(kTRUE, -1, particle->GetPdgCode(), 
		       particle->Px(), particle->Py(), 
		       particle->Pz(), particle->Energy(), 
		       particle->Vx() + v.X(), 
		       particle->Vy() + v.Y(), 
		       particle->Vz() + v.Z(), 
		       particle->T()  + v.T(), 
		       pol.X(), pol.Y(), pol.Z(), kPPrimary, ntr, 
		       particle->GetWeight(), 0);
  }
  Verbose(5, "GeneratePrimaries", "Generated %d primaries", n);
}

//____________________________________________________________________
//
// EOF
//
