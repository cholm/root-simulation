//____________________________________________________________________ 
//  
// $Id: BlocBuilder.cxx,v 1.2 2007-08-05 14:26:34 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/BlocBuilder.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Example::BlocBuilder
*/
#include "BlocBuilder.h"
#include <TGeoVolume.h>
#include <TGeoTube.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TTree.h>
#include <TLorentzVector.h>
#include <TVirtualMC.h>
#include <TVirtualMC.h>
#include <TFolder.h>
#include <TClonesArray.h>

//____________________________________________________________________
ClassImp(Example::BlocBuilder);

//____________________________________________________________________
Example::BlocBuilder::BlocBuilder(Int_t nr, Int_t nl) 
  : Framework::Task("BlocBuilder", "Time Projection Chamber"), 
    fNR(nr), 
    fNL(nl)
{
  fCache = new TClonesArray("Example::Hit");
}

//____________________________________________________________________
void
Example::BlocBuilder::Initialize(Option_t* option) 
{
  Debug(14, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);

  TGeoMedium*   medium     = gGeoManager->GetMedium("Absorber");
  TGeoMaterial* material   = medium->GetMaterial();
  Double_t      radl       = material->GetRadLen();
  TGeoTube*     blocShape  = new TGeoTube("BLOC", 0, 20 * radl / 4, 10 * radl);
  TGeoVolume*   blocVolume = new TGeoVolume("BLOC", blocShape, medium);  
  TGeoVolume*   rtubVolume = blocVolume->Divide("RTUB", 1, fNR, 0, 0);
  TGeoVolume*   ringVolume = rtubVolume->Divide("RING", 3, fNL, 0, 0);
  TGeoVolume*   top        = gGeoManager->GetTopVolume();
  top->AddNode(blocVolume, 0, 0);
}

//____________________________________________________________________
void
Example::BlocBuilder::Register(Option_t* option) 
{
  Debug(14, "Register", "Setting up volume");
  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits"))) 
    fBranch = tree->Branch(GetName(), &fCache);

}
//____________________________________________________________________
//
// EOF
//
