// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Main.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Simulation::Main
*/
#ifndef Simulation_Main
#define Simulation_Main
#include "framework/Main.h"
#include "simulation/Task.h"
#include "simulation/Application.h"

class TBrowser;

/** @namespace Simulation 
    @brief Namespace for all simulation framework classes */
namespace Simulation 
{
  class Application;
  /** @defgroup sim_steer Steering classes. 
      @brief Code in this group deals with steering of the application */ 
  
  /** @class Main simulation/Main.h <simulation/Main.h>
      @ingroup sim_steer
      @brief Main task used in simulation jobs 

      Users @e must instantize an object of this class before using
      the simulation framework.  
   */
  class Main : public Framework::Main 
  {
  public:
    enum {
      kGeant3,
      kGeant4TGeo,
      kGeant4Native
    };
    /** Make an new main task   */
    Main();
    /** Make an new main task   
	@param name The name of the main task 
	@param title The title of the main task */
    Main(const Char_t* name, const Char_t* title);
    /** Destructor  */
    virtual ~Main() {}
    /** Get the singleton object of this class 
	@return the singleton object of this class  */
    static Simulation::Main* Instance();
    

    /** Get the application 
	@return  The application */
    Application* GetApplication() const { return fApplication; }

    /** Initialize the simulation 
	@param option Option to pass to tasks */
    void ExecInitialize(Option_t* option=""); //*MENU*
    /** Make one event */ 
    void Event(); //*MENU*
    /** Make one run
	@param n the number of events to make */ 
    void Run(Int_t n=1); //*MENU*
    /** Make one trigger.  If ExecInitialize hasn't been called yet,
	then do so.  Note, that you need to terminate the job with
	ExecFinish. */
    void Trigger(); // *MENU*

    void Draw(Option_t* option="")
    {
      Draw(option, 30, 30, 0, 10, 10, 0.01, 0.01);
    } //*MENU*
    /** Draw the geometry. If ExecInitialize hasn't been called yet,
	then do so.  
	@param option Drawing option, or volume name. 
	@param theta  The @f$ \theta@f$ viewing angle. 
	@param phi    The @f$ \phi@f$ viewing angle. 
	@param psi    The @f$ \psi@f$ viewing angle. 
	@param u0     X-coordinate of center point in canvas. 
	@param v0     Y-coordinate of center point in canvas. 
	@param ul     Canvas X scale
	@param vl     Canvas Y scale */
    void Draw(Option_t* option,
	      Double_t  theta,
	      Double_t  phi, 
	      Double_t  psi,
	      Double_t  u0,
	      Double_t  v0, 
	      Double_t  ul,
	      Double_t  vl); //*MENU*
    
    /** Run the simulation.   This corresponds to first sending the
	message ExecInitialize, followed by the message Run, and
	finally sending the message ExecFinish. 
	@param n The max number of events to make 
	@return 0 on success, error code otherwise  */
    virtual Int_t Loop(Int_t n=1); //*MENU*
    /** Print information 
	@param option Print options.  */
    virtual void  Print(Option_t* option="M") const; //*MENU*
    /** Browse the object.
	@param b Browser to use  */
    virtual void  Browse(TBrowser* b);
    /** This member function is called each time the MC backend needs
	to know the magnetic field at a given position.  This member
	function is only called if there's no task for the current
	tracking medium or if the task delegates to this object. The
	argument @a x holds the current space point for which the
	magnetic field is needed.  The magnetic field should be
	returned in @a b in units of kilo Gaus (@f$ 10^4 \mbox{gauss}
	= 1 \mbox{T}@f$)

	@param x Space point where the magnetic field should be
	evaluated.

	@param b On return, it should hold the magnetic field at @a x
	in units of kilo Gauss.
     */
    virtual void Field(const TVector3& x, TVector3& b);
    /** Set the magnetic field object */
    void SetField(Simulation::Field* f) { fField = f; }
    /** Set the VMC configuration macro.  The script should define the
     * VMC and configure it.  Note, the geometry is not defined at the
     * time the script is called - indeed the geometry is typically
     * defined via a VMC call to Application::ConstructGeometry.
     *
     * @param scriptname File name of script 
     */
    void SetVMCConfig(const char* scriptname) { fVMCConfig = scriptname; }
    /** Set VMC to use based on preset values (Use SetVMCConfig to
	configure the VMC in more detail. */
    void SetVMC(Int_t which=kGeant3) {
      SetVMCConfig(which == kGeant3 ? "G3Config.C" :
		   which == kGeant4TGeo ? "G4Config.C" : "G4ConfigNative.C"); }
    /** Set script to execute on VMC backend _before_ Init, e.g., to
	set processes and cuts. */ 
    void SetMoreConfig(const char* scriptname) { fMoreConfig = scriptname; }
    /** Call the More configuration script on VMC backend */
    void MoreConfig(TVirtualMC* mc);
  private:
    /** The simulation application */
    Application* fApplication;
    /** Whether we have been initialized */ 
    Bool_t fIsInitialized;
    /** Magnetic field - not owned */
    Simulation::Field* fField;
    /** VMC back-end configuration */
    TString fVMCConfig;
    /** VMC back-end configuration */
    TString fMoreConfig;
    
    ClassDef(Main, 4) // 
  };
}

#endif
      
