// -*- mode: C++ -*-
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Stack.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Simulation::Stack
*/
#ifndef Simulation_Stack
#define Simulation_Stack
#include <TVirtualMCStack.h>
#include <TClonesArray.h>
#include <TObjArray.h>
#include <TParticle.h>
#include <TVirtualGeoTrack.h>
#include <vector>
class TGenerator;
class TBrowser;

namespace Simulation 
{
  /** @class Stack simulation/Stack.h <simulation/Stack.h>
      @ingroup sim_vmc
      @brief Stack of particles (and tracks) used for the transport
      code.   The simulation backend pushes particles on to the top of
      the stack as they are created, and pops them from the top (LIFO)
      when it needs to transport that track. 
   */
  class Stack : public TVirtualMCStack 
  {
  public: 
    /** Make a stack
	@param size Initialize the stack to this size  */
    Stack(Int_t size);
    /** Default ctor  */
    Stack();
    /** Destructor. */
    virtual ~Stack();

    /**@{*/
    /** @name TVirtualMCStack Methods  
        The TVirtualMCStack class dictates that these member function
	should be overloaded. 
    */
    /** Push a track onto the stack. 
	@param toBeDone    Wether to track this particle or not 
	@param parent      Parent track # 
	@param pdg         PDG particle type # 
	@param px          X coordinate of momentum vector GeV/C
	@param py          Y coordinate of momentum vector GeV/c
	@param pz          Z coordinate of momentum vector GeV/c
	@param e           Energy coordinate of the momentum vector GeV
	@param vx          X coordinate of production vertex cm
	@param vy          Y coordinate of production vertex cm
	@param vz          Z coordinate of production vertex cm
	@param tof         Time coordinate of production vertex t
	@param polx        X coordinate of polarisation vector 
	@param poly        Y coordinate of polarisation vector 
	@param polz        Z coordinate of polarisation vector 
	@param mech        Prodcution mechanism 
	@param ntr         On output, the number of the track stored
	@param weight      Weight (calculated mass) of particle
	@param is          Status */
    void PushTrack(Int_t toBeDone, Int_t parent, Int_t pdg,
		   Double_t px, Double_t py, Double_t pz, Double_t e, 
		   Double_t vx, Double_t vy, Double_t vz, Double_t tof, 
		   Double_t polx, Double_t poly, Double_t polz, 
		   TMCProcess mech, Int_t& ntr, Double_t weight, 
		   Int_t is); 
    /** Pop the next track for tracking. 
	@param track On return, the poped track number 
	@return  The next track */
    TParticle* PopNextTrack(Int_t& track);
    /** Pop the next primary particle from the stack. 
	@param i Number of primary to pop 
	@return  The primary track */
    TParticle* PopPrimaryForTracking(Int_t i);
    /** Set the current track number 
	@param track Track nmber to set as the current one */
    void SetCurrentTrack(Int_t track) { fCurrentTrack = track; }     
    /** Get nmber of tracks 
	@return  The number of tracks */
    Int_t GetNtrack() const { return fParticles->GetEntriesFast(); }
    /** Get the number of primaries 
	@return  The number of primaries left. */
    Int_t GetNprimary() const  { return fNPrimary; }  
    /** Get the current track 
	@return The current track */
    TParticle* GetCurrentTrack() const;
    /** Get the current track number 
	@return  The current track number */
    Int_t GetCurrentTrackNumber() const { return fCurrentTrack; }  
    /** Get the parent track number of the current track. 
	@return Parent track number of crrent track  */
    Int_t GetCurrentParentTrackNumber() const;
    /**@}*/

    /**@{*/
    /** @name Non-standard member functions 
	These member functions are not part of the TVirtualMCStack
	interface and can not be sent as messages to pointers of that
	class.  Access to these member functions must go via the
	Simulation::Main singleton */
    /** Reset the stack. */
    void Reset();
    /** Import particles from a TGenerator object onto the stack. 
	@param g The generator to import the particles from. */
    void ImportPrimaries(TClonesArray* g);
    /** Get particle number @a i 
	@param i The particle to get 
	@return  The particle to get, or 0 if not found. */
    TParticle* GetParticle(Int_t i) const;
    /** Get track number @a i 
	@param i The track to get 
	@return  The track to get, or 0 if not found. */
    TVirtualGeoTrack* GetTrack(Int_t i) const;
    /** Get the particles container */ 
    TClonesArray* GetParticles() const { return fParticles; }
    /** Whether we keep intermediate tracks for visualisation */
    Bool_t IsKeepIntermediate() const { return fKeepIntermediate; }
    /** Set whether we should keep the intermediate particles. 
	@param keep If true, the intermediate track lines are kept and
	can later by used in visualisation code. */
    void SetKeepIntermediate(Bool_t keep=kTRUE) { fKeepIntermediate = keep; } //*MENU*
    /**@}*/
    
    /**@{*/
    /** @name Navigation member functions */
    /** Whether the browser should represent this as a folder 
	@return  always true */
    Bool_t IsFolder() const { return kTRUE;  } 
    /** Browse this object
	@param b TBrowser to use  */
    virtual void Browse(TBrowser* b);
    /**@}*/
  protected:
    /** Create a visualisation track.
	@param ntr    Track number
	@param parent If 0 or positive, it is parent track number, and
	       we create the track as a daughter of that track
	@param particle The particle to create a track for

	@return pointer to new track or null.
    */
    TVirtualGeoTrack* MakeGeoTrack(Int_t      ntr,
				   Int_t      parent,
				   TParticle* particle);
  private:
    /** Type of stack entries */
    typedef std::pair<Int_t,TParticle*> StackEntry_t;
    /** Type of internal stack */
    typedef std::vector<StackEntry_t> Stack_t;
    /** Tracked particles */
    Stack_t fStack;               //!
    /** All particles */
    TClonesArray* fParticles;     //!
    /** All tracks */
    TObjArray* fTracks;           //!
    /** The current track number */
    Int_t fCurrentTrack;  
    /** The number of primaries */
    Int_t fNPrimary;  
    /** The number of tracks */
    Int_t fNtrack;
    /** Whether to store intermediate tracks for visualisation */
    Bool_t fKeepIntermediate;
    ClassDef(Stack, 1) // Stack definition
  };
}

#endif
//
// EOF
//
