//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/TreeIO.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Simulation::TreeWriter and
    Simulation::TreeReader 
*/
#include "simulation/TreeIO.h"
#include "framework/TreeIO.h"

//====================================================================
ClassImp(Simulation::TreeReader);

//____________________________________________________________________
Simulation::TreeReader::TreeReader(const Char_t* name, const Char_t* title) 
  : Simulation::Task(name, title) 
{
  // Default constructor
  fTreeReader = new Framework::TreeReader(name, title);
}

//____________________________________________________________________
void 
Simulation::TreeReader::SetVerbose(Int_t v) 
{
  fTreeReader->SetVerbose(v);
  Simulation::Task::SetVerbose(v);
}
//____________________________________________________________________
void 
Simulation::TreeReader::SetDebug(Int_t v) 
{
  fTreeReader->SetDebug(v);
  Simulation::Task::SetDebug(v);
}

//____________________________________________________________________
void 
Simulation::TreeReader::Print(Option_t* option) const
{
  fTreeReader->Print(option);
  Simulation::Task::Print(option);
}

//====================================================================
ClassImp(Simulation::TreeWriter);

//____________________________________________________________________
Simulation::TreeWriter::TreeWriter(const Char_t* name, const Char_t* title) 
  : Simulation::Task(name, title) 
{
  // Default constructor
  fTreeWriter = new Framework::TreeWriter(name, title);
}

//____________________________________________________________________
void 
Simulation::TreeWriter::SetVerbose(Int_t v) 
{
  fTreeWriter->SetVerbose(v);
  Simulation::Task::SetVerbose(v);
}
//____________________________________________________________________
void 
Simulation::TreeWriter::SetDebug(Int_t v) 
{
  fTreeWriter->SetDebug(v);
  Simulation::Task::SetDebug(v);
}
    
//____________________________________________________________________
void 
Simulation::TreeWriter::Print(Option_t* option) const
{
  fTreeWriter->Print(option);
  Simulation::Task::Print(option);
}


//____________________________________________________________________ 
//  
// EOF
//
