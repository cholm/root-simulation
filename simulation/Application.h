// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Application.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Simulation::Application
*/
#ifndef Simulation_Application
#define Simulation_Application
#include <vmc/TVirtualMCApplication.h>
#include <TVector3.h>
#include <map>
#include <vector>
class TBrowser;
class TGeoMedium;

namespace Simulation 
{
  class Stack;
  class Task;
  
  /** @defgroup sim_vmc VMC interface classes 
      @ingroup sim_steer
      @brief This group contains the classes needed to interface the
      ROOT VMC library.   The user shouldn't need to do much with
      these classes. 
  */

  /** @class Application simulation/Application.h <simulation/Application.h>
      @brief VMC application. 
      @ingroup sim_vmc

      This class defines the interface between the VMC and the
      Simulation::Main and Simulation::Task objects.  The flow is
      governed by the VMC and is as follows.

      @verbatim
      Construction     (done by the Simulation::Main object)
       |
      Simulation::Main::Initialize (via Simulation::Main::Loop.
       |                           This calls
       |                           Simulation::Task::Initialize for all tasks,
       |                           and then generates the VMC)
       |
       +- Init                  (This delegates to TVirtualMC::Init)
       |   |
       |   +- TVirtualMC::Init
       |       |
       |       +- AddParticles (delegates to Simulation::Main
       |	   |                  which then calls
       |	   |                  Simulation::Task::DefineParticles
       |	   |                  for all tasks)
       |	   |
       |	   +- ConstructGeometry (delegates to tasks
       |	   |                       Simulation::Task::Register)
       |	   |
       |	   +- InitGeometry (Closes geometry and possible saves it)
       |
      Simulation::Main::Run (via Simulation::Main::Loop)
       |
       +- Run(n)
       |   |
       |   +- TVirtualMC::ProcessRun(n)
       |       |
       |       +- BeginEvent
       |       |
       |       +- GeneratePrimaries (calls Simulation::Task::GeneratePrimaries)
       |       |   |
       |       |   +- BeginPrimary  (calls Simulation::Task::BeginPrimary)
       |       |   |   |
       |       |   |   +- PreTrack  (calls Simulation::Task::BeginTrack,
       |       |   |   |             also for generated secondaries)
       |       |   |   |
       |       |   |   +- Stepping  (calls registered Simulation::Task::Step
       |       |   |   |             for current medium)
       |       |   |   |
       |       |   |   +- PostTrack (calls Simulation::Task::FinishTrack,
       |       |   |                 also for generated secondaries)
       |       |   |    
       |       |   +- FinishPrimary (calls Simulation::Task::FinishPrimary)
       |       |
       |       +- FinishEvent (calls Simulation::Task::FinishEvent)
       |
     Simulation::Main::Finish (calls Simulation::Task::Finish)
     @endverbatim

     Note that the backends are not entirely consistent in how they
     call the client code (implementation of TVirtualMCApplication).

     - Construction time (TVirtualMC::TVirtualMC)
       - GEANT 3.21 does call any user code at this point.
       - GEANT 4 calls TVirtualMCApplication::ConstructGeometry
     - Initialisation time (TVirtualMC::Init)
       - GEANT 3.21 calls TVirtualMCApplication::ConstructGeometry,
         TVirtualMCApplication::AddParticles, etc.
       - GEANT 4 does _not_ call TVirtualMCApplication::ConstructGeometry,
         but does call TVirtualMCApplication::AddParticles, etc.

     We accommodate _both_ scenarios by insisting that the we call
     Simulation::Task::Initialize _before_ construction of the backend,
     and that this member function should define geometry.

     Note, newer VMC backend wrappers explicitly assumes ROOT units
     (cm, s, GeV).  In the Main task we check that this is the case
     before we start to generate geometry via Simulation::Task::Init.

     Note that I've check this by defining a simulation where I shoot
     a photon through a vacuume volume and record time-of-entry and
     -exit, and see that it takes the expected amount of time.  I've
     removed the code from the repo as it may confuse more than help,
     but it can be found in the history should need be.
  */              
  class Application : public TVirtualMCApplication 
  {
  public:
    /** Constructor 
	@param name Name 
	@param title Title */
    Application(const Char_t* name="", const Char_t* title="");
    /** Destructor */
    virtual ~Application() {}

    /** Set name of file to save geometry to.  If not set, then the
	geometry will not be saved.
	
	@param filename Name of file to save geometry to. 
    */
    void SaveGeometryToFile(const char* filename="geometry.root")
    {
      fGeoFileName = filename;
    }
    /**@{*/
    /** @name Medium registration member functions */
    /** Register a medium as beloning to a task.   This should be used
	only if the medium was made using @c TVirtualMC::Gstmed
	
	@deprecated RegisterMedium(TGeoMedium*,Simulation::Task*);

	@param id Medium ID
	@param task Task it belongs to 
	@return  true on success  */
    virtual Bool_t RegisterMedium(Int_t id, Simulation::Task* task);
    /** Register a medium as belonging to a task.  This should only
	used if the task makes mediums via TGeoMedium 
	@param medium The medium
	@param task The task. 
	@return true on success */
    virtual Bool_t RegisterMedium(TGeoMedium* medium, Simulation::Task* task);
    /**@}*/

    /**@{*/
    /** @name Navigation member functions */
    /** Return true to indicate that this should be browsed as a folder
	@return  laway true */
    virtual Bool_t IsFolder() const { return kTRUE; }
    /** Browse this object 
	@param b Browser to use  */
    virtual void   Browse(TBrowser* b);
    /** Print this object 
	@param option if it contains @c "M" print medium map  */
    virtual void Print(Option_t* option="M") const;
    void Draw(Option_t* option)
    {
      Draw(option, 30, 30, 0, 10, 10, .01, .01);
    }
    /** Draw the geometry. If ExecInitialize hasn't been called yet,
	then do so.  
	@param option Drawing option, or volume name. 
	@param theta  The @f$ \theta@f$ viewing angle. 
	@param phi    The @f$ \phi@f$ viewing angle. 
	@param psi    The @f$ \psi@f$ viewing angle. 
	@param u0     X-coordinate of center point in canvas. 
	@param v0     Y-coordinate of center point in canvas. 
	@param ul     Canvas X scale
	@param vl     Canvas Y scale */
    void Draw(Option_t* option,
	      Double_t  theta,
	      Double_t  phi, 
	      Double_t  psi,
	      Double_t  u0,
	      Double_t  v0, 
	      Double_t  ul,
	      Double_t  vl);
    /**@}*/

    /**@{*/
    /** @name Simulation control member functions */
    /** Get the stack */
    Stack* GetStack() const { return fStack; }
    /** Initialize the application. 
	Initialized the simulation backend, which in turn sends the
	message Simulation::Task::Register to all tasks */
    virtual void Init();
    /** Make an event */ 
    virtual void Event();
    /** Make a run 
	@param n Number of events to make in the run */
    virtual Bool_t Run(Int_t n);
    /**@}*/

    /**@{
       @name TVirtualMCApplication member functions.  

       The TVirtualMCApplication interface mandates that these member
       functions should be defined. 
    */
    /** Construct the geometry -
       calls Simulation::Task::Register for all defined tasks. */
    virtual void   ConstructGeometry();
    /** Initialize the geometry and TVirtual */
    virtual void   InitGeometry();
    /** Define new particles types, calles
	Simulation::Task::DefineParticles for all defined tasks.  */
    virtual void   AddParticles();
    /** Generate an event, and push them onto the stack.  Calls
	Simulation::Task::GeneratePrimaries for all defined tasks. */
    virtual void   GeneratePrimaries();
    /** Called at the beginning of an event. Calls
	Simulation::Task::BeginEvent for all defined tasks. */ 
    virtual void   BeginEvent();
    /** Called before starting transport of a primary particle. Calls
	Simulation::Task::BeginPrimary for all defined tasks. */ 
    virtual void   BeginPrimary();
    /** Called before starting transport of aparticle. Calls
	Simulation::Task::PreTrack for all defined tasks. */ 
    virtual void   PreTrack();
    /** Called every time a track makes a hit in an active
	volume. Calls Simulation::Task::Exec for the task associated
	with the medium the hit is made in. */  
    virtual void   Stepping();
    /** Called after transport of a particle. Calls
	Simulation::Task::PostTrack for all defined tasks. */ 
    virtual void   PostTrack();
    /** Called after transport of a primary particle. Calls
	Simulation::Task::FinishPrimary for all defined tasks. */ 
    virtual void   FinishPrimary();
    /** Called at the end of an event. Calls
	Simulation::Task::FinishEvent for all defined tasks. */ 
    virtual void   FinishEvent();
    /** Called at get the magnetic field at a given space point 
	@f$\mathbf{x}  = (x,y,z)@f$.  It calls Simulation::Task::Field
	for the task in which @f$ \mathbf{x}@f$.  The task should
	return the magnetic field in @f$\mathbf{B} =
	(b_0,b_1,b_2)@f$ in units of kilo Gaus (@f$ 10^4 \mbox{gauss}
	= 1 \mbox{T}@f$). */ 
    virtual void   Field(const Double_t* x, Double_t* b) const;
    /**@}*/

    /** Get X component of the @f$\mathbf{B}@f$ field 
	@return 3-vector the @f$\mathbf{B}@f$ field
	@f$(B_x,B_y,B_z)@f$ */
    const TVector3& GetFieldX() { return fX;; }
    /** Set the @f$\mathbf{B}@f$ field 
	@param b 3-vector of the @f$\mathbf{B}@f$ field
	@f$(B_x,B_y,B_z)@f$ */
    void SetFieldB(const TVector3& b) { fB = b; }
    /** Map MC volume ID to TGeo volume ID using the internal map 
	
	@param volId MC volume ID 
	@return TGeo volume ID (number) or -1 if not found */
    Int_t MCVol2Geo(Int_t volId) const;
    /** Set volume colours, etc. from mediums */
    virtual void SetColors(TObjArray* l);
  protected:
    /** The stack */
    Stack* fStack; //!
    /** Map of tracking medium ID to tasks */
    typedef std::multimap<Int_t, Simulation::Task*> MediumTaskMap;
    /** Iterator over tracking medium ID to tasks */
    typedef MediumTaskMap::const_iterator MediumTaskIter;
    /** Range over tracking medium ID to tasks */
    typedef std::pair<MediumTaskIter,MediumTaskIter> MediumTaskRange;
    /** Map of tracking medium to tasks */
    typedef std::multimap<TGeoMedium*,Simulation::Task*> GeoMediumMap;
    /** Map of MC volume ID to geo volume ID */
    typedef std::map<Int_t, Int_t> MCGeoMap;
    /** Map of tracing medium to tasks */
    MediumTaskMap fMediumTaskMap; 
    /** Map of tracking medium to tasks.  Only used during set-up. */
    GeoMediumMap fGeoMediumMap;
    /** Map of MC volume ID to geo volume ID */
    MCGeoMap fMCGeoMap;
    /** Get a task associated with a medium number. 
	@param mediumId The medium number to get the associated tasks 
	@return  The range of registered tasks for this medium */
    MediumTaskRange Medium2Tasks(Int_t mediumId) const;
    /** Set up mapping from MC volume numbers to TGeo volume numbers
	from a list of TGeoVolume objects */
    void MapVolumes(TObjArray* l);
    /** Set up the mapping from MC medium numbers to tasks */
    void MapMediums();
    /** The current position for fields */
    mutable TVector3 fX;  //! 
    /** The current field */
    TVector3 fB;  //!
    /** Output filename for geomtry */
    TString fGeoFileName; //!
    ClassDef(Application, 1) // 
  };
}

#endif
      
