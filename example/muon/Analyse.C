//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/muon/Analyse.C
 *  @author  Christian Holm Christensen <cholm@nbi.dk>
 *  @date    Thu Dec  2 01:12:02 2004
 *  @brief   Example to run simulation in batch mode 
 */
void Build(const char* script)
{
  // "s" option is needed here because of some wierd bug in ROOT's
  // TSystem::CompileMacro.  It seems like there's an overflow of a
  // string which causes the library name set there to recursively
  // refer back to it self.
  std::cout << " " << script << "..." << std::flush;
  gROOT->LoadMacro(Form("%s+s",script));
}
void
Analyse(Int_t nev=-1, Bool_t notbatch=false)
{
  if (nev != 0 and !notbatch) gROOT->SetBatch(kTRUE);
  std::cout << "Building ..." << std::flush;
  Build("Analyser.C");
  std::cout << std::endl;
  gROOT->Macro("Analysis.C");

  if (nev == 0) 
    Printf("Execute \n\n"
	   "\tFramework::Main* main = Framework::Main::Instance();\n\n"
	   "to get handle on main interface.  Loop over 10 events by\n"
	   "executing\n\n"
	   "\tmain->Loop(10);\n");
  else {
    gROOT->ProcessLine(Form("Framework::Main::Instance()->Loop(%d)",nev));
  }	
}
