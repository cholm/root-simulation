//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/Fixed.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Fixed
*/
#include "simulation/Fixed.h"
#include <TClonesArray.h>
#include <TVector3.h>
#include <TParticle.h>
#include <TParticlePDG.h>
#include <TDatabasePDG.h>
#include <TMath.h>
#include <TRandom.h>
#include <iostream>
#define DEGRAD TMath::Pi() / 180.

//____________________________________________________________________
ClassImp(Simulation::Fixed);

//____________________________________________________________________
Simulation::Fixed::Fixed() 
  : TGenerator("fixed", "A simple fixed particle generator")
{
  fParticles = new TClonesArray("TParticle");
  SetPdgCode();
}

//____________________________________________________________________
void
Simulation::Fixed::SetPdg(const char* name)
{
  TDatabasePDG* pdgDb = TDatabasePDG::Instance();
  TParticlePDG* pdgP  = pdgDb->GetParticle(name);
  if (!pdgP) {
    Warning("SetPdg", "Unknown particle type %s", name);
    return;
  }
  fPdgCode = pdgP->PdgCode();
}

//____________________________________________________________________
void
Simulation::Fixed::Print(Option_t* option) const
{
  std::cout << "Fixed particle canon of: " << fPdgCode << "\n" 
	    << "\tMultiplicity:    [" << fMinM << "," << fMaxM << "]\n"  
	    << "\tMomentum:        [" << fMinP << "," << fMaxP << "] GeV/c\n" 
	    << "\tPolar angle:     [" << fMinTheta << "," << fMaxTheta 
	    << "] degrees\n" 
	    << "\tAzimuthal angle: [" << fMinPhi << "," << fMaxPhi 
	    << "] degrees\n" 
	    << std::endl;
}
    
//____________________________________________________________________
Int_t
Simulation::Fixed::ImportParticles(TClonesArray* a, Option_t* option) 
{
  a->Clear();
  TDatabasePDG* pdgDb = TDatabasePDG::Instance();
  TParticlePDG* pdgP  = pdgDb->GetParticle(fPdgCode);
  Double_t      mass  = pdgP->Mass();
  Int_t m = Int_t(gRandom->Uniform(fMinM, fMaxM));
  for (Int_t i = 0; i < m; i++) {
    Double_t p     = gRandom->Uniform(fMinP,     fMaxP);
    Double_t phi   = gRandom->Uniform(fMinPhi,   fMaxPhi);
    Double_t theta = gRandom->Uniform(fMinTheta, fMaxTheta);
    Double_t pz    = p  * TMath::Cos(theta * DEGRAD);
    Double_t pt    = p  * TMath::Sin(theta * DEGRAD);
    Double_t px    = pt * TMath::Cos(phi   * DEGRAD);
    Double_t py    = pt * TMath::Sin(phi   * DEGRAD);
    Double_t e     = TMath::Sqrt(mass*mass + px*px + py*py + pz*pz);
    TParticle* pp = new ((*a)[i]) TParticle(fPdgCode, 0, -1, -1, -1, -1, 
					    px, py, pz, e, 0, 0, 0, 0);
    pp->SetLineColor(kGreen);
    // pp->SetUniqueID(1);
  }
  return m;
}


//____________________________________________________________________
TObjArray* 
Simulation::Fixed::ImportParticles(Option_t* option) 
{
  ImportParticles(static_cast<TClonesArray*>(fParticles), option);
  return fParticles;
}



//____________________________________________________________________
//
// EOF
//
