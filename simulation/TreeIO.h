// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/TreeIO.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Simulation::TreeWriter and
    Simulation::TreeReader 
*/
#ifndef Simulation_TreeIO
#define Simulation_TreeIO
#include <simulation/Task.h>
#include <framework/TreeIO.h>

namespace Simulation
{
  //==================================================================
  /** @class TreeReader simulation/TreeIO.h <simulation/TreeIO.h>
      @ingroup sim_special_tasks
      @brief Read TTree from a TFile

      This task will open the TFile passed in the title argument of
      the constructor in READ mode and read the TTree called
      @a name in that file.  The file is opened via TFile::Open, so a
      file using a protocol for which there's registered a handler
      (see TPluginManager) can be opened.
 
      The class registeres the tree as the input tree with Main.  The
      TFolder of the object is set to the base folder of Main.
  */
  class TreeReader : public Simulation::Task
  {
  private:
    Framework::TreeReader* fTreeReader;
  public:
    /** Create a writer object 
	@param name Name of tree to write to 
	@param file File to write tree to */
    TreeReader(const Char_t* name, const Char_t* file);
    /** Destruct a writer.  Closes the output tree */
    virtual ~TreeReader() {}

    /** Open the file, and get the tree from the file.  
	@param o Option string - passed on. */
    virtual void Initialize(Option_t* o="") { fTreeReader->Initialize(o); }
    /** Does nothing 
	@param o Option string - passed on. */
    virtual void Register(Option_t* o="") { fTreeReader->Register(o); }
    /** Gets the next entry from the tree  */
    virtual void BeginEvent() { fTreeReader->Exec(); }
    /** Closes the file. 
	@param o Option string - passed on. */
    virtual void Finish(Option_t* o="") { fTreeReader->Finish(o); }

    /** Set the verbosity 
	@param v Verbosity level  */
    virtual void SetVerbose(Int_t v);
    /** Set the debug level
	@param v The debug level */
    virtual void SetDebug(Int_t v);
    
    /** Write information on reader
	@param option Option string - passed on. */
    virtual void Print(Option_t* option="") const; //*MENU*

    ClassDef(TreeReader,1) // Write TTree to TFile
  };

  //==================================================================
  /** @class TreeWriter simulation/TreeIO.h <simulation/TreeIO.h>
      Write TTree to TFile

      This task will open the TFile passed in the title argument of
      the constructor in RECREATE mode and create the TTree called
      name in that file.  The file is opened via TFile::Open, so a
      file using a protocol for which there's registered a handler
      (see TPluginManager) can be opened.
 
      The class registeres the tree as the output tree with Main.  The
      TFolder of the object is set to the base folder of Main.
  */
  class TreeWriter : public Simulation::Task
  {
  private:
    Framework::TreeWriter* fTreeWriter;
  public:
    /** Create a writer object 
	@param name Name of tree to write to 
	@param file File to write tree to */
    TreeWriter(const Char_t* name, const Char_t* file);
    /** Destruct a writer.  Closes the output tree */
    virtual ~TreeWriter() {}

    /** Open the file, and make a tree in the file.  
	@param o Option string - passed on. */
    virtual void Initialize(Option_t* o="") { fTreeWriter->Initialize(o); }
    /** Does nothing 
	@param o Option string - passed on. */
    virtual void Register(Option_t* o="") { fTreeWriter->Register(o); }
    /** Fills the tree  */
    virtual void FinishEvent() { fTreeWriter->Exec(); }
    /** Flush data to file, and close file. 
	@param o Option string - passed on. */
    virtual void Finish(Option_t* o="") { fTreeWriter->Finish(o); }

    /** Set the verbosity 
	@param v Verbosity level  */
    virtual void SetVerbose(Int_t v);
    /** Set the debug level
	@param v The debug level */
    virtual void SetDebug(Int_t v);
    
    /** Write information on writer 
	@param option Option string - passed on. */
    virtual void Print(Option_t* option="") const; //*MENU*

    ClassDef(TreeWriter,1) // Write TTree to TFile
  };
}
#endif
//____________________________________________________________________ 
//  
// EOF
//
