//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/muon/Analysis.C
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:02 2004
    @brief   Example configuration script  */
//____________________________________________________________________
/** Configure the simulation */
void Analysis() 
{
  Framework::Main*              main    = new Framework::Main("Muon", "Muon");
  Framework::TreeReader*        treeIn  =
    new Framework::TreeReader("Hits","hits.root");
  Simulation::DefaultHitReader* hitIn   =
    new Simulation::DefaultHitReader("Muon","Muon","Hits");
  Simulation::TrackReader*      trackIn =
    new Simulation::TrackReader("Tracks","Tracks","Hits");
  Analyser*                     ana     = new Analyser("Analyser","Analyser");

  // Add to main task 
  main->Add(treeIn);
  main->Add(hitIn);
  main->Add(trackIn);
  main->Add(ana);
  
  // Set flags for execution 
  main->SetVerbose(1);
  main->SetDebug(0);
  
  if (!gROOT->IsBatch()) {
    TBrowser* b = new TBrowser("b", "Browser");
    Printf("\nSetup complete.\n\nUse context menu to execute analysis, "
	   "or type\n\n\tFramework::Main::Instance()->Loop();\n\n"
	   "to loop over all events");
  }
}


//____________________________________________________________________
//
// EOF
//
