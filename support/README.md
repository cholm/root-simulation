# VMC backend 

The scripts [`G3Config.C`](G3Config.C), [`G4Config.C`](G4Config.C),
and [`G4ConfigNative.C`](G4ConfigNative.C) are examples of setting up
a GEANT 3.21 and Geant 4, respectively, virtual monte-carlo backend.
These are installed in the data directory of this package, and the
main class `Simulation::Main` is set up to look for them there.

However, if a user need to configure the VMC, then the scripts can be
copied to the current working directory, or somewhere early in the
ROOT macro path, and changed there.  It could be that a user wants to
use a different physics list in Geant 4, or change some cuts in GEANT
3.21. 

- [`G3Config.C`](G3Config.C) uses `TGeant3TGeo` (from the [GEANT 3.21
  VMC](https://github.com/vmc-project/geant3) project). 
- [`G4Config.C`](G4Config.C) uses `TGeant4` (from the [ROOT VMC
  interface to Geant 4
  (`geant4_vmc`)](https://github.com/vmc-project/geant4_vmc.git)
  project), and assumes a geometry build via ROOT's `TGeo` classes and
  the simulation will navigate the geometry via these classes. 
- [`G4ConfigNative.C`](G4ConfigNative.C) uses `TGeant4` (from the
  [ROOT VMC interface to Geant 4
  (`geant4_vmc`)](https://github.com/vmc-project/geant4_vmc.git)
  project), and assumes a geometry build via ROOT's `TGeo` classes but
  the simulation will navigate the geometry via Geant 4.
