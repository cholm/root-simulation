// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: Gun.h,v 1.5 2005-12-05 15:19:17 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/Gun.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Example::Gun
*/
#ifndef Example_Gun
#define Example_Gun
#include <simulation/Generator.h>
#include <simulation/Single.h>

class TRandom;
class TLorentzVector;

namespace Example 
{
  class Gun : public Simulation::Generator
  {
  protected:
    TRandom* fRandom;
    /** Radiation length in active medium */
    Double_t fX0;
    /** Generator of primary particles */ 
    Simulation::Single* fSingle;
  public:
    /** Constructor

	@param pdg   Particle to creat
	@param p     Momentum
	@param vx    Production vertex X coordinate in cm
	@param vy    Production vertex X coordinate in cm
	@param vz    Production vertex X coordinate in cm
	@param t     Production vertex time in s
	@param phi   Azimuthal angle in degrees
	@param theta Polar angle in degrees
    */
    Gun(const char* pdg, 
	Double_t p=10, 
	Double_t vx=-1,
	Double_t vy=-1,
	Double_t yz=-1,
	Double_t phi=0,
	Double_t theta=0);
    /** Constructor

	@param pdg   Particle to creat
	@param p     Momentum
	@param vx    Production vertex X coordinate in cm
	@param vy    Production vertex X coordinate in cm
	@param vz    Production vertex X coordinate in cm
	@param t     Production vertex time in s
	@param phi   Azimuthal angle in degrees
	@param theta Polar angle in degrees
    */
    Gun(Int_t    pdg   = 22, 
	Double_t p     = 10, 
	Double_t vx    = -1,
	Double_t vy    = -1,
	Double_t yz    = -1,
	Double_t phi   = 0,
	Double_t theta = 0);
    void Register(Option_t* option="");
    /** Uh!? */
    void Step() {}
    /** Set particle type */
    void SetPdg(Int_t pdg=22) { fSingle->SetPdgCode(pdg); } //*MENU*
    /** Set momemtum */
    void SetP(Double_t p=10) { fSingle->SetP(p); } //*MENU*
    /** Set polar angle */
    void SetTheta(Double_t theta) { fSingle->SetTheta(theta); }
    /** Set azimuthal angle */
    void SetPhi(Double_t phi) { fSingle->SetPhi(phi); }
    ClassDef(Gun,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
