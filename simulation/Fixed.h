// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/Fixed.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Fixed
*/
#ifndef Simulation_Fixed
#define Simulation_Fixed
#include <TGenerator.h>
class TClonesArray;
class TObjArray;

namespace Simulation 
{
  
  /** @class Fixed simulation/Fixed.h <simulation/Fixed.h>
      @ingroup sim_gen
      @brief A simple event generator that generates fixed type
      particles uniformly in fixed @f$(p,\theta,\phi)@f$ space.
  */
  class Fixed : public TGenerator 
  {
  public:
    /** CTOR */
    Fixed();
    /** Set the type of particle to produce.
	@param pdg Particle type ID code */
    void SetPdgCode(Int_t pdg=211) { fPdgCode = pdg; }//*MENU*
    /** Set the tipe of paritcle to produce by name (one known via
	TDatabasePDG)
	@param pdg Particle type name - default is @f$ \pi^+@f$ */
    void SetPdg(const char* pdg="pi+"); //*MENU*
    /** Set the least number of particles to make. The number of
	particles produced is uniformly distributed between the
	minimum and maximum.
	@param n Minimum number of particles */
    void SetMinM(Int_t n) { fMinM = n; } //*MENU*
    /** Set the largest number of particles to make.  The number of
	particles produced is uniformly distributed between the
	minimum and maximum.
	@param n Maximum number of particles */
    void SetMaxM(Int_t n) { fMaxM = n; } //*MENU*
    /** Set least and largest number of particles to produce.  The
	number of particles produced is uniformly distributed between
	the minimum and maximum.
	@param min Minimum number of particles 
	@param max Maximum number of particles */
    void SetM(Int_t min, Int_t max) { fMinM = min; fMaxM = max; }
    /** Set the least z-momentum to produce per particle.  The
	momentum of particles is uniformly distributed between the
	minimum and maximum.
	@param p Minimum z-momentum in GeV*/
    void SetMinP(Double_t p) { fMinP = p; } //*MENU*
    /** Set the largest z-momentum to produce per particle.  The
	momentum of particles is uniformly distributed between the
	minimum and maximum.
	@param p Maximum z-momentum  in GeV*/
    void SetMaxP(Double_t p) { fMaxP = p; } //*MENU*
    /** Set the least and largest z-momentum to produce per particle.
	The momentum of particles is uniformly distributed between the
	minimum and maximum.
	@param min Minimum momentum in GeV
	@param max Maximum momentum  in GeV*/
    void SetP(Double_t min, Double_t max) { fMinP = min; fMaxP = max; }
    /** Set the least azimuthal angle to produce.  The azimuthal angle
	is uniformly distributed betweem the minumum and maximum.
	@param min Minimum @f$ \phi@f$ angle (polar angle) in degrees */
    void SetMinPhi(Double_t min) { fMinPhi = min; } //*MENU*
    /** Set the largest azimuthal angle to produce.  The azimuthal angle
	is uniformly distributed betweem the minumum and maximum.
	@param max Maximum @f$ \phi@f$ angle (polar angle) in degrees */
    void SetMaxPhi(Double_t max) { fMaxPhi = max; } //*MENU*
    /** Set the least and largest azimuthal angle to produce.  The
	azimuthal angle is uniformly distributed betweem the minumum
	and maximum.       
	@param min Minimum @f$ \phi@f$ angle (polar angle) in degrees	
	@param max Maximum @f$ \phi@f$ angle (polar angle) in degrees */
    void SetPhi(Double_t min,Double_t max) { fMinPhi = min; fMaxPhi = max; }
    /** Set the least polar angle to produce.  The polar angle is
	uniformly distribted between the minimum and maximum.
	@param min Minimum @f$ \theta@f$ angle (polar angle) in degrees */
    void SetMinTheta(Double_t min) { fMinTheta = min; } //*MENU*
    /** Set the largest polar angle to produce.  The polar angle is
	uniformly distribted between the minimum and maximum.
	@param max Maximum @f$ \theta@f$ angle (polar angle) in degrees */
    void SetMaxTheta(Double_t max) { fMaxTheta = max; } //*MENU*
    /** Set the least and largest polar angle to produce.  The polar
	angle is uniformly distribted between the minimum and maximum.
	@param min Minimum @f$ \theta@f$ angle (polar angle) in degrees 
	@param max Maximum @f$ \theta@f$ angle (polar angle) in degrees */
    void SetTheta(Double_t min,Double_t max) {fMinTheta = min;fMaxTheta = max; }
    /** @return Get the PDG code */
    Int_t GetPdgCode() const { return fPdgCode; }
    /** @return Minimum number of particles */
    Int_t GetMinM() const { return fMinM; };
    /** @return Maximum number of particles */
    Int_t GetMaxM() const { return fMaxM; };
    /** @return Minimum momentum  in GeV*/
    Double_t GetMinP() const { return fMinP; };
    /** @return Maximum momentum  in GeV*/
    Double_t GetMaxP() const { return fMaxP; };
    /** @return Minimum @f$ \phi@f$ angle (polar angle) in degrees */
    Double_t GetMinPhi() const { return fMinPhi; };
    /** @return Maximum @f$ \phi@f$ angle (polar angle) in degrees */
    Double_t GetMaxPhi() const { return fMaxPhi; };
    /** @return Minimum @f$ \theta@f$ angle (azimuthal angle) in degrees */
    Double_t GetMinTheta() const { return fMinTheta; };
    /** @return Maximum @f$ \theta@f$ angle (azimuthal angle) in degrees */
    Double_t GetMaxTheta() const { return fMaxTheta; };
    
    /** Generate one event, and return it in @a a 
	@param a Return array 
	@param option Not used 
      @return  # of particles made */
    Int_t ImportParticles(TClonesArray* a, Option_t* option="");
    /** Generate one event, and return it
	@param option Not used 
	@return  particles made */
    TObjArray* ImportParticles(Option_t* option="");

    /** Print information 
	@param option Not used */
    void Print(Option_t* option="") const;

  private:
    /** Particle type to make */
    Int_t    fPdgCode;
    /** Minimum number of particles */
    Int_t    fMinM;
    /** Maximum number of particles */
    Int_t    fMaxM;    
    /** Minimum momentum in GeV */
    Double_t fMinP;
    /** Maximum momentum in GeV */
    Double_t fMaxP;
    /** Minimum @f$ \phi@f$ angle (polar angle) in degrees */
    Double_t fMinPhi;
    /** Maximum @f$ \phi@f$ angle (polar angle) in degrees */
    Double_t fMaxPhi;
    /** Minimum @f$ \theta@f$ angle (azimuthal angle) in degrees */
    Double_t fMinTheta;
    /** Maximum @f$ \theta@f$ angle (azimuthal angle) in degrees */
    Double_t fMaxTheta;
    ClassDef(Fixed, 0);
  };
}
#endif
