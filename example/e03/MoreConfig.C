//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/MoreConfig.C
 *  @author  Christian Holm Christensen <cholm@nbi.dk>
 *  @date    Thu Dec  2 01:12:02 2004
 *  @brief   Set up a processes and cuts on simulation backend.
 *
 *  To configure the back-end copy this file to the working directory,
 *  or somewhere early in the ROOT macro path, and configure the
 *  settings there.
 */
void MoreConfig(TVirtualMC* mc)
{
  Printf("Modifying processes and cuts for E03");
  mc->SetProcess("DCAY", 1);  // Decay on 
  mc->SetProcess("PAIR", 1);  // Pair production on 
  mc->SetProcess("COMP", 1);  // Compton scattering on 
  mc->SetProcess("PHOT", 1);  // Photo-electric effect on 
  mc->SetProcess("PFIS", 0);  // Photo-induced fisson off
  mc->SetProcess("DRAY", 1);  // Delta ray production off 
  mc->SetProcess("ANNI", 1);  // e+/-e- annihilation on 
  mc->SetProcess("BREM", 1);  // Brehmsstrahlung on 
  mc->SetProcess("MUNU", 1);  // mu-nucleon interaction on  
  mc->SetProcess("CKOV", 1);  // Cherenkov radiation on
  mc->SetProcess("HADR", 0);  // Hadronic interactions on (GHEISHA)
  mc->SetProcess("LOSS", 1);  // Full energy loss on 
  //mc->SetProcess("LOSS", 4);  // No fluctuations in energy loss on 
  mc->SetProcess("MULS", 1);  // Multiple scattering on 
  mc->SetProcess("RAYL", 0);  // Rayleigh interactions on 
  mc->SetProcess("STRA", 0);  // Energy loss strangling 
  mc->SetProcess("SYNC", 0);  // Syncrotron radiation off 
  mc->SetCut("CUTGAM", 1e-3); // tracking gamma's            1MeV
  mc->SetCut("CUTELE", 1e-3); // tracking e+/e-              1MeV
  mc->SetCut("CUTNEU", 1e-2); // tracking neutral hadrons    1MeV
  mc->SetCut("CUTHAD", 1e-2); // tracking charged hadrons    1MeV
  mc->SetCut("CUTMUO", 1e-2); // tracking mu-/mu+            1MeV
  mc->SetCut("BCUTE",  1e-3); // brehmsstrahlung for e+/e-   1MeV
  mc->SetCut("BCUTM",  1e-3); // brehmsstrahlung for mu+/mu- 1MeV
  mc->SetCut("DCUTE",  1e-3); // delta-ray for e-/e+         1MeV
  mc->SetCut("DCUTM",  1e-3); // delta-ray for other         1MeV
  mc->SetCut("PPCUTM", 1e-2); // e+/e- pair from mu+/mu-     1MeV
  mc->SetCut("TOFMAX", 1e10); // Maximum time                
}
//
// EOF
//
