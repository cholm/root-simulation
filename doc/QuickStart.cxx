//
//   ROOT generic simulation framework
//   Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   doc/QuickStart.cxx
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Examples. */


/** @page quickstart Quick start

    Let us use the code in `example/muon.C` to illustrate how to make
    a simple simulation.

    @section quickstart_det The detector

    The script `Muon.C` defines the detector and how we generate hits
    in that detector.

    First, we include the headers we need.
    
    @dontinclude example/muon/Muon.C
    @skip #ifndef
    @until #include <TTree.h>

    These are pretty straight forward.  We need to know abouht `TGeo`
    classes, the `TVirtualMC` interface, and `TTree` for storage.

    We also include `simulation/Task.h` and `simulation/DefaultHit.h`
    from this project so that we may use them.

    Next, we define our class `Muon` which will define our detector.
    
    @skip class Muon
    @until {

    The class derives from `Simulation::Task` so that we can later
    plug an object of this class into a `Simulation::Main` object.

    We then define our constructor.

    @until }

    Here, we create the default kind of hit array by calling
    Simulation::Task::CreateDefaultHitArray.  This will initialise the
    internal member `fCache` to point o a `TClonesArray` of
    Simulation::DefaultHit.

    Now we can define the member function `Initialize` which will
    define our geometry.
    
    @until Task::Initialize

    It is important that we call the parent class `Initialize` member
    function, so that the initialisation done there is executed.

    Most of our geometry will consist of air, so we then define air as
    a material and tracking medium.
    
    @until medAir

    _Materials_ are more or less what the name implies - either an
    element or some compound of elements.  Air, as we defined it above
    consist of Oxygen (23%), Nitrogen (75%), Carbon (0.01%) and Argon
    (1%).  Note the important 3rd argument to the constructor, which
    is the density of the material
    (@f$\rho=0.00120479\mathrm{g}/\mathrm{cm}^3@f$).  This is given in
    grams per cubic centimetre.

    _Mediums_ are tracking mediums used by the simulation back-end.
    They are defined by a material and a number of paramters (the
    array `pAir`).  These are in order

    - isSensitive    Sensitive volume flag - not really used by VMC
    - fieldTracking  Magnetic field tracking kind. 
      - 0 No field
      - 1 Runga Kutta tracking (Geant 3.21)
      - 2 Helix tracking (Geant 3.21)
      - 3 Helix-3 tracking (Geant 3.21)
      - -1 User selected
    - maxField       Maximum field value in kilogauss
    - maxFieldAngle  Maximum change in angle due to field in degrees/step
    - maxStep        Maximum step size in cm
    - maxEdep        Maximum fractional energy loss in a single step
    - epsilon        Precision for tracking in cm
    - minStep        Minimum step due to continuous process in cm
    
    Now for the active material (polystyrene or
    scintillating plastic) of our detector. 

    @until RegisterMedium

    Note the last call to Simulation::Task::RegisterMedium.  This is
    _crucial_ for the proper operation of the code.  The way the
    simulation backends work, is by keeping track of the current
    _medium_.  Thus, when something happens - e.g., a particle
    deposits energy in a medium, then the backend will alert the
    _manager_ of that medium.  The call
    Simulation::Task::RegisterMedium makes _this_ object the manager
    of the medium `medScint` (Ok, behind the scenes it`s a bit more
    complicated, but this is good enough for now).

    And finally aluminium

    @until medAl

    Note that `medAl` is a pure element and not a mixture like the
    other materials defined.

    Now we have our mediums defined, we can turn to the geometric
    description.  We need to define a shape of the _Cave_ (or _World_)
    - the area in which we want to place the experiment.

    @until caveVol

    Here, `caveShape` defines the shape of the cave volume.  It is
    just a box which is 200 cm wide, 100 cm tall, and 12 m long.  Note
    that we specify _half_-lengths when defining shapes.  Thus, the
    width is specified as `100`, height as `50` and length as `600`.
    The unit of dimensions are always centimetres.

    The object `caveVol` is a _logical volume_.  It ties a shape
    (`caveShape`) and a tracking medium (`medAir`) together into a
    volume.  Thus `caveVol` is a box filled with air.

    We can now define our scintillating plates.

    @until scintVol

    Again, this is simply a box - with half-dimensions 50cm times 25cm
    times 1.25 cm - made out of polystyrene.

    We now have two _logical volumes_ - that is volumes that are
    position nowhere.  We can use these, however, to create _actual_ -
    or _physical_ - volumes.  We will come back to that in a moment.
    First, we define our aluminium plate.

    @until absVol

    This is where the muons will get trapped and decay.

    Before we go on, we set some colours on our materials.  These will
    be propagated to the volumes (logical and physical) automatically.
    We also set the scintillating material to be semi transparent (the
    transpacency argument is in the range 0 to 100, with 0 begin
    opaque, 100 fully transparent).
    
    @until matScint->SetTransparancy

    We want to indicate that the scintillating volumes are sensitive
    volumes (which is half-ways done by registering the medium above),
    which we do with `Simulation::Task::RegisterVolume`.

    @until RegisterVolume

    This means that all _actual_ (placed) scintillating volumes will
    be considered active and we can record hits in those volumes.

    Now we want to make our actual volumes.  We want two scintillating
    plates (`scintVol`) on the bottom at _z_=498.75 cm and _z_=488.75 cm.

    @until scintVol, 2

    This created two _actual_, or _physical_, volumes, placed inside
    the _logical_ `caveVol` volume. They are identified by the copy
    numbers `1` and `2`.  By placing a logical volume inside a logical
    volume, we create a hierarchy of volumes.  That is, here we have
    volume `caveVol` that has two `scintVol` volumes placed _inside_
    it.

    In this example, the hierarchy is 1 deep - that is, all volumes
    are added to the `caveVol` and `caveVol` will be considered the
    world for the simulation.  More complicated geometries can have
    very deep hierarchies.

    Next, we place our single aluminium absorber at _z_=467.5 cm -
    also in the `caveVol` volume.

    @until absVol

    and then finally the two top scintillating plates at _z_=448.75cm
    and _z_=438.75 cm.

    The last thing we do, is to let the geometry consider the
    `caveVol` volume to be the entire world.  We do that by calling
    `TGeoManger::SetTopVolume`.

    @until }

    That finished the initialisation member function.

    Our next member function - `Register` is a lot shorter

    @until }

    In this member function we get the output `TTree` and register our
    `TClonesArray` of Simulation::DefaultHit objects, created in the
    constructor, as a branch in that tree.

    The last an final member function will get called when ever
    something happens in the mediums associated with this object.
    Remember the call to `Simulation::Task::RegisterMedium` above?
    And how this is _crucial_.  Well, if the medium wasn't registered
    to this object, then this next member function - `Step` - would
    never be called.

    @until TVirtualMC

    The first thing we do when we get alerted that something happened
    in our registered medium, is to get a handle to the simulation
    backend `TVirtualMC`.

    We then get the logical volume identifier and physical volume copy
    number using this handle.

    @until vol

    We can now check if the volume hit is indeed sensitive.  Again,
    remember the call to the member function
    `Simulation::Task::RegisterVolume`? This is what makes sure that
    the scintillating volume is considered sensitive.  So here, we
    check that what ever happened happend in some volume we're
    interested in.

    @until IsSensitive

    Having assured ourselves that the volume is sensitive, we can now
    record a hit in this volume.  Because we decided to use
    `Simulation::DefaultHit as our hit structure (in the constructor),
    we can simply call `Simulation::Task::CreateDefaultHit` here.

    @until CreateDefaultHit

    The first argument says, when true, that we would like to store
    the particle, in the output tree, that caused this hit.  The
    second argument says that we also want to store parents, grand
    parents, grand-grand parents, and so on, of the particle that
    caused the hit.  The last flag is, however, a request.  The exact
    settings of the `Simulation::TrackSaver` object used to save the
    tracks to the output tree may limit how deep down the ancestry
    tracks are saved (see Simulation::TrackSaver).

    @until };

    That finishes the detector definition.

    @section muon_config Simulation configuration 

    Configuration, saying what goes into the simulation, is done in
    the script `Config.C`.

    @dontinclude example/muon/Config.C
    @skip g3
    @until Config

    This function takes one argument (default to true) which says
    whether to use a GEANT 3.21 or Geant 4 simulation backend.  We
    will see how that's used later on.

    The next thing is to define our steering Simulation::Main object.

    @until Main

    This is the object that will steer the simulation (up to a point,
    but that's a detail we won't go into here).  Other tasks will be
    added to this object.

    The first task we should add is our event generator.  Here, we use
    Simulation::Generator which encapsulated a `TGenerator` object.

    @until Generator

    We will define our `TGenerator` object a little later.

    Now we create our detector model

    @until Muon

    And finally we define

    - an event display (Simulation::Display),
    - a task to save tracks to the output `TTree`, and
    - a task to create and save our output `TTree` to disk.

    @until hits.root

    Having defined our tasks, we now configure them.

    First, we define a `TGenerator` object - actually the concrete
    implementation Simulation::Fixed which generates particles within
    some narrow parameters.

    @until SetMaxP

    Our generate makes
    - negative muon with
      - a polar angle between 0 and 10 degrees,
      - an azimutal angle between 0 and 360 degrees,
      - exactly 100 of these, and 
      - with momentum in Z between 0.1 GeV/c and 0.3 GeV/c

    We now say that the Simulation::Generator object should use this
    event generator, and that it should produce the particles at
    @f$z=-500\,\mathrm{cm}@f$.

    @until SetVertex

    Note that it is important that the production vertex is within the
    top volume defined in the geometric description of the experiment.

    To make our event display a little nicer, we define a TLatex
    object which we add to the display task.  This will be drawn in
    the display window together with the event.

    @until AddDraw

    As mentioned when we went through the `Muon.C` file, we have
    requested that parents, grand parents, etc. of tracks that produce
    hits, are kept in the output file.  We now say that we only want
    to keep at most 10 levels of that ancestry.

    @until SetMaxDepth
    
    The next thing to do, is to create our backend.  This is where the
    argument to the Config function comes in.

    @until SetVMCConfig

    If the argument was true, then we let the ROOT interpreter create
    the GEANT 3.21 backend by calling the script `G3Config.C`.
    Otherwise, we create a Geant 4 backend via the script
    `G4Config.C`.

    Finally, we add all our task to the `main` task,

    @until writer

    The order in which the tasks are added is _important_.

    - The generator should always be first,
    - followed by detector definitions,
    - and then the
      - event display
      - track saver
    - and the tree writer must be last

    We do a bit more configuration, which is to set the verbosity and
    debug levels on the `main` object.  This will propagate down, so
    if you want to set different levels for a specific task, then that
    should happen afterwards (e.g., the commented `stack->SetDebug(5)`
    will override the general debug level set).

    @until stack

    It can be useful to save the geometric description to a file so we
    can use it later.  We do that next.
    
    @until SaveGeometry

    The last part of the script mainly serve as reminders to the user.

    If we're _not_ in batch mode, then we open a `TBrowser`, and print
    a friendly message on how to proceed.

    @until }
    
    We also ask the application to keep intermediate tracks so that
    the event display may show these.

    If we _are_ in batch mode, we assume that the caller will start
    the simulation (see more below) and we _do not_ keep intermediate
    tracks as we will not use the event display (the event display is
    automatically turned off in batch mode).

    @until }

    That ends the configuration of the simulation.

    @section muon_run Running the simulation

    There are a number of different ways to run the simulation.

    @subsection muon_run_fwmain fwmain
    
    One is to use the program `fwmain` from the `root-framework`
    project.  In that case, one would do in the terminal

        $ fwmain -l Muon.C Config.C -n 10

    to make 10 events with the default GEANT 3.21 backend.  This will
    run in non-batch mode, and the event display will be shown.  To
    generate 1000 events, using the Geant 4 backend in batch mode, one
    can do

        $ fwmain -l Muon.C Config.C\(0\) -n 1000 -b

    @subsection muon_run_script Run.C
    
    Another option is to use the provided `Run.C` script.  Let's just
    go through that quickly.

    @dontinclude example/muon/Run.C
    @skip Build
    @until }

    This function simply AcLIC compiles a script, preventing ROOT from
    triggering a bug, and giving some nice output.

    @until SetBatch

    The function `Run` takes a number of arguments

    - `nev` number of events to make,
    - `g3` if true use GEANT 3.21, otherwise Geant 4, and 
    - `notbatch` if true, do not go to batch mode even if `nev` is larger than 0.

    Next, we compile our `Muon.C` script (using AcLIC) with some nice
    output around it.

    @until endl

    Then we execute our configuration `Config.C`

    @until gROOT

    If the number of events to generate was 0 or less, we give a
    friendly reminder of how to run the simulation and drop to the
    ROOT prompt.

    @until ;
    
    Finally, if we gave a positive number of events to make, we start
    the simulation by calling Simulation::Main::Instance()->Loop with
    the number of events to make as argument.

    @until }

    We can execute this script as

        $ root Run.C

    to make 10 event with GEANT 3.21 in batch mode.  To make 3 events
    with Geant 4 in interactive mode, we can do

        $ root Run.C\(3,0,1\)

    or 20 events with Geant 4 in batch mode

        $ root Run.C\(20,0\)

    or 100 events with GEANT 3.21 in interactive mode

        $ root Run.C\(100,1,1\)

    @section muon_ana Analysing the output

    An example of analysing the output of the above simulation is
    given in the script

    - Analyser.C - this defines the class `Analyser` which is a task,
      that does the actual analysis.  What is important to note, is
      that it retrives the data via the `TFolder` hierarchy.

    - Analysis.C - an analysis job configuration like `Config.C` is a
      configuration of the simulation.  Here, one should notice the
      use of the tasks

      - `Framework::TreeReader` to open the input tree,

      - `Simulation::DefaultHitReader` to read hits in the active
        volumes in to a `TFolder` names `Muon`,
	
      - `Simulation::TrackReader` which reads stored tracks into the
        folder `Tracks`, and finally
      
      - our analysis task `Analyser`.

      The rest of the script adds these objects to the
      `Framework::Main` object.

    We can run the analysis like we did the simulation using `fwmain`

        fwmain -l Analyser.C Analysis.C -n -1

   (negative argument to option `-n` means read as many events as
   possible).  or with the script `Analyse.C` which is entirely
   parallel to the `Run.C` script described above.
 */
//
// EOF
//

