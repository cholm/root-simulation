// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Generator.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Simulation::Generator
*/
#ifndef Simlation_Generator
#define Simlation_Generator
#include <simulation/Task.h> 
#include <TLorentzVector.h>
class TGenerator;
class TObjArray;

namespace Simulation 
{
  /** @defgroup sim_special_tasks Specialised tasks 
      @ingroup sim_tasks 
      @brief This group contains some specialised tasks that the user
      can add.  */
  /** @class Generator simulation/Generator.h <simulation/Generator.h>
      @brief Specialised task for event generators.  
      @ingroup sim_special_tasks

      This class uses an external TGenerator to create primary
      particles and push them onto the stack.  One could use this like 
      @code 
      gSystem->Load("libEGPythia.so");
      Simulation::Generator* generator 
        = new Simulation::Generator("pythia", new TPythia6);
      @endcode 
  */
  class Generator : public Task 
  {
  public:
    /** Create a generator task. */
    Generator();
    /** Create a generator task. 
	@param name The name of the generator task 
	@param title The title of the generator task */
    Generator(const Char_t* name, const Char_t* title);
    /** Create a generator task. 
	@param name The name of the generator task 
	@param g Pointer to external generator code k */
    Generator(const Char_t* name, TGenerator* g);
    /** Destruct the generator task - does nothing. */
    virtual ~Generator() {}

    /** Browse the generator task 
	@param b browser to use */
    virtual void Browse(TBrowser* b);
    
    /** Set the external generator code 
	@param g The external generator to use  */
    void SetGenerator(TGenerator* g) { fGenerator = g; }
    /** Get the external generator code 
	@return The external generator code  */
    TGenerator* GetGenerator() const { return fGenerator; }

    /** Make a vertex.  By default, the vertex coordinates, including
	time, is normal distributed around the specified mean with the
	specified spreads.  To change this, derive from this class and
	override this member function.
	
	@param v 4-vector to write vertex in (cm,cm,cm,s) */
    virtual void MakeVertex(TLorentzVector& v);
    
    /** Set the vertex.
	@param x X-coordinate in centimetres
	@param y Y-coordinate in centimetres 
	@param z Z-coordinate in centimetres 
	@param t T-coordinate in centimetres */
    void SetVertex(Double_t x=0, Double_t y=0, 
		   Double_t z=0, Double_t t=0); //*MENU*
    /** Set the vertex.  Units are centimetres.   
	@param v Vertex */
    void SetVertex(const TLorentzVector& v) { fV = v; }
    /** Set the vertex spread.
	@param x Spread in X-coordinate in centimetres 
	@param y Spread in Y-coordinate in centimetres 
	@param z Spread in Z-coordinate in centimetres 
	@param t Spread in T-coordinate in centimetres */
    void SetVertexSigma(Double_t x=0, Double_t y=0, 
			Double_t z=0, Double_t t=0); //*MENU*
    /** Set the vertex spread.  Units are centimetres and seconds. 
	@param v Vertex spread (cm,cm,cm,s) */
    void SetVertexSigma(const TLorentzVector& v) { fSigmaV = v; }
    
    /** Create primary particles and push them on the stack for
	tracking. */
    virtual void GeneratePrimaries();
    
  protected:
    /** Pointer to generator code */
    TGenerator* fGenerator;
    /** Generator vertex */
    TLorentzVector fV;
    /** Generator vertex spread*/
    TLorentzVector fSigmaV;
  public:
    ClassDef(Generator,1) // Generator in Simulation framework
  };
}

#endif
