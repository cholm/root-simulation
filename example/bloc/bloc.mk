ROOT_CLING	= rootcling
ROOT_CPPFLAGS	= $(filter -I%, $(shell root-config --cflags)) 	\
		  $(filter -D%, $(shell root-config --cflags)) 
ROOT_CXXFLAGS	= $(filter-out -I%, $(filter-out -D%, 		\
			$(shell root-config --cflags)))
ROOT_LDFLAGS	= $(shell root-config --ldflags)
ROOT_LIBS	= $(shell root-config --noldflags --libs)
SIM_CPPFLAGS	= $(shell simulation-config --cppflags)
SIM_LDFLAGS	= $(filter-out -l%,$(shell simulation-config --ldflags))
SIM_LIBS	= $(shell simulation-config --libs)
VMC_CPPFLAGS	= -I$(shell root-config --incdir)/vmc

CXX		= g++ -c
CXXFLAGS	= $(ROOT_CXXFLAGS) -fPIC
CPPFLAGS	= $(ROOT_CPPFLAGS) $(SIM_CPPFLAGS) $(VMC_CPPFLAGS)
SO		= g++ -shared
SOFLAGS		= $(ROOT_LDFLAGS) $(SIM_LDFLAGS)
LIBS		= $(SIM_LIBS)

MODULE		= Example
MODULE_PCM	= $(MODULE)_rdict.pcm
MODULE_MAP	= $(MODULE).rootmap

HEADERS		= BlocBuilder.h		\
		  BlocSimulator.h	\
		  ECalBuilder.h		\
		  Gun.h			\
		  Hit.h			\
		  LatrBuilder.h		\
		  LeakBuilder.h		\
		  Profiler.h		\
		  Linkdef.h

SOURCES		= BlocBuilder.cxx	\
		  BlocSimulator.cxx	\
		  ECalBuilder.cxx	\
		  Gun.cxx		\
		  Hit.cxx		\
		  LatrBuilder.cxx	\
		  LeakBuilder.cxx	\
		  Profiler.cxx		

%.o:%.cxx
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $<

Example.so:Example_dict.o $(SOURCES:%.cxx=%.o)
	$(SO) $(SOFLAGS) -o $@ $^

Example_dict.cxx:$(HEADERS)
	$(ROOT_CLING) -f $@ \
		-s $(MODULE).so \
		-rml $(MODULE).so \
		-rmf $(MODULE_MAP) \
		-DCLING $(SIM_CPPFLAGS) $^


clean:
	rm -f *.o *~ *_dict.* Example.so *.rootmap *.pcm
