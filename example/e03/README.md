# Example E03 

This is from [Geant 4
VMC](https://github.com/vmc-project/geant4_vmc.git). 

## The setup 

A calorimeter of lead slats interspaced with aerogel volumes. 

The particle generator can make different kinds of particles.  The
base class `Gun` takes care of most stuff, while derived classes (such
as `Default` or `Anti`) sets-up the specific particle production. 

## Demonstrates 

- Divided volumes 
- Sensitive volumes 
- Hadronic processes off 
- Particle cuts 
  - G4: cut in range 1mm 
  - G3: corresponding cuts 
- Calorimeter hits and storing those 
- Different particle generators 





