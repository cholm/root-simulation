// -*- mode: C++ -*-
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Task.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Simulation::Task
*/
#ifndef Simulation_Task
#define Simulation_Task
# include <framework/Task.h>
# include <TVector3.h>
# include <TArrayI.h>
# include <simulation/Field.h>
class TGeoMedium;
class TGeoVolume;
class TBrowser;

namespace Simulation 
{
  class DefaultHit;
  
  /** @brief Bits for track flags.  
      @ingroup sim_vmc */
  enum {
    /** Keep this track */
    kKeepBit      = 1, 
    /** This track has daughters */
    kDaughtersBit = 2, 
    /** We're done tracking this track */
    kDoneBit      = 4,
    /** Request to keep parents */
    kKeepParentsBit  = 8
  };
  /** @defgroup sim_tasks Simulation tasks 
      @brief Simulation tasks 

      A task can in general be one of many things: 
      <dl>
        <dt> A %Generator </dt>
	<dd> A generator tasks job is to create primary particles for
	  transport through the geometry.  A specialies sub-class
	  exists for exactly this purpose 
	  (@link Generator Generator @endlink). 
	</dd>
	<dt> A Detector </dt> 
	<dd> A detector task should define mediums needed by that
	  detector by overloading the member function Initialize.
	  It should also define a geometry of the detector, and assign
	  a unique tracking medium to each active volume in the
	  detector, bu overloading the member function Register.  A
	  detector task should also overload the Step member function
	  to deal with hits generated my simulation backend in an
	  active volume.  
	</dd>
      </dl>
      <b>Important</b>: The VMCs (TGeant3 and TGeant4) will _explicitly_
      assume that the geometry is specified in ROOT units (cm, s, GeV)
      irrespective of the underlying unit system used by the backend.
      
      All kinds of tasks can overload various member functions to do
      various things at different stages of the simulation.  
      @verbatim 
        Contruction  (usally done in a configuration script)
	 |
	Initialization   (member function Initialize - should define 
	 |                materials, tracking mediums, geometry, and folders.
	 |                This also creates the simulation back-end via the
	 |                specified VMC configuration script.) 
	 |
	<<Define particles>> (member function DefineParticles, via backend) 
         |
        <<Registration>> (member function Register - should define output
         |                branches, and can modify specific medium parameters.
         |                Done via simulation backend)
         |
	<<Run>>          (done by simulation backend)
	 | |
	 | +- Begin event  (member function - BeginEvent)
	 | |   |
         | |  Generate primaries (member function GeneratePrimaries -
         | |   |                  should make primary particles and
	 | |   |                  push them onto the simulations stack
	 | |   |                  for transport through the geometry -
	 | |   |                  usually done by (sub classes of)
	 | |   |                  specialised task Generator)
	 | |   |
         | |   +- Before primary     (member function BeginPrimary) 
	 | |       |
	 | |       +- <<Transport Primary>> (done by simulation backend - 
	 | |       |   |                     This may produce secondaries, 
	 | |       |   |                     which are then transported).
	 | |       |   |
	 | |       |   +- Begin Track (member function PreTrack)
	 | |       |   |
         | |       |   +- Make hits (member function Step - should deal
         | |       |   |               with hits in active volumes)
	 | |       |   |
	 | |       |   +- End Track (member function PostTrack)
	 | |       |        
	 | |       +- End primary   (member function FinishPrimary)
	 | |        	     
	 | +- End Event             (member function FinishEvent)
         | 
        End of run                  (member function Finish)  
      @endverbatim
  */
  /** @class Task simulation/Task.h <simulation/Task.h>
      @ingroup sim_tasks
      @brief A task in a simulation run 

      This class abstracts the job of a task in the simulation run. 
   */
  class Task : public Framework::Task 
  {
  public:    
    /** The various simulation choirs */
    enum {
      /** Call Simulation::Task::DefineParticles */
      kDefineParticles   = 'd',
      /** Call Simulation::Task::GeneratePrimaries */
      kGeneratePrimaries = 'g',
      /** Call Simulation::Task::BeginEvent */
      kBeginEvent        = 'e',
      /** Call Simulation::Task::BeginPrimary */
      kBeginPrimary      = 'p',
      /** Call Simulation::Task::BeginTrack */
      kBeginTrack        = 't',
      /** Call Simulation::Task::Step */
      kStep              = 's',
      /** Call Simulation::Task::Field */
      kField             = 'F',
      /** Call Simulation::Task::FinishTrack */
      kFinishTrack       = 'T',
      /** Call Simulation::Task::FinishPrimary */
      kFinishPrimary     = 'P',
      /** Call Simulation::Task::FinishEvent */
      kFinishEvent       = 'E'
    };

    /** Constructor 
	@param name Name of the task 
	@param title Title of the task */
    Task(const Char_t* name="", const Char_t* title="");
    /** Destructor */
    virtual ~Task() {}

    /** This member function should define the needed tracking mediums
	and register them with the main task, so that the main task
	can map active tracking medium to a task.   The default
	implementation makes a folder in the parent tasks folder and
	puts the cache in that folder. 
	
	An example could be 
	@dontinclude example/bloc/BlocSimulator.cxx 
	@skip BlocSimulator::Initialize
	@until }
    */
    virtual void Initialize(Option_t* option="") 
    { 
      Framework::Task::Initialize(option); 
    }
    /** This member function should define the geometry of the task.
	It should also create a branch (or similar) in the output tree
	if needed. 

	An example could be 
	@dontinclude example/bloc/BlocSimulator.cxx
	@skip BlocSimulator::Register
	@until }
    */
    virtual void Register(Option_t* option="") { (void)option; }
    /** Execute this task 
	@param option Not really used */
    virtual void Exec(Option_t* option="");
    /** This member function is supposed to define additional
	particles that may be needed for the simlation.  For most
	tasks the default implementation (which does nothing) should
	be OK.   A specialised task, say @c ParticleTypes could be
	made to define the needed particles: 
	@code 
	class ParticleType : public Simulation::Task
	{ 
	public:
	  void DefineParticles() { 
	    TVirtualMC* mc = TVirtualMC::GetMC();
	    mc->DefineParticle(211, "pi+", kPTHadron,.139,+1,0);
	    ...
          }
        };
	@endcode
     */
    virtual void DefineParticles() {}
    /** This member function is supposed to generate primary particles
	and put them on the MC stack for transport through the
	geometry.  The specialiesed task `Generator' (and it's derived
	classes) usually takes care of this, so for most tasks the
	default implementation should be OK. */
    virtual void GeneratePrimaries() {}
    /** This message is recieved at the begining of an event.  The
        deault implementation clears the internal cache. */
    virtual void BeginEvent();
    /** This message is recieved at the begining of tracking a primary
        particle. */
    virtual void BeginPrimary() {  }
    /** This message is recieved at the begining of tracking a particle. */
    virtual void BeginTrack() {  }
    /** This member function is called each time a hit is generated in
	the tasks registered tracking mediums.   The task should
	overload this member function to store the hits, if needed, in
	the cache. 

	An example could be: 
	@dontinclude example/bloc/BlocSimulator.cxx
	@skip BlocSimulator::Step
	@until }
     */
    virtual void Step() {}
    /** This member function is called each time the MC backend needs
	to know the magnetic field at a given position.  
	This member function is only called for the task that holds
	the currently active medium.    The argument @a x holds the
	current space point for which the magnetic field is needed.
	The magnetic  field should be returned in @a b in units of
	kilo Gaus (@f$ 10^4 \mbox{gauss} = 1 \mbox{T}@f$)
	@param x Space point where the magnetic field should be
	evaluated. 
	@param b On return, it should hold the magnetic field at @a x
	in units of kilo Gauss. 
     */
    virtual void Field(const TVector3& x, TVector3& b);
    /** This message is recieved at the end of tracking a particle. */
    virtual void FinishTrack() {  }
    /** This message is recieved at the end of tracking a primary
	particle. */ 
    virtual void FinishPrimary() {  }
    /** This message is recieved at the end of an event. */
    virtual void FinishEvent() {  }
    /** This member function is called once for each defined task at
	the end of the simulation run. Tasks can overload this member
	function to do final summary histograms or the like.  
        @param option Option to pass */
    virtual void Finish(Option_t* option="") { (void)option; }

    /** Set the magnetic field object */
    void SetField(Simulation::Field* f) { fField = f; }

    /** Make this task active, in the sense that it produces hits and
	so on.  If this is set to false, the Step is never called for
	this task, though Field might. 
	@param on Whether to turn on this task or not. */
    void SetActive(Bool_t on=kTRUE); //*TOGGLE* *GETTER*=IsActive
    /** @return True if task does stepping */
    Bool_t IsActive() const { return fActive; }

    /** Browse this task. 
	@param b The browser to use */
    virtual void Browse(TBrowser* b);
  protected:
    /**@{*/
    /** @name Register mediums */    
    /** Register a medium as belonging to this task.  This should only
	used if the task makes mediums via TGeoMedium
	@param medium Pointer to medium object
	@return  true on success  */
    Bool_t RegisterMedium(TGeoMedium* medium);
    /** Register a medium as beloning to this task.  This should be
	used only if the medium was made using @c TVirtualMC::Gstmed
	@param id The medium ID
	@return true on success */
    Bool_t RegisterMedium(Int_t id);
    /**@}*/
    /**@{*/
    /** @name Register sensitive volumes, and check if a volume is 
	sensitive */    
    /** Register a volume as a senstive volume.  If this member
	function is called for @a id, then calling the member function
	IsSensitive with an argument of @a id will return @c true.
	This is usefull for figuring out if one is in a sensitive
	volume or not in the stepping function. Note, that it's use is
	entirely optional.  
	@param id Volume ID to register as an active volume */
    void RegisterVolume(Int_t id);
    /** Register a volume as a senstive volume.  If this member
	function is called for @a volume, then calling the member
	function IsSensitive with an argument of @a id corresponding
	to @a volume, then it will return @c true.
	This is usefull for figuring out if one is in a sensitive
	volume or not in the stepping function. Note, that it's use is
	entirely optional.  
	@param volume Volume to register as an active volume */
    void RegisterVolume(TGeoVolume* volume);
    /** Check if the volume corresponding to @a id has been registered
	as a senstive volume, by the user callig RegisterVolume.   The
	user can use this member function in Step to skip volumes that
	have not been registered. Note, that it's use is entirely
	optional. 
	@param id Volume to check
	@param idIsMC If true (default) assume the volume ID is given
	by the MC, not TGeo.
	@return @c true if the volume has been registered as
	sensitive.  @c false otherwise. */
    Bool_t IsSensitive(Int_t id, Bool_t idIsMC=true);
    /** List of senstive volume IDs. */
    TArrayI fSensitive;
    /** List of registed volumes */
    // TList fVolumes; //!
    /**@}*/
    /**@{*/
    /** @name Hit array and hit creation */    
    /** Create the default hit array - that is, a TClonesArray of
	Simulation::Hits.  Users can call this in the construtor or
	the derived class, and should then also call
	Simulation::Task::Initialize and Simulation::Task::Register
	in the corresponding overloaded functions. This is useful if
	the simulation is a simple one, and one does not need
	additional information in the hits.  The use of this member
	function is entirely optional. */
    void CreateDefaultHitArray();
    /** Create a default kind of hit. That is, create a
	Simulation::DefaultHit in the hit array.  Note, that the user
	should have called CreateDefaultHitArray for this to work.
	The hits created are very simple. The use of this member
	function is entirely optional.

	@param keepParticle If true, keep particle.

	@param keepParents If true, ask track saver to save the parents
	of this track.  Note that the track saver may limit the depth
	to which parents are saved.  
    */ 
    DefaultHit* CreateDefaultHit(Bool_t keepParticle=true,
				 Bool_t keepParents=false);
    /**@}*/
    /** Wether this task is active, in the sense that it's making hits
	and similar */ 
    Bool_t fActive;
    /** Pointer to current field.  This object does not own thie
	pointed to object. */
    Simulation::Field* fField;
  public:
    ClassDef(Task, 2) // Task
  };
}

#endif 
