// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: Linkdef.h,v 1.5 2005-12-13 18:56:09 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/Linkdef.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:24:27 2004
    @brief   Link specification for CINT
*/

#if !defined(__CINT__) and !defined(CLING)
#error Not for compilation
#endif

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace Example;
#pragma link C++ class     Example::ECalBuilder+;
#pragma link C++ class     Example::LeakBuilder+;
#pragma link C++ class     Example::LatrBuilder+;
#pragma link C++ class     Example::BlocBuilder+;
#pragma link C++ class     Example::BlocSimulator+;
#pragma link C++ class     Example::Gun+;
#pragma link C++ class     Example::Hit+;
#pragma link C++ class     Example::Profiler+;


//____________________________________________________________________ 
//  
// EOF
//
