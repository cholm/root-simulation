//
// $Id: Usage.cxx,v 1.7 2006-11-07 23:49:21 cholm Exp $
//
//   ROOT generic simulation framework
//   Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   doc/Usage.cxx
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Usage of library documentation. */
/** @page usage Using the Simulation framework

    Running a simulation is quite simple.  Generally, one writes a
    configuration script, say @c Config.C, where one defines what
    should be done in the simulation.  The configuration script is a
    normal ROOT script, which can be interpreted by the ROOT C++
    interpretor CINT.  Ofcourse, one can compile this script, but in
    general it shouldn't be necessary, as the script is interpreted
    once during startup, and then never again.  Hence, the overhead of
    interpreting the script is negligible, and the loss of
    configurability is often a greater disadvantage. 

    @section setup Making a configuration script 

    First, one needs to create an object of the class
    Simulation::Main, and then add objects of sub classes of
    Simulation::Task to that object.  Tasks can be nested inside other
    tasks by calling Framework::Task::Add.  The simplest and most
    flexible way to set up a simulation run, is to create a
    configuration script (see
    @link example/bloc/Config.C @c Config.C @endlink)

    @dontinclude example/bloc/Config.C 
    @skip void Config
    @until Simulation::Main*
    
    Here, the Simulation::Main object is created on the heap.

    @until new Simulation::TreeWriter
    
    Next, we instantise the tasks we need.  In the example above,
    we're building a simple calorimeter detector (based on the GEANT
    3.21 <a
    href="http://wwwasd.web.cern.ch/wwwasd/geant/tutorial/tutstart.html">tutorial</a>).
    Note, that we have added a task to write a @c %TTree to a file
    (Simulation::TreeWriter) and a task to do event display 
    (Simulation::Display). 
    
    (All the code of the above example is shown in @ref examples )

    @skip Add(gun)
    @until Add(writer) 

    Next, we add the tasks to the Simulation::Main object, in the
    order we want.  Note, that the order is significant.  The
    generator should go first, and the writer close to the end, to
    allow proper processing. 

    @until SetDebug 

    We add our analysis task (Example::Profiler), and set the debug
    and verbosty level of all the tasks.

    @until gROOT

    We then instantise our simulation back-end - either GEANT 3.21 or
    Geant 4.  We use the scripts provided by the package.  If we want
    to change settings on the simulation backend, then we should copy
    the relevant script (G3Config.C) or (G4Config.C) to our working
    directory and edit the script there.

    @until }

    Finally, we check if we're in batch mode.  Otherwise, we start a
    browser and add our main object to it so that we may interactively
    interact with it.
    
    @section run Running the simulation 

    One can load and execute this script in an interactive ROOT
    session 

    @code 
    Root> .x Config.C 
    @endcode 

    This will setup the geometry and return, ready to create events.  
    If one does 
    @code 
    Root> Simulation::Main* main = Simulation::Main::Instance();
    Root> main->Loop(5);
    @endcode 
    one will create 5 simulated events.  

    Note, that @c Display task halts execution after each event, and
    then displays the current event on the screen.  The event display
    looks like

    @image html display.png

    Via the menu, we can open the OpenGL viewer, and playing with the
    opacity if the elements in the scene, and setting a cut plane, one
    can get a really nifty view of the detector and event 

    Note, that you have to press @b Continue to make more events.
    When the display shows the event, the main task is idleing in the
    Display task, and no other action can be taken until we continue.  

    The event display is only shown if the @c %TVirtualMC supports a
    ROOT defined geometry. 

    @image html display3dcut.png

    @section browser Using the browser 

    Once the configuration script has been parsed by ROOT, one can
    open up a browser to control the simulation from there.  The
    context menus of the various objects gives various control
    options.   Using the example above, and browsing the @c example
    tree, we see 

    @image html browser_geom.png

    As show above, we can use the context menu to draw the defined
    geometry, make some of the geometry invisible (not in-active!),
    and so on.  Note also the ticks next to the volumes in the
    geometry.  These control the visibility of the corresponding
    volume.   Note also the ticks next to the tasks.  These control
    whether the corresponding task produces hits or not (toggles
    Simulation::Task::IsActive()).  

    We can now create event by using the context menu of our 
    Simulation::Main object (@c example in our case).  The entry to
    look for is @b Trigger 

    @image html browser_ctrl.png

    This will create one event.  Note, to finish the run, one @e must
    call Simulatio::Main::ExecFinish - either from the context menu or
    via the command line. 

    Note, that you can control many things from the browser.   You
    could for example change parameters of the generator task or
    object, or you can make tasks (in)active, and so forth.  Note
    however, that you can @e not change the geometry after it has been
    closed (done in Simulation::Main::ExecInitialize)

    Note, that you have to press @b Continue to make more events.
    When the display shows the event, the main task is idleing in the
    Display task, and no other action can be taken until we continue.  

    @section extention Exploiting the Configuration Script 

    Using configuration scripts like this provides an enomours
    flexibility.  A collaboration may define a number of official
    tasks that the user can put into the job.  However, a user could
    also develop his/her own tasks, and in her configuration script
    load a library containg these tasks, and add them to the job.  In
    this way, people can easily and quickly develop new official tasks
    without rebuilding the collaborations full code.   

    This is possible as the configuration script defines everything
    that's in the job, and because the framework provides many hooks
    into the running job.   A Simulation::Task needn't correspond to a
    physical detector, event generator or other normal simulation
    tasks.  A user defined task could do diagnostics histograms,  add
    additional particles to showers, or even digitisation of the
    simulated data - the possibilities are endless and your only
    limitation is the very generalised interface the framework
    provides.   And the best thing is, that because of ROOTs ability
    to dynamically load code, you can easily develop new tasks without
    interfering with existing code. 

    A user could for example plug in in an afterburner code to do
    two-particle correlations in the generated event, or plug in a task
    that pushes the event information into a tape storage, and so on. 

    Users can even subsitute official tasks with their own derived
    tasks that does more or less which ever is best suited for the
    users needs.  For example, a TPC developer may need an abundance
    of information on hits in the central TPC, while a TOF developer
    only needs to have the right medium information for the TPC.  The
    TPC developer could then make a sub class of the official TPC task
    that keeps track of additional information, while the TOF
    developer will use a simplified TPC task. 

    @note A similar approach was taken in the 
    <a href="http://www4.rhic.bnl.gov/brahms/WWW">BRAHMS</a>
    collaboration and proved very effective and fruitful. 

    @section postprocess Post-Processing of the Simulation Output

    If the collaboration uses a similar approach for digitization,
    event reconstruction, analysis and so on, users will have a very
    uniform and flexible interface for all parts of the collaboration
    and personal work.

    The example task @c Example::Profiler
    (@link example/bloc/Profiler.h Profiler.h @endlink and 
    @link example/bloc/Profiler.cxx Profiler.cc @endlink) is an example of
    using tasks derived from Framework::Task to do post-analysis of
    the simulated data.   The example configuration script 
    @link example/bloc/Analysis.C Analysis.C @endlink uses this class to make
    profile histograms of the showers in the calorimeter described
    above
    @dontinclude example/bloc/Analysis.C
    @skip Analysis()
    @until }
    Note the use of the general classes Framework::ArrayReader to read
    back the data stored in the output file of the simulation.  This
    class reads a TClonesArray branch from the specified input tree
    and puts the data in a TFolder.   The class @c Example::Profiler
    then gets the data from these folders and processes them 
    @dontinclude example/bloc/Profiler.cxx
    @skip Exec 
    @until fN++
    

*/

#error Not for compilation 
//
// EOF
//
