//____________________________________________________________________ 
//  
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/DefaultHitReader.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for DefaultHitReader
*/
#include <simulation/DefaultHitReader.h>
#include <simulation/DefaultHit.h>
#include <TTree.h>
#include <TParticle.h>
#include <TClonesArray.h>
#include <TFolder.h>

//____________________________________________________________________
ClassImp(Simulation::DefaultHitReader);

//____________________________________________________________________
Simulation::DefaultHitReader::DefaultHitReader(const char* name,
					       const char* branchName,
					       const char* treeName) 
  : Framework::ArrayReader(name, "Simulation::DefaultHit",
			   branchName, treeName)
{
}
//____________________________________________________________________
void
Simulation::DefaultHitReader::Initialize(Option_t* option)
{
  Framework::ArrayReader::Initialize(option);
}
  
//____________________________________________________________________
void
Simulation::DefaultHitReader::Register(Option_t* option) 
{
  ArrayReader::Register(option);
}

//____________________________________________________________________
void
Simulation::DefaultHitReader::Exec(Option_t* option)
{
}
  
//____________________________________________________________________
//
// EOF
//
