//____________________________________________________________________ 
//  
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/TrackSaver.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for TrackSaver
*/
#include <simulation/TrackSaver.h>
#include <simulation/Main.h>
#include <simulation/Application.h>
#include <simulation/Stack.h>
#include <TTree.h>
#include <TParticle.h>
#include <TClonesArray.h>
#include <map>

//____________________________________________________________________
ClassImp(Simulation::TrackSaver);

//____________________________________________________________________
Simulation::TrackSaver::TrackSaver(const char* name,
				   const char* treename,
				   Bool_t      keepAll,
				   UInt_t      maxDepth) 
  : Simulation::Task(name, treename),
    fKeepAll(keepAll),
    fMaxDepth(maxDepth)
{
  fMap = new TClonesArray("TObject");
}
//____________________________________________________________________
void
Simulation::TrackSaver::Register(Option_t* option) 
{
  Debug(5, "Register", "Setting branch for tracks");
  Simulation::Main*        main  = Simulation::Main::Instance();
  Simulation::Application* app   = main->GetApplication();
  Simulation::Stack*       stack = app->GetStack();
  fCache                         = stack->GetParticles();
  Verbose(0,"Register","Got stack cache %p",fCache);
  
  TTree* tree = 0;  
  if ((tree = GetBaseTree(GetTitle()))) {
    const char* name = GetName();
    fBranch          = tree->Branch(name, &fCache);
    tree->SetAlias(Form("%sMech",name),Form("%s.fUniqueID",name));
    tree->Branch(Form("%sMap",name),&fMap);
  }
}
//____________________________________________________________________
void
Simulation::TrackSaver::BeginEvent()
{
  Simulation::Task::BeginEvent();
  fMap->Clear();
  Debug(3,"BeginEvent", "Clearing cache and map");
}

//____________________________________________________________________
void
Simulation::TrackSaver::FinishEvent() 
{
  Debug(2, "FinishEvent", "Cleaning particle stack");
  TClonesArray* cache = static_cast<TClonesArray*>(fCache);

  std::map<TParticle*,Int_t> mapping;
  Int_t n = cache->GetEntriesFast();
  Debug(2, "FinishEvent", "%d particles on the stack (keep all? %s)",
	n, (fKeepAll ? "yes" : "no"));

  if (not fKeepAll && fMaxDepth > 0) {
    Debug(5,"FinishEvent","Marking parents to be saved at depth %d",fMaxDepth);
    for (Int_t i = 0; i < n; i++) {
      TParticle* particle = static_cast<TParticle*>(cache->At(i));
      if (!particle)                           continue;
      if (!particle->TestBit(kKeepBit))        continue;
      if (!particle->TestBit(kKeepParentsBit)) continue;
      KeepParent(particle->GetFirstMother(),  fMaxDepth);
      KeepParent(particle->GetSecondMother(), fMaxDepth);
    }
  }
  
  for (Int_t i = 0; i < n; i++) {
    TParticle* particle = static_cast<TParticle*>(cache->At(i));
    if (!particle) continue;

    if (!fKeepAll and !particle->TestBit(Simulation::kKeepBit)) {
      Debug(10, "FinishEvent", "Removing particle @ %d -> %s", 
	    i, particle->GetName());
      cache->RemoveAt(i);
    }
    else
      mapping[particle] = i;
    
    TObject* m = new ((*fMap)[i]) TObject();
    m->SetUniqueID(0xFFFFFFFF);
  }
  cache->Compress();

  n = cache->GetEntriesFast();
  Debug(2, "FinishEvent", "After cleaning, %d particles on the stack", n);
  for (Int_t i = 0; i < n; i++) {
    TParticle* particle = static_cast<TParticle*>(cache->At(i));
    auto it = mapping.find(particle);
    if (it == mapping.end()) continue;

    Int_t trackNo = it->second;
    // trackNo maps to index i
    (*fMap)[trackNo]->SetUniqueID(i);
  }
}

//____________________________________________________________________
void
Simulation::TrackSaver::KeepParent(Int_t i, UInt_t lvl)
{
  if (i < 0) return;
  Debug(10,"KeepParent","Parent %6d at lvl=%2d (max=%2d)",i,lvl,fMaxDepth);
  if (lvl < 1) {
    if (fDebug >= 5) {
      Warning("KeepParent",
	      "At max depth=%d, there are still parent particles "
	      "that could be saved", fMaxDepth); 
    }
    return;
  }
  
  TClonesArray* cache    = static_cast<TClonesArray*>(fCache);
  TParticle*    particle = static_cast<TParticle*>(cache->At(i));
  if (!particle) return;

  particle->SetBit(Simulation::kKeepBit,lvl > 0);

  KeepParent(particle->GetFirstMother(),  lvl - 1);
  KeepParent(particle->GetSecondMother(), lvl - 1);
}


//____________________________________________________________________
//
// EOF
//
