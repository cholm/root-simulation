//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Stack.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Simulation::Stack
*/
#include "simulation/Stack.h"
#include "simulation/Application.h"
#include "simulation/Task.h"
#include <TDatabasePDG.h>
#include <TClonesArray.h>
#include <TGeoManager.h>
#include <TGenerator.h>
#include <TParticle.h>
#include <TBrowser.h>
#include <cmath>

//____________________________________________________________________
ClassImp(Simulation::Stack);

//____________________________________________________________________
Simulation::Stack::Stack(Int_t size)
  : fParticles(0),
    fTracks(0),
    fCurrentTrack(-1),
    fNPrimary(0), 
    fNtrack(0)
{
  fParticles = new TClonesArray("TParticle", size);
  fTracks    = new TObjArray(size);
  SetKeepIntermediate();
}


//____________________________________________________________________
Simulation::Stack::Stack()
  : fParticles(0),
    fCurrentTrack(-1),
    fNPrimary(0)
{
  fParticles = new TClonesArray("TParticle", 1000);
  fTracks    = new TObjArray(1000);
  SetKeepIntermediate();
}

//____________________________________________________________________
Simulation::Stack::~Stack() 
{
  // Deallocate the stack
  if (fParticles) fParticles->Delete();
  delete fParticles;
}
  
//____________________________________________________________________
void
Simulation::Stack::Browse(TBrowser* b)
{
  if (fParticles) b->Add(fParticles, "Particles");
}

//____________________________________________________________________
void
Simulation::Stack::Reset() 
{
  // Reset the stack. 
  fCurrentTrack = -1;
  fNPrimary     = 0;
  fNtrack       = 0;
  if (fParticles)  fParticles->Clear();
  if (fTracks)     fTracks->Clear();
  if (gGeoManager) gGeoManager->ClearTracks();
  // Should we free the memory hold by fStack?  Probably not, as the
  // particles should have been pop'ed 
  if (fStack.size() != 0) {
    // This is confusing - Geant 4 does not pop primaries from the stack! 
    Warning("Reset", "Not all particles pop'd from stack, %ld remaining "
	    "(OK for Geant 4)", fStack.size());
    fStack.erase(fStack.begin(), fStack.end());
  }
}

//____________________________________________________________________
TVirtualGeoTrack*
Simulation::Stack::MakeGeoTrack(Int_t      ntr,
				Int_t      parent,
				TParticle* particle)
{
  if  (!IsKeepIntermediate() || !gGeoManager) return 0;
  
  //   Debug(50,"PushTrack", "Getting father");
  TVirtualGeoTrack* track = 0;

  // If we have a parent particle number 
  if (parent >= 0) {
    TVirtualGeoTrack* father = GetTrack(parent);
    if (!father) {
      Warning("PushTrack", "Father track %d not found", parent);
      return 0;
    }
    // Debug(50,"PushTrack", "Adding a daughter %d to track", ntr, father);
    track = father->AddDaughter(ntr, particle->GetPdgCode(), particle);
    father->AddPoint(particle->Vx(),
		     particle->Vy(),
		     particle->Vz(),
		     particle->T());
  }
  // If this is a primary 
  else {
    Int_t n = gGeoManager->AddTrack(ntr, particle->GetPdgCode(), particle);
    track   = gGeoManager->GetTrack(n);
  }
  if (track)  
    track ->AddPoint(particle->Vx(),
		     particle->Vy(),
		     particle->Vz(),
		     particle->T());

  return track;
}

//____________________________________________________________________
void
Simulation::Stack::PushTrack(Int_t toBeDone, Int_t parent, Int_t pdg,
			     Double_t px, Double_t py, 
			     Double_t pz, Double_t e,
			     Double_t vx, Double_t vy, 
			     Double_t vz, Double_t tof,
			     Double_t polx, Double_t poly, Double_t polz,
			     TMCProcess mech, Int_t& ntr, Double_t weight,
			     Int_t is)  
{
  // Push a track onto the stack. 
  //
  //   toBeDone    Wether to track this particle or not 
  //   parent      Parent track # 
  //   pdg         PDG particle type # 
  //   px          X coordinate of momentum vector 
  //   py          Y coordinate of momentum vector 
  //   pz          Z coordinate of momentum vector 
  //   e           Energy coordinate of the momentum vector
  //   vx          X coordinate of production vertex 
  //   vy          Y coordinate of production vertex 
  //   vz          Z coordinate of production vertex 
  //   tof         Time coordinate of production vertex
  //   polx        X coordinate of polarisation vector 
  //   poly        Y coordinate of polarisation vector 
  //   polz        Z coordinate of polarisation vector 
  //   mech        Prodcution mechanism 
  //   ntr         On output, the number of the track stored
  //   weight      Weight (calculated mass) of particle
  //   is          Status

  const Int_t kFirstDaughter = -1;
  const Int_t kLastDaughter  = -1;
  if (std::isnan(px) 
      || std::isnan(py) 
      || std::isnan(pz) 
      || std::isnan(e) 
      || std::isnan(vx) 
      || std::isnan(vy) 
      || std::isnan(vz) 
      || std::isnan(tof))
    return;

  ntr = fNtrack;
  fNtrack++;
  TParticle* particle = 
    new((*fParticles)[ntr]) TParticle(pdg, is, parent, -1, 
				      kFirstDaughter, kLastDaughter,
				      px, py, pz, e, vx, vy, vz, tof);
  particle->SetPolarisation(polx, poly, polz);
  particle->SetWeight(weight);
  particle->SetUniqueID(mech);
  particle->SetLineColor(kGreen);
  
  // Set bits
  if (!toBeDone) particle->SetBit(kDoneBit);
  particle->SetBit(kDaughtersBit);

  TVirtualGeoTrack* track = 0;
  if (parent >= 0) {
    TParticle* mother = GetParticle(parent);
    if (mother) {
      // If this is a secondary.  Tracks are pushed in order, so we do
      // not need to check the ordering.
      mother->SetLastDaughter(ntr);
      if (mother->GetFirstDaughter() < 0) 
	mother->SetFirstDaughter(ntr);
    }
    else 
      Warning("PushTrack", "Mother particle %d not found", parent);

    track = MakeGeoTrack(ntr,parent,particle);
  }  
  else {
    // If this is a primary 
    fNPrimary++;
    track = MakeGeoTrack(ntr,-1,particle);
  }
  if (track) {
    // Info("PushTrack", "Got a track: %p", track);
    fTracks->AddAtAndExpand(track, ntr);
    TDatabasePDG* pdgDb = TDatabasePDG::Instance();
    TParticlePDG* pdgP  = pdgDb->GetParticle(pdg);
    // Ugly hack, in case geometry was read from file
    // TString gname(gGeoManager  ? gGeoManager->GetPdgName(pdg) : "XXX");
    if (pdgP) track->SetName(pdgP->GetName());
  }
  if (toBeDone) {
    fStack.push_back(StackEntry_t(ntr, particle));    
  }
}
//____________________________________________________________________
void
Simulation::Stack::ImportPrimaries(TClonesArray* array) 
{
  if (!array) return;
  //Int_t nOld = fParticles->GetEntriesFast();
  Int_t n = array->GetEntriesFast();
  Printf("Stack got %d particles", n);
  for (Int_t i = 0; i  < n; i++) {
    TParticle* particle = static_cast<TParticle*>(array->At(i));
    particle->Print();
    
    TVector3 pol;
    particle->GetPolarisation(pol);
    Int_t ntr;
    PushTrack(kTRUE, -1, particle->GetPdgCode(), 
	      particle->Px(), particle->Py(), 
	      particle->Pz(), particle->Energy(), 
	      particle->Vx(), particle->Vy(), 
	      particle->Vz(), particle->T(), 
	      pol.X(), pol.Y(), pol.Z(), kPPrimary, ntr, 
	      particle->GetWeight(), 0);
  }
}

  
//____________________________________________________________________
TParticle*
Simulation::Stack::PopNextTrack(Int_t& id) 
{
  id = -1;
  if  (fStack.empty()) return 0;
  StackEntry_t entry = fStack.back();
  fStack.pop_back();

  TParticle* particle = entry.second;

  if (!particle) return 0;  

  if (particle->GetFirstMother() < 0) fNPrimary--;
  fCurrentTrack = id = entry.first; // fStack.size();
  if (gGeoManager && IsKeepIntermediate())
    gGeoManager->SetCurrentTrack(GetTrack(entry.first));
  particle->SetBit(kDoneBit);
  return particle;
}
//____________________________________________________________________
TParticle*
Simulation::Stack::PopPrimaryForTracking(Int_t i) 
{
  TParticle* particle = GetParticle(i);
  if (particle && !particle->TestBit(kDoneBit)) return particle;
  return 0;
}
//____________________________________________________________________
TParticle*
Simulation::Stack::GetCurrentTrack() const 
{
  TParticle* current = GetParticle(fCurrentTrack);
  return current;
}
//____________________________________________________________________
Int_t
Simulation::Stack::GetCurrentParentTrackNumber() const 
{
  TParticle* current = GetCurrentTrack();
  if (!current) return -1; 
  return current->GetFirstMother();
}

//____________________________________________________________________
TParticle*
Simulation::Stack::GetParticle(Int_t id) const 
{
  Int_t n = fParticles->GetEntriesFast();
  if (id < 0 || id >= n)
    Fatal("GetParticle", "Index %d out of range [0,%d)", id, n); 
  return static_cast<TParticle*>(fParticles->At(id));
}

//____________________________________________________________________
TVirtualGeoTrack*
Simulation::Stack::GetTrack(Int_t id) const 
{
  Int_t n = fTracks->GetEntriesFast();
  if (id < 0 || id >= n)
    Fatal("GetTrack", "Index %d out of range [0,%d)", id, n); 
  return static_cast<TVirtualGeoTrack*>(fTracks->At(id));
}
  
  
//
// EOF
//
