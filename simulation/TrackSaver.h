// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/TrackSaver.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for TrackSaver
*/
#ifndef Simulation_TrackSaver
#define Simulation_TrackSaver
#include <simulation/Task.h>
class TClonesArray;

namespace Simulation
{
  /** @class TrackSaver TrackSaver.h <simulation/TrackSaver.h>
      @brief Put stack particles in output tree 
      @ingroup sim_special_tasks
  */
  class TrackSaver : public Simulation::Task 
  {
  public:
    /** Constructor 
	@param name     Name of branch to store in.
	@param treename Name of tree to store branch in.
	@param keepAll  If true, keep all tracks, wether marked or not
	@param maxDepth Maximum depth (see TrackSaver::SetMaxDepth)
    */
    TrackSaver(const char* name     = "Tracks",
	       const char* treename = "Hits",
	       Bool_t      keepAll  = false,
	       UInt_t      maxDepth = 0);
    /** Initialize 
	@param option Not used */
    void Register(Option_t* option="");
    /** Clear map cache at beginning of the event */
    void BeginEvent();
    /** Save flaged tracks in output tree */
    void FinishEvent();
    /** Set wether to keep all particles or not */
    void SetKeepAll(Bool_t keepAll=true) { fKeepAll = keepAll; }
    /** Check if all particles are kept */
    Bool_t AllKept() const { return fKeepAll; }
    /** Set max depth to keep parents at.

	A track can request to be kept by having the bit kKeepBit set.
	Tracks with this bit set are always kept.

	A track may also request that its history is kept by setting
	the bit kKeepParentsBit.  How deep that request is honoured
	depends on this setting.

	If max depth is

	- 0, then only the track itself is kept.  Parents can still be
          kept, but must have the kKeepBit set themselves.

	- 1, the track and its immediate parents are kept.

	- 2, the track, its parents, and its grand parents are kept

	- ... and so on down the tree.

	Of course, if a parent of a track is set to be kept directly,
	and also requests parents to be kept, then that will result in
	a deeper history to be kept.

	Setting this to a large number is the same as keeping all
	tracks, more or less, and will enlarge the output
	significantly.

	@param maxDepth The maximum depth.
    */
    void SetMaxDepth(UInt_t maxDepth) { fMaxDepth = maxDepth; }
    /** Get the maximum depth */
    UInt_t GetMaxDepth() const { return fMaxDepth; }
  protected:
    /** Keep parent particles.  This recurses down the tree until lvl
	is zero, subtracting one from lvl at each recursion.  Thus, is
	lvl starts at 1 (i.e., fMaxDepth is 1), then this will store
	the parent of tracks which request to keep their parents.  If
	this starts at 2, then this will keep parents and grand
	parents of tracks that request that.
     */
    virtual void KeepParent(Int_t i, UInt_t lvl);
    /** If true, keep all particles */
    Bool_t fKeepAll;
    /** The maximum depth to keep parents at.

	A value of 
	- 0 means keep no parents (unless kept directly).
	- 1 means keep parents
	- 2 means keep parents and grand parents
	- ... and so on
    */
    UInt_t fMaxDepth;
    /** Map trackno to cache index */
    TClonesArray* fMap;
    ClassDef(TrackSaver,0); //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
