// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: Hit.h,v 1.4 2005-12-05 15:19:17 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/Hit.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Example::Hit
*/
#ifndef Example_Hit
#define Example_Hit
#include <simulation/DefaultHit.h>

namespace Example 
{
  class Hit : public Simulation::DefaultHit
  {
  public:
    Int_t   fTube;
    Int_t   fRing;
    
    Hit() : fTube(-1), fRing(-1) {}
    /** Create a hit
	@param tube       Tube number
	@param ring       Ring number
	@param energyLoss in GeV
	@param nTrack     Track number
	@param pdg        PDG identifier
	@param v          4-Spatial coordiante in cm and s
	@param p          4-Momentum X coordinates in GeV/c and GeV
    */
    Hit(Int_t                 tube, 
	Int_t                 ring, 
	Float_t               energyLoss,
	Int_t                 nTrack,
	Int_t                 pdg,
	const TLorentzVector& v, 
	const TLorentzVector& p) 
      : Simulation::DefaultHit(nTrack, pdg, v, p, energyLoss), 
	fTube(tube), fRing(ring)
    {}
    /** Create a hit
	@param tube       Tube number
	@param ring       Ring number
	@param energyLoss in GeV
	@param nTrack     Track number
	@param pdg        PDG identifier
	@param x          X coordiante in cm
	@param y          y coordiante in cm
	@param z          z coordiante in cm
	@param t          t coordiante in s
	@param px         Momentum X coordiante in GeV/c
	@param py         Momentum Y coordiante in GeV/c
	@param pz         Momentum Z coordiante in GeV/c
	@param e          Energy in GeV
    */
    Hit(Int_t   tube, 
	Int_t   ring, 
	Float_t energyLoss,
	Int_t   nTrack,
	Int_t   pdg,
	Float_t x,
	Float_t y,
	Float_t z,
	Float_t t,
	Float_t px,
	Float_t py,
	Float_t pz,
	Float_t e) 
      : Simulation::DefaultHit(nTrack, pdg, x,y,z,t,px,py,pz,e,energyLoss), 
	fTube(tube), fRing(ring)
    {}
    /** Print it */
    void Print(Option_t* option="P") const; //*MENU*
    ClassDef(Hit,1);
  };
}
#endif
//
// EOF
//
