# Simulation  - a generic Simulation framework using ROOT


![Movie](doc/movie.gif)

This is a package for defining a simulation of a detector.  It builds
on the [generalised framework
package](https://gitlab.cern.ch/cholm/root-framework.git) and the
[ROOT Virtual
Monte-Carlo](http://root.cern.ch/root/vmc/VirtualMC.html) framework.

This is _not_ intent as the definite framework for ROOT based
simulations.  It's meant as an inspiration and possibly as a starting
point for a simulation.

![](doc/display3dcut.png)

## Installation

### Dependencies 

This package requires 

- [ROOT](https://root.cern.ch) 
- [ROOT Framework](https://gitlab.cern.ch/cholm/root-framework.git) 
- [ROOT VMC](https://github.com/vmc-project/vmc)

#### GEANT 3.21 back-end 

Get the ROOT Virtual Monte-Carlo (misnomer) interface to GEANT 3.21
from 

- [GEANT 3.21 VMC](https://github.com/vmc-project/geant3)

This package also contains the Fortran source code for GEANT 3.21 and
nothing else is needed 

#### Geant 4 back-end 

This requires, in order of installation 

- [Geant 4](https://github.com/Geant4/geant4.git) - as of this
  writing, one need the latest and greatest (`master` - 11.1.1) of
  this package. 
- [Virtual Geometry Model (VGM)](https://github.com/vmc-project/vgm) 
- [ROOT VMC interface to Geant
  4 (`geant4_vmc`)](https://github.com/vmc-project/geant4_vmc.git)
  
Note that it is important that 

- the libraries are built in the above order and that 
  - VGM is configured for Geant 4 and ROOT 
  - `geant4_vmc` is configured for VGM 
- that the same C++ standard is used in all cases.  That is, if ROOT
  was built with `-std=c++17` then Geant 4 and the other packages
  should also be built with `-std=c++17`.
  - The Application Binary Interfaces (ABIs) of C++ 14 and later are
    _not_ compatible with C++ 11 or earlier (`std::string` has a
    different layout). 

### From git clone 

Clone the [repo](https://gitlab.cern.ch/cholm/root-simulation.git) from 

    https://gitlab.cern.ch/cholm/root-simulation.git

Then, prepare the autotools 

    cd root-simulation
    autoreconf -i -f 

Then proceed as below 

### Configuration and building 

**Simulation** is build using **Autotools**.  That means that the
installation goes through the well known triplet

    ./configure 
    make 
    make install 

Note that it is recommended to build in a directory parallel to the
sources.  E.g., if the sources are in 

    ~/src/root-simulation 
	
then one should do 

    cd 
	mkdir -p build/root-simulation
	cd build/root-simulation
	../../src/root-simulation/configure 
	make 
	make install 
	
### Configure Arguments

The `configure` script takes a number of arguments, including path to
top-level installation directory, where ROOT is installed
and so on.

For example, to specify that you want to install **Simulation** in
your home directory, do

    ./configure --prefix=$HOME 

To make the libraries without optimisation (useful for debugging), do
something like

    ./configure --disable-optimization 

To see all the available options, do 


    ./configure --help 

Please also refer to the file `INSTALL` in the source code
distribution. 

### Configuring ROOT to see Simulation

Suppose you installed this package in `$HOME`.  To make ROOT aware of
the **Simulation** plugin, add the directory`$HOME/lib` to
`DynamicPath` in one of `etc/root/system.rootrc`, `~/.rootrc`, or
`./.rootrc,` e.g.,

    Unix.*.Root.DynamicPath:    .:$(HOME)/lib:/usr/lib/root
	
Alternatively, you can add `$HOME/lib` to the environment variable
`LD_LIBRARY_PTH`, for example in `~/.profile` 

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/lib

Also, you may need to add the installation header directory to the
environment variable `ROOT_INCLUDE_PATH`.  Suppose, as above, you have
installed in `$HOME`, then you need in your `~/.profile` (or
equivalent) 

    export ROOT_INCLUDE_PATH=$ROOT_INCLUDE_PATH:$HOME/include
	
In a Korn shell (BASH etc.), one can add the following lines to ones
`~/.profile` 

    ROOT_SIMULATION_PREFIX=$HOME  # Replace with chosen installation prefix
    if test -d $ROOT_SIMULATION_PREFIX/lib && \
	   test -d $ROOT_SIMULATION_PREFIX/include ; then 
	  export LD_LIBRARY_PATH=$ROOT_SIMULATION_PREFIX/lib:$LD_LIBRARY_PATH
      export ROOT_INCLUDE_PATH=$ROOT_SIMULATION_PREFIX/include:$ROOT_INCLUDE_PATH
    fi

## Quick start 

To get things started, here's a small example of a detector simulation
of a $`\mu^\pm`$ life-time experiment.

![](doc/muon.png)

See the **Quick start** _related page_ in the Doxygen documentation.

	
//
// EOF
//

