#ifndef SIMULATION_UTIL_MakeTask
# include <simulation/MakeTask.h>
#endif
#include <TClass.h>
#include <TList.h>
#include <TObjString.h>
#include <TSystem.h>
#include <TMap.h>
#include <TRegexp.h>
#include <TGeoManager.h>
#include <TGeoVolume.h>
#include <TGeoShape.h>
#include <TGeoMedium.h>
#include <TGeoMaterial.h>
#include <TGeoBBox.h>
#include <TGeoTube.h>
#include <TGeoCone.h>
#include <TGeoPcon.h>
#include <TGeoPgon.h>
#include <TGeoArb8.h>
#include <TGeoPara.h>
#include <TGeoParaboloid.h>
#include <TGeoHalfSpace.h>
#include <TGeoSphere.h>
#include <TGeoTorus.h>
#include <TGeoTrd1.h>
#include <TGeoTrd2.h>
#include <TGeoXtru.h>
#include <TGeoHype.h>
#include <TGeoEltu.h>
#include <TGeoCompositeShape.h>
#include <TGeoScaledShape.h>
#include <TGeoShapeAssembly.h>
#include <iostream>
#include <iomanip>

//____________________________________________________________________
namespace 
{
  //__________________________________________________________________
  struct spaces 
  {
    int n;
    int dn;
    spaces(int d=1) : n(0), dn(d) {}
    void inc() { n += dn; }
    void dec() { n -= dn; }
  };
  spaces  indent;
  spaces  indent2(2);
  

  //__________________________________________________________________
  struct ruler
  {
    spaces* s;
    ruler(spaces& i) : s(&i) {}
  };
  ruler rule(indent);
  ruler rule2(indent2);
}


//__________________________________________________________________
std::ostream& operator<<(std::ostream& o, const spaces& s) 
{
  if (s.n <= 0) return o;
  return o << std::setw(s.n) << " ";
}

std::ostream& operator<<(std::ostream& o, const ruler& r)
{
  return o << *(r.s) << "//" << std::setfill('_') 
	   << std::setw(68 - r.s->n) << "_" << std::setfill(' ');
}

//____________________________________________________________________
namespace Simulation 
{
  namespace Util
  {
    //________________________________________________________________
    TMap MakeTask::fMap;
    Bool_t MakeTask::fIsTop = kFALSE;
  }
}


//____________________________________________________________________
Simulation::Util::MakeTask::MakeTask(const char* name) 
  : TNamed(name,name), fSource(Form("%s.cxx", name))
{
  fSubs = new MakeTask * [100];
  fNsubs = 0;
  fNSens = 0;
  fCurSens = 0;
  fHaveField = kFALSE;
}
//____________________________________________________________________
Simulation::Util::MakeTask* 
Simulation::Util::MakeTask::AddMakeTask(const char* name) 
{
  MakeTask* cand = FindMakeTask(name);
  if (cand) return cand;
  fSubs[fNsubs] = new MakeTask(name);
  fNsubs++;
  return fSubs[fNsubs-1];
}
//____________________________________________________________________
Simulation::Util::MakeTask* 
Simulation::Util::MakeTask::FindMakeTask(const char* name) 
{
  for (Int_t i = 0; i < fNsubs; i++) {
    if (fSubs[i]->fName == name) return fSubs[i];
    MakeTask* cand = fSubs[i]->FindMakeTask(name);
    if (cand) return cand;
  }
  return 0;
}
//____________________________________________________________________
Simulation::Util::MakeTask* 
Simulation::Util::MakeTask::FindVolume(TGeoVolume* vol) 
{
  if (fVolumes.FindObject(vol)) return this;
  for (Int_t i = 0; i < fNsubs; i++) 
    if (fSubs[i]->FindVolume(vol)) return fSubs[i];
  return 0;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::AddVolume(TGeoVolume* vol) 
{
  if (fVolumes.FindObject(vol->GetName())) return;
  fVolumes.Add(vol);
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::Print(Option_t*) const 
{
  std::cout << rule << "\n"
	    << indent << "MakeTask: " << fName << ":" << std::endl;
  indent.inc();
  std::cout << indent << "Volumes: " << std::endl;
  indent.inc();
  std::cout << indent << std::flush;
  for (Int_t i = 0; i < fVolumes.GetEntries(); i++) {
    TGeoVolume* vol = static_cast<TGeoVolume*>(fVolumes.At(i));
    std::cout << vol->GetName() << " " <<std::flush;
  }
  std::cout << std::endl;
  indent.dec();
  if (fNsubs > 0) { 
    std::cout << indent << "Subs: " << std::endl;
    indent.inc();
    for (Int_t i = 0; i < fNsubs; i++) fSubs[i]->Print();
    indent.dec();
  }
  indent.dec();
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::Write()
{
  fIsTop = kTRUE;
  WriteTop();
  WriteMediums();
  WriteVolumes();
  WriteNodes();
  WriteFile();
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteTop()
{
  TObjArray done;
  fSource << "#include \"" <<  fName << ".h\"\n"
	  << "#include <TVirtualMC.h>\n"
	  << "#include <TGeoManager.h>\n"
	  << "#include <TGeoMedium.h>\n"
	  << "#include <TGeoMaterial.h>\n"
	  << "#include <TGeoVolume.h>\n"
	  << "#include <TGeoMatrix.h>\n" << std::endl;
  for (Int_t i = 0; i < fVolumes.GetEntries(); i++) {
    TGeoVolume* vol   = static_cast<TGeoVolume*>(fVolumes.At(i));
    TGeoShape* shape  = vol->GetShape();
    TObjString* sname = 
      new TObjString(gSystem->BaseName(shape->IsA()->GetDeclFileName()));
    if (done.FindObject(sname)) { delete sname ; continue; }
    done.Add(sname);
    fSource << "#include <" << sname->String() << ">" << std::endl;
  }
  done.Delete();
  fSource << "\n" 
	  << rule2 << "\n" 
	  << fName << "::" << fName << "()\n" 
	  << "  : Simulation::Task(\""<< fName << "\",\""<< fName << "\"),\n"
	  << "    fActive(0)\n"
	  << "{\n"
	  << "  // This creates the default hit array\n" 
	  << "  CreateDefaultHitArray();\n"
	  << "}\n" << std::endl;
  for (Int_t i = 0; i < fNsubs; i++) fSubs[i]->WriteTop();
}
//____________________________________________________________________
TString 
Simulation::Util::MakeTask::TransName(const char* name, Bool_t specific)
{
  TString ret;
  if (specific) ret = Form("%s_%s", fName.Data(), name);
  else          ret = name;
  TRegexp reg("[^a-zA-Z0-9_]+");
  Int_t n;
  Int_t idx;
  while ((idx = reg.Index(ret, &n)) >= 0) ret.Replace(idx, n, "_");
  ret.Remove(TString::kBoth, '_');
  return ret;
}

//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteMediums() 
{
  fSource << rule2 << "\n" 
	  << "void\n" 
	  << fName << "::Initialize(Option_t* option)\n" 
	  << "{" << std::endl;
  indent2.inc();
  fSource << indent2 << "Simulation::Task::Initialize(option);" << std::endl;
  if (fIsTop) WriteMaterials();
  fSource << indent2 << "// Register mediums" << std::endl;
  for (Int_t i = 0; i < fVolumes.GetEntries(); i++) {
    TGeoVolume* vol = static_cast<TGeoVolume*>(fVolumes.At(i));
    TGeoMedium* med = vol->GetMedium();
    if (!med) continue;
    if (med->GetParam(0) == 0 && med->GetParam(1) == 0) continue;
    if (med->GetParam(0) != 0) fNSens++;
    if (med->GetParam(1) != 0) fHaveField = kTRUE;
    if (fDoneMed.FindObject(med)) continue;
    TGeoMaterial* mat = med->GetMaterial();
    if (!fDoneMat.FindObject(mat)) {
      fDoneMat.Add(mat);
      TString mname(TransName(mat->GetName(), kFALSE));
      fSource << indent2 << "TGeoMaterial* " << mname 
	      << "_Mat = gGeoManager->GetMaterial(\"" << mname << "\");\n" 
	      << indent2 << "if (!" << mname << "_Mat) \n"
	      << indent2 << "  Error(\"Initialize\","
	      << " \"No such material: " << mname << "\");"
	      << std::endl;
    }
    WriteMedium(med);
  }
  indent2.dec();
  fSource << indent2 << "}" << std::endl;
  fIsTop = kFALSE;
  for (Int_t i = 0; i < fNsubs; i++) fSubs[i]->WriteMediums();
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteMaterials() 
{
  TList* mats = gGeoManager->GetListOfMaterials();
  fSource << indent2 << "// All materials and mixtures" << std::endl;
  for (Int_t i = 0; i < mats->GetEntries(); i++) {
    TObject* mat = mats->At(i);
    if (mat->IsA() == TGeoMaterial::Class()) 
      WriteMaterial(static_cast<TGeoMaterial*>(mat), kFALSE);
    else 
      WriteMixture(static_cast<TGeoMixture*>(mat), kFALSE);
  }
  fSource << indent2 << "\n" << std::endl;
  TList* meds = gGeoManager->GetListOfMedia();
  fSource << indent2 << "// All non-sensitive or magnetic mediums" 
	  << std::endl;
  for (Int_t i = 0; i < meds->GetEntries(); i++) {
    TGeoMedium* med = static_cast<TGeoMedium*>(meds->At(i));
    if (med->GetParam(0) != 0 || med->GetParam(1) != 0) continue;
    WriteMedium(med, kFALSE);
  }
  fSource << indent2 << "\n" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteMedium(TGeoMedium* med, Bool_t spec) 
{
  fDoneMed.Add(med);
  TString name(TransName(med->GetName(), spec));
  TString mname(TransName(med->GetMaterial()->GetName(), kFALSE));
  fSource << indent2 << "// Tracking medium " << name << "\n" << indent2 
	  << "// isvol,ifield,fieldm,tmaxfd,stemax,deemax,epsil,stmin\n"
	  << indent2 << "Double_t " << name << "Pars[] = { " 
	  << (med->GetParam(0) != 0 ? "fActive" : "0") << ", " << std::flush;
  for (Int_t j = 1; j < 8; j++) 
    fSource << med->GetParam(j) << (j != 7 ? ", " : " };") << std::flush;
  fSource << "\n" << indent2 << "TGeoMedium* " << name 
	  << "_Med = new TGeoMedium(\"" << name << "\", 0, "
	  << mname << "_Mat, " << name << "Pars);" << std::endl;
  if (spec) 
    fSource << indent2 << "if (fActive) RegisterMedium(" 
	    << name << "_Med);" << std::endl;
}
  
    
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteMaterial(TGeoMaterial* mat, Bool_t spec) 
{
  fDoneMat.Add(mat);
  TString mname(TransName(mat->GetName(), spec));
  fSource << indent2 << "// Material " << mname << "\n" 
	  << indent2 << "TGeoMaterial* " << mname 
	  << "_Mat = new TGeoMaterial(\""  << mname << "\", " << mat->GetA() 
	  << ", " << mat->GetZ() << ", " << mat->GetDensity() << ", " 
	  << mat->GetRadLen() << mat->GetIntLen() << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteMixture(TGeoMixture* mat, Bool_t spec) 
{
  fDoneMat.Add(mat);
  TString mname(TransName(mat->GetName(), spec));
  fSource << indent2 << "// Mixture " << mname << "\n" 
	  << indent2 << "TGeoMixture* " << mname 
	  << "_Mat = new TGeoMixture(\"" << mname << "\", " 
	  << mat->GetNelements() << ", " << mat->GetDensity() << ");" 
	  << std::endl;
  for (Int_t i = 0; i <  mat->GetNelements(); i++) {
    fSource << indent2 << mname << "_Mat->DefineElement(" << i << ", "
	    << mat->GetAmixt()[i] << ", " << mat->GetZmixt()[i] << ", "
	    << mat->GetWmixt()[i] << ");\t// "  
	    << mat->GetElement(i)->GetName() << std::endl;
  }
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteVolumes() 
{
  TObjArray done;
  fSource << rule2 << "\n" 
	  << "void\n" 
	  << fName << "::Register(Option_t* option)\n" 
	  << "{" << std::endl;
  indent2.inc();
  fSource << indent2 << "Simulation::Task::Register(option);\n"
	  << indent2 << "// Build geometry\n"  
	  << indent2 << "// Shapes and volumes " << std::endl;
  for (Int_t i = 0; i < fVolumes.GetEntries(); i++) {
    TGeoVolume* vol = static_cast<TGeoVolume*>(fVolumes.At(i));
    TGeoMedium* med = vol->GetMedium();
    TString mname;
    if (med) {
      Bool_t spec = (med->GetParam(0) == 1 || med->GetParam(1) == 1);
      mname = TransName(med->GetName(), spec);
      if (!done.FindObject(med)) {
	fSource << indent2 << "// Get medium " << mname << "\n"
		<< indent2 << "TGeoMedium* " << mname
		<< "_Med = gGeoManager->GetMedium(\"" << mname << "\");\n"
		<< indent2 << "if (!" << mname << "_Med) {\n" 
		<< indent2 << "  Warning(\"Initialize\", \"Medium " 
		<< mname << " not found\");\n" 
		<< indent2 << "  return;\n" 
		<< indent2 << "}"
		<< std::endl;
	done.Add(med);
      }
    }
    TGeoShape* shape = vol->GetShape();
    WriteShape(shape, vol->GetName());
    if (med) {
      fSource << indent2 << "// Volume " << vol->GetName() << "\n"
	      << indent2 << "TGeoVolume* " << vol->GetName() 
	      << "_Vol = new " << vol->ClassName() << "(\"" 
	      << vol->GetName() << "\", "
	      << vol->GetName() <<  "_Shape, " << mname << "_Med);" 
	      << std::endl;
      if (med->GetParam(0) != 0) {
	fSource << indent2 << "RegisterVolume(" << vol->GetName() << "_Vol);"
		<< std::endl;
	fCurSens++;
      }
    }
    else 
      fSource << indent2 << "TGeoVolume* " << vol->GetName() 
	      << "_Vol = new " << vol->ClassName() << "(\"" 
	      << vol->GetName() << "\");" << std::endl;
    if (vol->IsTopVolume()) 
      fSource << indent2 << "gGeoManager->SetTopVolume(" 
	      << vol->GetName() << "_Vol);" << std::endl;
  }
  fSource << "\n" << indent2 << "// Nodes and matrixes" << std::endl;
  indent2.dec();
  for (Int_t i = 0; i < fNsubs; i++) fSubs[i]->WriteVolumes();
}


#define SHAPE_HEAD(shape,vol) \
    fSource << indent2 << "// Shape of " << vol << "\n" \
            << indent2 << shape->ClassName() << "* " << vol << "_Shape" \
	    << " = new " << shape->ClassName() << "(\"" << vol \
            << "_Shape\", " << std::flush
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteShape(TGeoShape* shape, const char* vol) 
{
  if (shape->IsA() == TGeoBBox::Class())
    WriteBBox(static_cast<TGeoBBox*>(shape), vol);
  else if (shape->IsA() == TGeoHype::Class())
    WriteHype(static_cast<TGeoHype*>(shape), vol);
  else if (shape->IsA() == TGeoEltu::Class())
    WriteEltu(static_cast<TGeoEltu*>(shape), vol);
  else if (shape->IsA()->InheritsFrom(TGeoTube::Class())) 
    WriteTube(static_cast<TGeoTube*>(shape), vol);
  else if (shape->IsA()->InheritsFrom(TGeoCone::Class())) 
    WriteCone(static_cast<TGeoCone*>(shape), vol);
  else if (shape->IsA()->InheritsFrom(TGeoPcon::Class()))
    WritePcon(static_cast<TGeoPcon*>(shape), vol);
  else if (shape->IsA() == TGeoHalfSpace::Class())
    WriteHalfSpace(static_cast<TGeoHalfSpace*>(shape), vol);
  else if (shape->IsA() == TGeoPara::Class())
    WritePara(static_cast<TGeoPara*>(shape), vol);
  else if (shape->IsA() == TGeoParaboloid::Class())
    WriteParaboloid(static_cast<TGeoParaboloid*>(shape), vol);
  else if (shape->IsA() == TGeoSphere::Class())
    WriteSphere(static_cast<TGeoSphere*>(shape), vol);
  else if (shape->IsA() == TGeoTorus::Class())
    WriteTorus(static_cast<TGeoTorus*>(shape), vol);
  else if (shape->IsA() == TGeoTrd1::Class())
    WriteTrd1(static_cast<TGeoTrd1*>(shape), vol);
  else if (shape->IsA() == TGeoTrd2::Class())
    WriteTrd2(static_cast<TGeoTrd2*>(shape), vol);
  else if (shape->IsA() == TGeoXtru::Class())
    WriteXtru(static_cast<TGeoXtru*>(shape), vol);
  else if (shape->IsA() == TGeoTrap::Class())
    WriteTrap(static_cast<TGeoTrap*>(shape), vol);
  else if (shape->IsA() == TGeoGtra::Class())
    WriteGtra(static_cast<TGeoGtra*>(shape), vol);
  else if (shape->IsA() == TGeoArb8::Class())
    WriteArb8(static_cast<TGeoArb8*>(shape), vol);
  else if (shape->IsA() == TGeoCompositeShape::Class())
    WriteCompositeShape(static_cast<TGeoCompositeShape*>(shape), vol);
  else if (shape->IsA() == TGeoScaledShape::Class())
    WriteScaledShape(static_cast<TGeoScaledShape*>(shape), vol);
  else if (shape->IsA() == TGeoShapeAssembly::Class())
    WriteShapeAssembly(static_cast<TGeoShapeAssembly*>(shape), vol);
  else {
    std::cerr << "Shape class " << shape->ClassName() 
	      << " not supported for node " << vol << std::endl;
  }
}

//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteBBox(TGeoBBox* s,const char* vol)
{
  const Double_t* origin = s->GetOrigin();
  if (origin[0] != 0 || origin[1] != 0 || origin[2] != 0) 
    fSource << indent2 << "Double_t " << vol << "_Origin[] = {" 
	    << origin[0] << ", " << origin[1] << ", " << origin[2] 
	    << "};" << std::endl;
  else 
    origin = 0;
  SHAPE_HEAD(s,vol);
  fSource << s->GetDX() << ", " << s->GetDY() << ", " << s->GetDZ();
  if (origin) 
    fSource << ", "  << vol << "_Origin";
  fSource << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteHype(TGeoHype* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetRmin() << "," << s->GetStIn() << "," 
	  << s->GetRmax() << "," << s->GetStOut() << "," 
	  << s->GetDz() 
	  << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteEltu(TGeoEltu* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetA() << "," << s->GetB() << "," 
	  << s->GetDz() << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteTube(TGeoTube* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetRmin() << "," << s->GetRmax() << "," << s->GetDz() 
	  << std::flush;
  if (s->IsA()->InheritsFrom(TGeoTubeSeg::Class())) {
    TGeoTubeSeg* ss = static_cast<TGeoTubeSeg*>(s);	
    fSource << ", " << ss->GetPhi1() << ", " << ss->GetPhi2() 
	    << std::flush;
    if (ss->IsA()->InheritsFrom(TGeoCtub::Class())) {
      TGeoCtub* cs = static_cast<TGeoCtub*>(s);	
      fSource << ", " << cs->GetNlow()[0] << ", " << cs->GetNlow()[1]
	      << ", " << cs->GetNlow()[2] << ", " << cs->GetNhigh()[0] 
	      << ", " << cs->GetNhigh()[1] << ", " << cs->GetNhigh()[2] 
	      << std::flush;
    }
  }
  fSource << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteCone(TGeoCone* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetDz() << ", " << s->GetRmin1() << "," 
	  << s->GetRmax1() << "," << s->GetRmin2() << "," 
	  << s->GetRmax2() << std::flush;
  if (s->IsA()->InheritsFrom(TGeoConeSeg::Class())) {
    TGeoConeSeg* ss = static_cast<TGeoConeSeg*>(s);	
    fSource << ", " << ss->GetPhi1() << ", " << ss->GetPhi2() 
	    << std::flush;
  }
  fSource << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WritePcon(TGeoPcon* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetPhi1() << ", " << s->GetDphi() << ", " << std::flush;
  Int_t nz = s->GetNz();
  if (s->IsA()->InheritsFrom(TGeoPgon::Class())) {
    TGeoPgon* ss = static_cast<TGeoPgon*>(s);	
    fSource << ss->GetNedges() << ", " << nz << ");" << std::endl;
  }
  else {
    fSource << nz << ");" << std::endl;
  }
  for (Int_t i = 0; i < nz; i++)
    fSource << indent2 << vol << "_Shape->DefineSection(" << i << ","
	    << s->GetZ(i) << "," << s->GetRmin(i) << ", " 
	    << s->GetRmax(i) << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteHalfSpace(TGeoHalfSpace* s, const char* vol)
{
  fSource << indent2 << "Double_t " << vol << "_Shape_P[] = { " 
	  << s->GetPoint()[0] << "," << s->GetPoint()[1] 
	  << "," << s->GetPoint()[2] << "};\n" 
	  << indent2 << "Double_t " << vol << "_Shape_N[] = { " 
	  << s->GetNorm()[0] << "," << s->GetNorm()[1] 
	  << "," << s->GetNorm()[2] << "};"  << std::endl;
  SHAPE_HEAD(s,vol);
  fSource << vol << "_Shape_P, " << vol << "_Shape_N);" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WritePara(TGeoPara* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetX() << "," << s->GetY() << "," << s->GetZ() << ","
	  << s->GetAlpha() << "," << s->GetTheta() << "," 
	  << s->GetPhi() << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteParaboloid(TGeoParaboloid* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetRlo() << "," << s->GetRhi() << "," << s->GetDz() 
	  << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteSphere(TGeoSphere* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetRmin() << "," << s->GetRmax() << "," 
	  << s->GetTheta1() << "," << s->GetTheta2() << "," 
	  << s->GetPhi1() << "," << s->GetPhi2() << ");" 
	  << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteTorus(TGeoTorus* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetR() <<"," << s->GetRmin() << "," 
	  << s->GetRmax() << "," << s->GetPhi1() << "," 
	  << s->GetDphi() << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteTrd1(TGeoTrd1* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetDx1() << "," << s->GetDx2() << ","
	  << s->GetDy() << "," << s->GetDz() << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteTrd2(TGeoTrd2* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetDx1() << "," << s->GetDx2() << ","
	  << s->GetDy1() << "," << s->GetDy2() << "," 
	  << s->GetDz() << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteXtru(TGeoXtru* s, const char* vol)
{
  Int_t nz = s->GetNz();
  Int_t nvert = s->GetNvert();
  SHAPE_HEAD(s,vol);
  fSource << nz << ");\n" 
	  << indent << "{\n" 
	  << indent << "  Double_t polyx[] = { " << std::flush;
  for (Int_t i = 0; i < nvert; i++) 
    fSource << s->GetX(i) << (i != nvert - 1 ? "," : " };");
  fSource << "\n"
	  << indent << "  Double_t polyy[] = { " << std::flush;
  for (Int_t i = 0; i < nvert; i++) 
    fSource << s->GetY(i) << (i != nvert - 1 ? "," : " };");
  fSource << "\n"
	  << indent << "  " << vol << "_Shape->DefinePolygon(" 
	  << nvert << ", polyx, polyy);\n" 
	  << indent << "}" << std::endl;
  for (Int_t i = 0; i < nz; i++) {
    fSource << indent << vol << "_Shape->DefineSection(" << i << ", "
	    << s->GetZ(i) << ", " << s->GetXOffset(i) << ", "
	    << s->GetYOffset(i) << ", " << s->GetScale(i) << ");" 
	    << std::endl;
  }
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteTrap(TGeoTrap* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetDz() << "," << s->GetTheta() << "," 
	  << s->GetPhi() << "," << s->GetH1() << "," 
	  << s->GetBl1() << "," << s->GetTl1() << ","
	  << s->GetAlpha1() << "," << s->GetH2() << "," 
	  << s->GetBl2() << "," << s->GetTl2() << "," 
	  << s->GetAlpha2() << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteGtra(TGeoGtra* s, const char* vol)
{
  SHAPE_HEAD(s,vol);
  fSource << s->GetDz() << "," << s->GetTheta() << "," 
	  << s->GetPhi() << "," << s->GetTwistAngle() << "," 
	  << s->GetH1() << "," << s->GetBl1() << "," 
	  << s->GetTl1() << "," << s->GetAlpha1() 
	  << "," << s->GetH2() << "," << s->GetBl2() 
	  << "," << s->GetTl2() << "," << s->GetAlpha2() 
	  << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteArb8(TGeoArb8* s, const char* vol)
{
  fSource << indent2 << "Double_t " << vol << "_Shape_V[] = {" 
	  << std::flush;
  Double_t* v = s->GetVertices();
  for (Int_t i = 0; i < 16; i++) {
    Int_t ix = i / 2;
    Int_t iy = i % 2;
    fSource << v[ix + iy * 8] << (i != 15 ? "," : "};");
  }
  fSource << std::endl;
  SHAPE_HEAD(s,vol);
  fSource << s->GetDz() << ", " << vol << "_Shape_V);" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteCompositeShape(TGeoCompositeShape* s, 
					       const char*)
{
  if (s->GetBoolNode()) {
    std::cerr << "Composite shape with boolean node not supported" 
	      << std::endl;
  }
}
    
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteScaledShape(TGeoScaledShape*,const char*) 
{}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteShapeAssembly(TGeoShapeAssembly*,const char*) 
{}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteNodes() 
{
  TObjArray tops;
  indent2.inc();
  for (Int_t i = 0; i < fVolumes.GetEntries(); i++) {
    TGeoVolume* vol = static_cast<TGeoVolume*>(fVolumes.At(i));
    if (vol->GetFinder()) {
      // Divided volume 
      TGeoNode*   dnode = vol->GetNode(0);
      TGeoVolume* dvol  = dnode->GetVolume();
      TGeoMedium* dmed  = dvol->GetMedium();
      if (dmed != dvol->GetMedium()) {
	TString mname(TransName(dmed->GetName()));
	fSource << indent2 << "TGeoMedium* " << dvol->GetName() 
		<< "Med = gGeoManager->GetMedium(\"" << mname << "\");"
		<< std::endl;
      }
      fSource << indent2 << "TGeoVolume* " << dvol->GetName() 
	      << "_Vol = " << vol->GetName() << "_Vol->Divide(\"" 
	      << dvol->GetName() << "\"," << std::flush;
      vol->GetFinder()->SavePrimitive(fSource,"");
      if (dmed != dvol->GetMedium()) 
	fSource << ", " << dvol->GetName() << "Med->GetId()" << std::flush;
      fSource << ");" << std::endl;
      if (dmed->GetParam(0) != 0) {
	fSource << indent2 << "RegisterVolume(" << dvol->GetName() 
		<< "_Vol);" << std::endl;
	fCurSens++;
      }   
    }
    else {
      Int_t nd = vol->GetNdaughters();
      for (Int_t i = 0; i < nd; i++) {
	TGeoNode*   dnode = vol->GetNode(i);
	TGeoVolume* dvol  = dnode->GetVolume();
	if (!fVolumes.FindObject(dvol)) {
	  MakeTask* which = FindVolume(dvol);
	  if (!which) {
	    std::cerr << "No handler of " << dvol->GetName() << " in " 
		      << fName << std::endl;
	    continue;
	  }
	  int n = indent2.n;
	  indent2.n = 2;
	  which->WriteNode(dnode, vol);
	  indent2.n = n;
	  continue;
	}
	WriteNode(dnode, vol);
      }
    }
  }
  indent2.dec();
  for (Int_t i = 0; i < fNsubs; i++) fSubs[i]->WriteNodes();
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteNode(TGeoNode* dnode, TGeoVolume* vol) 
{
    
  if (!fVolumes.FindObject(vol)) {
    if (!fDoneVol.FindObject(vol)) {
      fSource << indent2 << "TGeoVolume* " << vol->GetName() << "_Vol = "
	      << "gGeoManager->GetVolume(\"" << vol->GetName() << "\");\n"
	      << indent2 << "if (!" << vol->GetName() << "_Vol) {\n"
	      << indent2 << "  Warning(\"Initialize\", \"Volume " 
	      << vol->GetName() << " not found \");\n"
	      << indent2 << "  return;\n" 
	      << indent2 << "}" << std::endl;
      fDoneVol.Add(vol);
    }
  }
  TGeoVolume* dvol  = dnode->GetVolume();
  TGeoMatrix* matrix = dnode->GetMatrix();
  Int_t icopy = dnode->GetNumber();
  WriteMatrix(matrix, dnode->GetName(), icopy,1);
  fSource << indent2 << vol->GetName() << "_Vol->AddNode";
  if (dnode->IsOverlapping()) fSource << "Overlap";
  fSource << "(" << dvol->GetName() << "_Vol, " << icopy;
  if (!matrix->IsIdentity()) WriteMatrix(matrix, dnode->GetName(), icopy,2);
  fSource << ");" << std::endl;
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteMatrix(TGeoMatrix* m, const char* node, 
				       Int_t copy, Int_t pass)
{
  if (pass == 1) {
    if (m->IsA() == TGeoIdentity::Class()) return;
    if (m->IsA() == TGeoTranslation::Class()) return;
    else if (m->IsA() == TGeoCombiTrans::Class() || 
	     m->IsA() == TGeoRotation::Class()) {
      TGeoRotation* r = 0;
      if (m->IsA() == TGeoCombiTrans::Class()) 
	r = static_cast<TGeoCombiTrans*>(m)->GetRotation();
      else 
	r = static_cast<TGeoRotation*>(m);
      if (!r) {
	return;
      }
      Double_t phi1, phi2, phi3, theta1, theta2, theta3;
      r->GetAngles(theta1, phi1, theta2, phi2, theta3, phi3);
      fSource << indent2 << "TGeoRotation* " << node << copy
	      << "Rot = new TGeoRotation(\"" << node 
	      << copy << "Rot\", " << theta1 << ", " << phi1 << ", "
	      << theta2 << ", " << phi2 << ", " << theta3 << ", " 
	      << phi3 << ");" << std::endl;
      return;
    }
    else 
      std::cerr << "Matrix type " << m->ClassName() 
		<< " from " << node << " " << copy 
		<< " not supported " << pass << std::endl ;
  }
  else {
    if (m->IsA() == TGeoTranslation::Class() || 
	m->IsA() == TGeoCombiTrans::Class()) {
      const char* cname = m->ClassName();
      if (m->IsA() == TGeoCombiTrans::Class() && 
	  !static_cast<TGeoCombiTrans*>(m)->GetRotation()) 
	cname = "TGeoTranslation";
      fSource << ", new " << cname << "(" 
	      << m->GetTranslation()[0] << "," 
	      << m->GetTranslation()[1] << "," 
	      << m->GetTranslation()[2] << std::flush;
      if (m->IsA() == TGeoCombiTrans::Class() && 
	  static_cast<TGeoCombiTrans*>(m)->GetRotation()) 
	fSource << ", " << node << copy << "Rot" << std::flush;
      fSource << ")" << std::flush;
    }
    else if (m->IsA() == TGeoRotation::Class()) 
      fSource << ", " << node << copy << "Rot" << std::flush;
    else {
      std::cerr << "Matrix type " << m->ClassName() 
		<< " from " << node << " " << copy 
		<< " not supported " << pass << std::endl;
    }
  }
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteFile()
{
  fSource << "}" << std::endl;
  if (fNSens > 0)
    fSource << rule2 << "\n"
	    << indent2 << "void\n"
	    << fName << "::Step()\n"
	    << indent2 << "{\n" 
	    << indent2 << "  if (!fActive) return;\n"
	    << indent2 << "  TVirtualMC* mc = TVirtualMC::GetMC();\n"  
	    << indent2 << "  Int_t copy;\n"
	    << indent2 << "  Int_t vol = mc->CurrentVolID(copy);\n" 
	    << indent2 << "  if (!IsSensitive(vol)) return;\n\n" 
	    << indent2 << "  // Fill in stepping function for " << fName 
	    << " here\n" 
	    << indent2 << "  CreateDefaultHit();\n"
	    << indent2 << "}\n" << std::endl;
  if (fHaveField) 
    fSource << rule2   << "\n" 
	    << indent2 << "void\n"
	    << indent2 << fName << "::Field(const TVector3& x,TVector3& b)\n"
	    << indent2 << "{\n"
	    << indent2 << "  // Fill in magnetic field code here\n"
	    << indent2 << "  b[0] = b[1] = b[2] = 0;\n"
	    << indent2 << "}\n"
	    << std::endl;
  fSource << rule2 << "\n" 
	  << indent2 << "// EOF\n" 
	  << indent2 << "//\n" 
	  << std::endl;
  fSource.close();

  TString fname (Form("%s.h", fName.Data()));
  fSource.open(fname.Data());
  fSource << "// -*- mode: C++ -*- \n" 
	  << "#ifndef " << fName << "_h\n"
	  << "#define " << fName << "_h\n" 
	  << "#ifndef Simulation_Task\n" 
	  << "# include <simulation/Task.h>\n"  
	  << "#endif" << std::endl;
  fSource << "\nclass " << fName << " : public Simulation::Task\n"
	  << "{\n"
	  << "public:\n"
	  << "  " << fName << "();\n" 
	  << "  void Register(Option_t* option=\"\");\n" 
	  << "  void Initialize(Option_t* option=\"\");" << std::endl; 
  if (fHaveField) 
    fSource << "  void Field(const TVector3& x, TVector3& b);"
	    << std::endl;
  if (fNSens > 0) 
    fSource << "  void Step();" << std::endl;
  fSource << "  void SetActive(Int_t on=1) { fActive = on; }\n"
	  << "protected:\n"
	  << "  Int_t fActive;\n" 
	  << "public:\n" 
	  << "  ClassDef(" << fName << ",0); // Simulation task " 
	  << fName << "\n"
	  << "};\n\n" 
	  << "#endif\n" 
	  << rule2 << "\n"
	  << "// \n" 
	  << "// EOF\n"
	  << "// \n" << std::endl;
  fSource.close();
  for (Int_t i = 0; i < fNsubs; i++) fSubs[i]->WriteFile();
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::Extract(TGeoVolume* vol) 
{
  AddVolume(vol);
  // Do not process a divided volume 
  if (vol->GetFinder()) return;
  Int_t  nd = vol->GetNdaughters();
  for (Int_t i = 0; i < nd; i++) {
    TGeoNode*   dnode = vol->GetNode(i);
    TGeoVolume* dvol  = dnode->GetVolume();
    TString dvolName(dvol->GetName());
    MakeTask* next = this;
    const char* nnext = FindMakeTaskName(dvolName.Data());
    if (nnext) next = AddMakeTask(nnext);
    indent.inc();
    next->Extract(dvol);
    indent.dec();
  }
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteMakefile(const char* name) 
{
  std::ofstream mf("Makefile.am");
  mf << "#\n" 
     << "#\n" 
     << "#\n" 
     << "AM_CPPFLAGS\t\t\t= @FRAMEWORK_CPPFLAGS@\t\\\n"
     << "\t\t\t\t  @SIMULATION_CPPFLAGS@\t\\\n" 
     << "\t\t\t\t  -I@ROOTINCDIR@\n"
     << "EXTRA_DIST\t\t\t= Config.C\n"
     << "pkglib_LTLIBRARIES\t\t= lib" << name << ".la\n" 
     << "pkginclude_HEADERS\t\t= \\" << std::endl;
  for (Int_t i = 0; i < fNsubs; i++) 
    fSubs[i]->WriteMakeFile(mf, "h");
  mf << "\t\t\t\t  " << fName << ".h\n"
     << "noinst_HEADERS\t\t\t= LinkDef.h\n"
     << "lib" << name << "_la_LDFLAGS\t\t= -L@ROOTLIBDIR@\t\\\n"
     << "\t\t\t\t  -R @ROOTLIBDIR@\t\\\n"
     << "\t\t\t\t  @SIMULATION_LDFLAGS@\n"
     << "lib" << name << "_la_LIBADD\t\t= "
     << "-lCint -lCore -lTree -lEG -lSimulation\n"
     << "lib" << name << "_la_SOURCES\t\t= \\" << std::endl;
  for (Int_t i = 0; i < fNsubs; i++) 
    fSubs[i]->WriteMakeFile(mf, "cxx");
  mf << "\t\t\t\t  " << fName << ".cxx\n"
     << "nodist_lib" << name << "_la_SOURCES\t= Dict.cxx Dict.h\n\n"
     << "Dict.cxx:$(pkginclude_HEADERS) $(noinst_HEADERS)\n" 
     << "\t$(ROOTCINT) -f Dict.cxx -c $(AM_CPPFLAGS) $(CPPFLAGS) $^\n"
     << "Dict.h:Dict.cxx\n"
     << "\tif test -f $@ ; then : else rm -f $^ ; $(MAKE) $^ ; fi\n\n" 
     << "extract:makeit.C\n"
     << "\trm -rf $(wildcard *.h) $(wildcard *.cxx)\n"
     << "\troot -l -q load.C makeit.C++\n\n"
     << "# \n"
     << "# EOF\n"
     << "# \n"
     << std::endl;
  mf.close();
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteMakeFile(std::ostream& mf, const char* ext) 
{
  TString fname(Form("%s.%s", fName.Data(), ext));
  mf << "\t\t\t\t  " << std::left << std::setw(24) 
     << fname << "\\" << std::endl;
  for (Int_t i = 0; i < fNsubs; i++) fSubs[i]->WriteMakeFile(mf, ext);
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WritePragma(std::ofstream& ld) 
{
  ld << "#pragma link C++ class " << fName << "+;" <<  std::endl;
  for (Int_t i = 0; i < fNsubs; i++) fSubs[i]->WritePragma(ld);
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteLinkDef() 
{
  std::ofstream ld("LinkDef.h");
  ld << "// -*- mode: C++ -*-\n" 
     << "#ifndef __CINT__\n"
     << "# error Not for compilation\n" 
     << "#endif\n\n"
     << "#pragma link off all classes;\n"
     << "#pragma link off all globals;\n"
     << "#pragma link off all functions;\n" <<std::endl;
  WritePragma(ld);
  ld << "//\n " 
     << "// EOF\n"
     << "// \n"
     << std::endl;
  ld.close();
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteAdd(std::ofstream& cfg) 
{
  cfg << rule2 << "\n" 
      << indent2 << fName << "* p" << fName << " = new " << fName << ";\n" 
      << indent2 << "p" << fName << "->SetActive();\n" 
      << indent2 << "main->Add(p" << fName << ");\n" << std::endl; 
  if (fNsubs > 0) {
    cfg << indent2 << "{" << std::endl;
    indent2.inc();
    for (Int_t i = 0; i < fNsubs; i++) fSubs[i]->WriteAdd(cfg);
    indent2.dec();
    cfg << indent2 << "}\n" << std::endl;
  }
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::WriteConfig(const char* name) 
{
  std::ofstream cfg("Config.C");
  cfg << "void Config()\n" 
      << "{" << std::endl;
  indent2.inc();
  cfg << "  gSystem->Load(\"lib" << name << ".so\");\n\n"
      << "  Simulation::Main* main =\n"
      << "    new Simulation::Main(\"" << name 
      << "\", \"Montecarlo\");\n\n"
      << rule2   << "\n" 
      << "  Simulation::Generator* generator = \n"
      << "    new Simulation::Generator(\"generator\", \"\");\n"
      << "  main->Add(generator);\n"
      << std::endl;
  WriteAdd(cfg);
  cfg << rule2   << "\n" 
      << "  Simulation::Display* display = new Simulation::Display;\n"
      << "  main->Add(display);\n\n" 
      << rule2   << "\n"
      << "  Simulation::TreeWriter* writer = \n" 
      << "    new Simulation::TreeWriter(\"Hits\",\"hits.root\");\n"
      << "  main->Add(writer);\n" << std::endl;
  cfg << rule2   << "\n" 
      << "  gSystem->Load(\"libTHijing.so\");\n"
      << "  THijing* hijing = new THijing(\"hijing\", \"Hijing\");\n"
      << "  hijing->Initialize(200,\"CMS\",\"A\",\"A\",179,92,179,92);\n"
      << "  hijing->SetParameter(\"IHPR2\", 4 ,1); // Jet Quencing\n"
      << "  hijing->SetParameter(\"IHPR2\", 6, 1); // Shadowing\n"
      << "  generator->SetGenerator(hijing);\n\n"
      << "  THijingPara* hijingp = new THijingPara(\"hijingp\");\n"
      << "  hijingp->Initialize(600);\n"
      << "  generator->SetGenerator(hijingp);\n\n" 
      << rule2 << "\n"
      << "  gSystem->Load(\"libEGPythia6.so\");\n"
      << "  gSystem->Load(\"libdummies.so\");\n"
      << "  gSystem->Load(\"libgeant321.so\");\n"
      << "  TGeant3TGeo* mc = new TGeant3TGeo(\"Geant 3.21\");\n" 
      << "  mc->SetRootGeometry();\n\n"
      << rule2 << "\n"
      << "  if (!gROOT->IsBatch()) {\n"
      << "    new TBrowser(\"b\");\n"
      << "    cout << \"Setup complete. Generate one event:\\n\\n\"\n"
      << "         << \"\\tSimulation::Main::Instance()->Loop();\\n\"\n"
      << "         << endl;\n" 
      << "  }" << std::endl;
  indent2.dec();
  cfg << "}\n\n"
      << "// \n"
      << "// EOF\n"
      << "// \n"
      << std::endl;
  cfg.close();
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::AddMap(const char* vol, const char* lvol) 
{
  fMap.Add(new TNamed(vol, ""), new TNamed(lvol, ""));
}
//____________________________________________________________________
void 
Simulation::Util::MakeTask::AddMap(const char* vol) 
{
  fMap.Add(new TNamed(vol, ""), new TNamed(vol, ""));
}
//____________________________________________________________________
const char* 
Simulation::Util::MakeTask::FindMakeTaskName(const char* vol) 
{
  TPair* res = static_cast<TPair*>(fMap.FindObject(vol));
  if (!res) return 0;
  return res->Value()->GetName();
}

//____________________________________________________________________
//
// EOF
//


    
