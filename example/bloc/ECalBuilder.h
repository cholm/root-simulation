// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: ECalBuilder.h,v 1.1 2005-12-13 18:56:08 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/ECalBuilder.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Example::ECalBuilder
*/
#ifndef Example_ECalBuilder
#define Example_ECalBuilder
#include <framework/Task.h>

namespace Example 
{
  /** The main volume of the experiment.  This is just a tube of air
      large enough to contain the tracker and leak volumes.

      Note that this is _not_ a Simulation::Task but a
      Framework::Task, which is why we need to define `Register` and
      `Exec`.
  */ 
  class ECalBuilder : public Framework::Task 
  {
  protected:
    /** Wether to use lead glass or BGO as tracking medium */
    Bool_t fUseBgo;
  public:
    ECalBuilder(Bool_t useBGO=false);
    void Initialize(Option_t* option="");
    void Register(Option_t* option="") {}
    void Exec(Option_t* option="") {}
    ClassDef(ECalBuilder,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
