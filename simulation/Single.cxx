//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/Single.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Single
*/
#include "simulation/Single.h"
#include <TClonesArray.h>
#include <TVector3.h>
#include <TParticle.h>
#include <TParticlePDG.h>
#include <TDatabasePDG.h>
#include <TMath.h>
#include <iostream>
#define DEGRAD TMath::Pi() / 180.

//____________________________________________________________________
ClassImp(Simulation::Single);

//____________________________________________________________________
Simulation::Single::Single() 
  : TGenerator("single", "A simple single particle generator")
{
  fParticles = new TClonesArray("TParticle");
  SetPdgCode();
}

//____________________________________________________________________
void
Simulation::Single::SetPdgCode(Int_t pdg) 
{
  TDatabasePDG* pdgDb = TDatabasePDG::Instance();
  TParticlePDG* pdgP  = pdgDb->GetParticle(pdg);
  if (!pdgP) return;
  fPdgCode     = pdg;
  Double_t m   = pdgP->Mass();
  Double_t px  = fP.Px();
  Double_t py  = fP.Py();
  Double_t pz  = fP.Pz();
  Double_t e   = TMath::Sqrt(m*m + px*px + py*py + pz * pz);
  fP.SetPxPyPzE(px,py,pz,e);
}

//____________________________________________________________________
void
Simulation::Single::SetPdg(const char* pdg) 
{
  TDatabasePDG* pdgDb = TDatabasePDG::Instance();
  TParticlePDG* pdgP  = pdgDb->GetParticle(pdg);
  if (!pdgP) return;
  SetPdgCode(pdgP->PdgCode());
}

//____________________________________________________________________
void
Simulation::Single::SetTheta(Double_t theta) 
{
  fP.SetTheta(theta * DEGRAD);
}

//____________________________________________________________________
void
Simulation::Single::SetPhi(Double_t phi) 
{
  fP.SetPhi(phi * DEGRAD);
}

//____________________________________________________________________
void
Simulation::Single::SetPt(Double_t pt) 
{
  Double_t px  = TMath::Cos(fP.Phi()) * pt;
  Double_t py  = TMath::Sin(fP.Phi()) * pt;
  Double_t pz  = fP.Pz();
  SetP(px, py, pz);
}

//____________________________________________________________________
void
Simulation::Single::SetP(Double_t px, Double_t py, Double_t pz) 
{
  Double_t m   = GetMass();
  Double_t e   = TMath::Sqrt(m*m + px*px + py*py + pz * pz);
  fP.SetPxPyPzE(px, py, pz, e);
}

//____________________________________________________________________
void
Simulation::Single::SetP(Double_t p)
{
  Double_t pt = TMath::Sin(fP.Theta()) * p;
  Double_t pz = TMath::Cos(fP.Theta()) * p;
  Double_t px = TMath::Cos(fP.Phi()) * pt;
  Double_t py = TMath::Sin(fP.Phi()) * pt;
  SetP(px,py,pz);
}

//____________________________________________________________________
void
Simulation::Single::SetY(Double_t y) 
{
  Double_t m  = GetMass();
  Double_t pt = fP.Pt();
  Double_t mt = TMath::Sqrt(pt*pt + m * m);
  Double_t pz = mt * TMath::SinH(y);
  fP.SetPz(pz);
}

//____________________________________________________________________
TParticlePDG*
Simulation::Single::GetPdgParticle() const
{
  TDatabasePDG* pdgDb = TDatabasePDG::Instance();
  TParticlePDG* pdgP  = pdgDb->GetParticle(fPdgCode);
  return pdgP;
}
  
//____________________________________________________________________
Double_t
Simulation::Single::GetMass() const
{
  return GetPdgParticle()->Mass();
}
  
//____________________________________________________________________
Int_t
Simulation::Single::ImportParticles(TClonesArray* a, Option_t* option) 
{
  a->Clear();
  TParticlePDG* pdgP = GetPdgParticle();
  TParticle*    pp   = new ((*a)[0]) TParticle(fPdgCode, 0, -1, -1, -1, -1, 
					       fP.Px(), fP.Py(), fP.Pz(), 
					       fP.Energy(), 0, 0, 0, 0);
  pp->SetLineColor(kGreen);
  return 1;
}


//____________________________________________________________________
TObjArray* 
Simulation::Single::ImportParticles(Option_t* option) 
{
  ImportParticles(static_cast<TClonesArray*>(fParticles), option);
  return fParticles;
}


//____________________________________________________________________
//
// EOF
//
