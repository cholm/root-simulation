// -*- mode: C++ -*-
#ifndef SIMULATION_UTIL_MakeTask
#define SIMULATION_UTIL_MakeTask
#include <TObjArray.h>
#include <TMap.h>
#include <TNamed.h>
#include <fstream>
class TGeoArb8;
class TGeoBBox;
class TGeoCompositeShape;
class TGeoCone;
class TGeoCtub;
class TGeoEltu;
class TGeoGeometry;
class TGeoGtra;
class TGeoHalfSpace;
class TGeoHype;
class TGeoMaterial;
class TGeoMatrix;
class TGeoMedium;
class TGeoMixture;
class TGeoNode;
class TGeoPara;
class TGeoParaboloid;
class TGeoPcon;
class TGeoScaledShape;
class TGeoShape;
class TGeoShapeAssembly;
class TGeoSphere;
class TGeoTorus;
class TGeoTrap;
class TGeoTrd1;
class TGeoTrd2;
class TGeoTube;
class TGeoVolume;
class TGeoXtru;

namespace Simulation 
{
  /** @defgroup sim_utils Utility classes 
      @brief This group contains utility code */
  /** @namespace Util 
      @brief Namespace for some utility classes 
      @ingroup sim_utils
  */
  namespace Util 
  {
    /** Create a set of tasks based on a passed TGeoGeometry. 
	@code
	void
	makeit() 
	{
	  gROOT->Macro("full.C");
	  MakeTask::AddMap("BBCL", "BB");
	  MakeTask::AddMap("BBCR", "BB");
	  MakeTask::AddMap("BEVI", "BEAM");
	  MakeTask::AddMap("BP4O", "BEAM");
	  MakeTask::AddMap("BPHB", "BEAM");
	  MakeTask::AddMap("BPHC", "BEAM");
	  MakeTask::AddMap("BPHH", "BEAM");
	  MakeTask::AddMap("BPHV", "BEAM");
	  MakeTask::AddMap("BPP1", "BEAM");
	  MakeTask::AddMap("FLTA", "BEAM");
	  MakeTask::AddMap("DX",   "DX");
	  MakeTask::AddMap("DXCL", "DX");
	  MakeTask::AddMap("DXPO", "DX");
	  MakeTask::AddMap("INV1", "INEL");
	  MakeTask::AddMap("INV2", "INEL");
	  MakeTask::AddMap("INV3", "INEL");
	  MakeTask::AddMap("INV4", "INEL");
	  MakeTask::AddMap("INV5", "INEL");
	  MakeTask::AddMap("INV6", "INEL");
	  MakeTask::AddMap("INV7", "INEL");
	  MakeTask::AddMap("INV8", "INEL");
	  MakeTask::AddMap("SBXA", "SMA");
	  MakeTask::AddMap("PREB", "SMA");
	  MakeTask::AddMap("PREA", "SMA");
	  MakeTask::AddMap("PFIN", "SMA");
	  MakeTask::AddMap("T1VV", "T1");
	  MakeTask::AddMap("T2VV", "T2");
	  MakeTask::AddMap("S2T2", "T2");
	  MakeTask::AddMap("S1T2", "T2");
	  MakeTask::AddMap("M1VV", "TPM1");
	  MakeTask::AddMap("MMS1", "TPM1");
	  MakeTask::AddMap("MMS2", "TPM1");
	  MakeTask::AddMap("MMS3", "TPM1");
	  MakeTask::AddMap("M2VV", "TPM2");
	  MakeTask::AddMap("MMS4", "TPM2");
	  MakeTask::AddMap("MMS5", "TPM2");
	  MakeTask::AddMap("MMS6", "TPM2");
	  MakeTask::AddMap("TFVV", "TOFW");
	  MakeTask::AddMap("TILE", "TMA");
	  MakeTask::AddMap("MIDS", "MRS");
	  MakeTask::AddMap("FMS1", "FFS");
	  MakeTask::AddMap("FPPB", "FFS");
	  MakeTask::AddMap("FPPT", "FFS");
	  MakeTask::AddMap("FMS2", "BFS");
	  MakeTask::AddMap("MULT"); 
	  MakeTask::AddMap("ZCAL", "ZDC"); 
	  MakeTask::AddMap("D1");
	  MakeTask::AddMap("SD1B", "D1");
	  MakeTask::AddMap("SD1A", "D1");
	  MakeTask::AddMap("D1TR"); 
	  MakeTask::AddMap("C1");
	  MakeTask::AddMap("D2"); 
	  MakeTask::AddMap("TFSV", "TFS");
	  MakeTask::AddMap("TOF1"); 
	  MakeTask::AddMap("WF1V", "WF1");
	  MakeTask::AddMap("WF2V", "WF2"); 
	  MakeTask::AddMap("D3");
	  MakeTask::AddMap("D4"); 
	  MakeTask::AddMap("RICH");
	  MakeTask::AddMap("T3"); 
	  MakeTask::AddMap("T4"); 
	  MakeTask::AddMap("T5"); 
	  MakeTask::AddMap("TOF2"); 
	  MakeTask::AddMap("C4"); 
	  MakeTask::AddMap("M0", "D5"); 
	  MakeTask::AddMap("WM1V", "WM1"); 
	  MakeTask::AddMap("WM2V", "WM2");
	  TGeoVolume* top = gGeoManager->GetTopVolume();
	  MakeTask* topl   = new MakeTask("CAVE");
	  topl->AddVolume(top);
	  topl->Extract(top);
	  topl->Print();
	  topl->Write();
	  topl->WriteMakefile("Brag");
	  topl->WriteLinkDef();
	  topl->WriteConfig("Brag");
	}
	@endcode 
     */
    class MakeTask : public TNamed
    {
    public:
      /** Constructor 
	  @param name Name */
      MakeTask(const char* name);
      /** Add another logical division 
	  @param name Name of the logical devision
	  @return Representation of logical division */
      MakeTask* AddMakeTask(const char* name);      
      /** Find a logical division. 
	  @param name Name of the logical division 
	  @return 0 if not found, pointer otherwise */
      MakeTask* FindMakeTask(const char* name);
      /** Find a logical division that contains a volume 
	  @param vol volume to look for
	  @return pointer if found, 0 otherwise */
      MakeTask* FindVolume(TGeoVolume* vol);
      /** Add a volume to a the list of volumes handled by this
	  division. 
	  @param vol Volume to add */
      void AddVolume(TGeoVolume* vol);
      /** Print information to standard output 
	  @param option Not used. */
      void Print(Option_t* option="") const;
      /** Write to output. */
      void Write();
      /** Extract information from passed volume. 
	  @param vol Volume to extract information from. */
      void Extract(TGeoVolume* vol);
      /** Write make file for name
	  @param name Name */
      void WriteMakefile(const char* name);
      /** Write a link def file. */
      void WriteLinkDef();
      /** Write a configuration stub. 
	  @param name Name of config. */
      void WriteConfig(const char* name);
      /** Add a mapping from volume name to a Task 
	  @param vol  Volume name 
	  @param lvol Task name */
      static void AddMap(const char* vol, const char* lvol);
      /** Add a mapping from volume name to a Task.  Task will have
	  same name as volume 
	  @param vol  Volume name */
      static void AddMap(const char* vol);
      /** Find a logical corresponding to a volume name. 
	  @param vol Volume name. 
	  @return Name of logical corresponding to volume. */
      static const char* FindMakeTaskName(const char* vol);
    protected:
      /** Write top of source file. */
      void WriteTop();
      /** Translate a name 
	  @param name Name to translate 
	  @param specific Specific to this logical division?
	  @return Transformed name */
      TString TransName(const char* name, Bool_t specific=kFALSE);

      /** @{ */
      /** @name Materials, Mixtures and Mediums */
      /** Write mediums to output */
      void WriteMediums();
      /** Write materials to output. */
      void WriteMaterials();
      /** Write a medium to output 
	  @param med Medium to output. 
	  @param spec Whether it's spefic to this division. */
      void WriteMedium(TGeoMedium* med, Bool_t spec=kFALSE);
      /** Write a material to output. 
	  @param mat Material to output. 
	  @param spec Whether it's spefic to this division. */
      void WriteMaterial(TGeoMaterial* mat, Bool_t spec=kFALSE);
      /** Write a material to output. 
	  @param mat Material to output. 
	  @param spec Whether it's spefic to this division. */
      void WriteMixture(TGeoMixture* mat, Bool_t spec=kFALSE);
      /** @} */

      /** Write volumes to output. */
      void WriteVolumes();


      /** @{*/
      /** @name Shape handling */
      /** Write a shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteShape(TGeoShape* shape, const char* vol);
      /** Write a TGeoBBox shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteBBox(TGeoBBox* shape, const char* vol);
      /** Write a TGeoHype shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteHype(TGeoHype* shape, const char* vol);
      /** Write a TGeoEltu shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteEltu(TGeoEltu* shape, const char* vol);
      /** Write a TGeoTube shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteTube(TGeoTube* shape, const char* vol);
      /** Write a TGeoCtub shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteCtub(TGeoCtub* shape, const char* vol);
      /** Write a TGeoCone shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteCone(TGeoCone* shape, const char* vol);
      /** Write a TGeoPcon shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WritePcon(TGeoPcon* shape, const char* vol);
      /** Write a TGeoHalfSpace shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteHalfSpace(TGeoHalfSpace* shape, const char* vol);
      /** Write a TGeoPara shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WritePara(TGeoPara* shape, const char* vol);
      /** Write a TGeoParaboloid shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteParaboloid(TGeoParaboloid* shape, const char* vol);
      /** Write a TGeoSphere shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteSphere(TGeoSphere* shape, const char* vol);
      /** Write a TGeoTorus shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteTorus(TGeoTorus* shape, const char* vol);
      /** Write a TGeoTrd1 shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteTrd1(TGeoTrd1* shape, const char* vol);
      /** Write a TGeoTrd2 shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteTrd2(TGeoTrd2* shape, const char* vol);
      /** Write a TGeoXtru shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteXtru(TGeoXtru* shape, const char* vol);
      /** Write a TGeoTrap shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteTrap(TGeoTrap* shape, const char* vol);
      /** Write a TGeoGtra shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteGtra(TGeoGtra* shape, const char* vol);
      /** Write a TGeoArb8 shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteArb8(TGeoArb8* shape, const char* vol);
      /** Write a TGeoCompositeShape shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteCompositeShape(TGeoCompositeShape* shape, const char* vol);
      /** Write a TGeoScaledShape shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteScaledShape(TGeoScaledShape* shape, const char* vol);
      /** Write a TGeoShapeAssembly shape to output. 
	  @param shape Shape to write
	  @param vol Volume name. */
      void WriteShapeAssembly(TGeoShapeAssembly* shape, const char* vol);
      /**@}*/

      /** @{ */
      /** @name Nodes and Matrices */
      /** Write nodes to output */
      void WriteNodes();
      /** Write a node to output. 
	  @param dnode Daughter node 
	  @param vol Mother volume. */
      void WriteNode(TGeoNode* dnode, TGeoVolume* vol);      
      /** Write a transformation matrix. 
	  @param m    Matrix 
	  @param node Node name
	  @param copy Copy number 
	  @param pass Pass. */
      void WriteMatrix(TGeoMatrix* m, const char* node, 
		       Int_t copy, Int_t pass);
      /** @} */

      /** Write top-level make file. */
      void WriteFile();
      /** Write makefile entry 
	  @param mf  Stream. 
	  @param ext Extension. */
      void WriteMakeFile(std::ostream& mf, const char* ext);
      /** Write a pragma to linkdef file. 
	  @param ld Stream */
      void WritePragma(std::ofstream& ld);
      /** Write an add statement to configuration file. 
	  @param cfg Stream */
      void WriteAdd(std::ofstream& cfg);

      /** list of volumes asociated with this */
      TObjArray     fVolumes;    
      /** Number of subs  */
      Int_t         fNsubs;      
      /** List of logical sub volumes  */
      MakeTask**     fSubs;       
      /** Output file. */
      std::ofstream fSource;     
      /** # of Sensitive volumes */
      Int_t         fNSens;      
      /** Current sensitive volume  */
      Int_t         fCurSens;    
      /** List of materials done  */
      TObjArray     fDoneMat;    
      /** List of mediums done  */
      TObjArray     fDoneMed;    
      /** List of volumes done  */
      TObjArray     fDoneVol;    
      /** Whether there's a magnetic field or not  */
      Bool_t        fHaveField;  
      /** Map  */
      static TMap   fMap;        
      /** Is this the  top volume */
      static Bool_t fIsTop;      
      // ClassDef(MakeTask,0);
    };
  }
}
      
#endif
//
// EOF
//
