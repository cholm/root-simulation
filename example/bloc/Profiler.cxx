//____________________________________________________________________ 
//  
// $Id: Profiler.cxx,v 1.8 2005-12-05 15:19:17 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#include <TBranch.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH1F.h>
#include <TParticle.h>
#include <TFolder.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TFile.h>
#include "Profiler.h"
#include "Hit.h"
#include <list>
#include <map>

//____________________________________________________________________
ClassImp(Example::Profiler);

//____________________________________________________________________
Example::Profiler::Profiler(Int_t nR, Int_t nL, const char* outfile)
  : Framework::Task("profiler", outfile), 
    fNL(nR), 
    fNR(nL),
    fTotalEnergyLoss(0),
    fN(0),
    fLongitudinalProfile(0),
    fCumulativeLongitudinalProfile(0),
    fRadialProfile(0),
    fCumulativeRadialProfile(0)
{}

//____________________________________________________________________
void 
Example::Profiler::Initialize(Option_t* option) 
{
  TDirectory* saveDir = gDirectory;
  gROOT->cd();
  fTotalEnergyLoss = 0;
  fN               = 0;
  fLongitudinalProfile           = new TH1F("longitudinalProfile", 
					    "Longitudinal profile", 
					    fNL, .5, fNL + .5);
  fCumulativeLongitudinalProfile = new TH1F("cumulativeLongitudinalProfile", 
					    "Longitudinal cumulative profile", 
					    fNL, .5, fNL + .5);
  fRadialProfile                 = new TH1F("radialProfile", 
					    "Radial profile", 
					    fNR, .5, fNR + .5);
  fCumulativeRadialProfile       = new TH1F("cumulativeRadialProfile", 
					    "Radial cumulative profile", 
					    fNR, .5, fNR + .5);
  fLongitudinalProfile          ->GetXaxis()->SetLabelFont(132);
  fLongitudinalProfile          ->GetYaxis()->SetLabelFont(132);
  fCumulativeLongitudinalProfile->GetXaxis()->SetLabelFont(132);
  fCumulativeLongitudinalProfile->GetYaxis()->SetLabelFont(132);
  fRadialProfile                ->GetXaxis()->SetLabelFont(132);
  fRadialProfile                ->GetYaxis()->SetLabelFont(132);
  fCumulativeRadialProfile      ->GetXaxis()->SetLabelFont(132);
  fCumulativeRadialProfile      ->GetYaxis()->SetLabelFont(132);
  fLongitudinalProfile          ->SetXTitle("Ring");
  fCumulativeLongitudinalProfile->SetXTitle("Ring");
  fRadialProfile                ->SetXTitle("Tube");
  fCumulativeRadialProfile      ->SetXTitle("Tube");
  fLongitudinalProfile          ->SetYTitle("Energy loss");
  fCumulativeLongitudinalProfile->SetYTitle("Energy loss");
  fRadialProfile                ->SetYTitle("Energy loss");
  fCumulativeRadialProfile      ->SetYTitle("Energy loss");
  
  saveDir->cd();
}

//____________________________________________________________________
void 
Example::Profiler::Exec(Option_t* option) 
{
  if ((option[0] != '\0' && option[0] != 'E')) return;
  TFolder* top = GetBaseFolder();
  if (!top) {
    Error("Exec", "couldn't find top level folder");
    return;
  }
  TFolder* particleFolder = static_cast<TFolder*>(top->FindObject("Gun"));
  if (!particleFolder) {
    Error("Exec" , "couldn't find particle folder");
    return;
  }
  particleFolder->ls();
  TFolder* hitFolder = static_cast<TFolder*>(top->FindObject("BlocSimulator"));
  if (!hitFolder) {
    Error("Exec" , "couldn't find hit folder");
    return;
  }
  TClonesArray* particles = 
    static_cast<TClonesArray*>(particleFolder->GetListOfFolders());
  if (!particles) {
    Error("Exec", "couldn't get array of particles");
    return;
  }  
  particles->ls();
  TClonesArray* hits = 
    static_cast<TClonesArray*>(hitFolder->GetListOfFolders());
  if (!hits) {
    Error("Exec", "couldn't get array of hits");
    return;
  }

  // Update the total energy loss 
  Int_t nParticles = particles->GetEntries();
  Verbose(1, "Exec", "got %d particles from this event", nParticles);
  for (Int_t i = 0; i < nParticles; i++) {
    TParticle* p = static_cast<TParticle*>(particles->At(i));
    Info("Exec", "Energy: %f", p->Energy());
    fTotalEnergyLoss += p->Energy();
  }

  // Find the summed energy loss in each ring and tube 
  Int_t   nHits = hits->GetEntries();
  Verbose(1, "Exec", "got %d hits from this event", nHits);
  for (Int_t i = 0; i < nHits; i++) {
    Example::Hit* hit = static_cast<Example::Hit*>(hits->At(i));
    if (!hit) {
      Error("Summary", "Didn't find hit # %d", i);
      continue;
    }

    Int_t   ring  = hit->fRing;
    Int_t   tube  = hit->fTube;
    Float_t eloss = hit->EnergyLoss();
    fLongitudinalProfile->Fill(ring-1,eloss);
    fRadialProfile->Fill(tube-1,eloss);
    for (Int_t j = ring; j < fNL; j++) 
      fCumulativeLongitudinalProfile->Fill(j, eloss);
    for (Int_t j = tube; j < fNR; j++) 
      fCumulativeRadialProfile->Fill(j, eloss);
  }
  fN++;
}

//____________________________________________________________________
void 
Example::Profiler::Finish(Option_t* option) 
{
  if (fTotalEnergyLoss <= 0) {
    Error("finish", "Total energy loss is %f <= 0", fTotalEnergyLoss);
    return;
  }
  
  Float_t norm = 100. / fTotalEnergyLoss;

  fLongitudinalProfile          ->Scale(norm);
  fCumulativeLongitudinalProfile->Scale(norm);
  fRadialProfile                ->Scale(norm);
  fCumulativeRadialProfile      ->Scale(norm);
  if (!gROOT->IsBatch()) {
  
    // Set some style options 
    gStyle->SetOptStat(1001100);
    gStyle->SetStatBorderSize(1);
    gStyle->SetStatX(.95);
    gStyle->SetStatY(.95);
    gStyle->SetStatFont(132);
    gStyle->SetStatColor(0);
    gStyle->SetTitleColor(0);
    gStyle->SetTitleBorderSize(0);
    gStyle->SetTitleFillColor(0);
    gStyle->SetTitleY(1);
    gStyle->SetTitleX(.1);
    gStyle->SetTitleFont(132);
    gStyle->SetTitleAlign(13);
    gStyle->SetPadTopMargin(0.05);
    gStyle->SetPadLeftMargin(0.10);
    gStyle->SetPadRightMargin(0.05);
    gStyle->SetPadBottomMargin(0.10);
    gStyle->SetLabelFont(132, "xyz");
    gStyle->SetTitleFont(132, "xyz");
    gStyle->SetTextFont(132);
    gStyle->SetLabelFont(132, "xyz");
  
    // Make a canvas and some pads 
    TCanvas* c = new TCanvas("c", "c", 800, 800);
    TPad* p1 = new TPad("p1", "p1", 0.0, 0.5, 0.5, 1.0, 0, 0, 0);
    TPad* p2 = new TPad("p2", "p2", 0.0, 0.0, 0.5, 0.5, 0, 0, 0);
    TPad* p3 = new TPad("p3", "p3", 0.5, 0.5, 1.0, 1.0, 0, 0, 0);
    TPad* p4 = new TPad("p4", "p4", 0.5, 0.0, 1.0, 0.5, 0, 0, 0);

    std::map<TPad*,TH1*> m = {{p1, fLongitudinalProfile},
			      {p2, fRadialProfile},
			      {p3, fCumulativeLongitudinalProfile},
			      {p4, fCumulativeRadialProfile}};
    for (auto kv : m) {
      c->cd();
      kv.first->Draw();
      kv.first->cd();
      kv.second->DrawCopy("E");
    }

    // Update canvas 
    c->Modified();  c->cd();
  }
  if (fTitle.IsNull()) return;
  TFile* out = TFile::Open(GetTitle(), "RECREATE");

  std::list<TH1*> hs = {fLongitudinalProfile,
			fRadialProfile,
			fCumulativeLongitudinalProfile,
			fCumulativeRadialProfile};
  for (auto h : hs) h->Write();
  out->Close();
    
  
  
			 
}

//____________________________________________________________________ 
//  
// EOF
//
