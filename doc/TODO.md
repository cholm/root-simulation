## Tasks 

The idea is, that for each detector part, one defines a class that
derives from Simulation::Task. 

Simulation::Task is a specialised Framework::Task.  It extends the
Framework::Task interface via overloading Framework::Task::Exec (see
Simulation::Task::Exec).  When the Simulation::Application executes
one of the simulation steps, it calls TTask::ExecuteTask with an
option that describes what action should be taken.  The option string
is one character long, so there's little overhead due to going via
Simulation::Task::Exec.  Simulation::Task::Exec has a `switch`
statemet that then forwards to the appropiate member function.

### Tasks that corresponds to sub detectors and volumes

The Task derived class that corresponds to sub detectors or other
kinds of volumes in the detector, should define one or more of the
following member functions as needed. 

One can define the geometry directly in the Simulation::Task derived
classes.  In that case, the mediums should be made in
Simulation::Task::Initialize, and the actual geometry in
Simulation::Task::Register.

However, a better option, is to make sub-classes of Framework::Task
that simply constructs the geometry.  There's then a clear distinction
between geometry and particle transport.  This has the advantage that
it's easier to test and re-use the geometry code without depending on
the simulation code.  In this case, the Framework::Task derived class
should define mediums in Simulation::Task::Initialize, 

	```{cxx}
    void
    Example::BlocBuilder::Initialize(Option_t* option) 
    {
      Framework::Task::Initialize(option);
    
      TGeoMixture* bgo = new TGeoMixture("BGO", 3, 7.1);
      bgo->DefineElement(0, 208.98,  83, .1818);
      bgo->DefineElement(1,  72.59,  32, .3636);
      bgo->DefineElement(2,  15.999,  8, .4546);
      
      TGeoMixture* leadGlass = new TGeoMixture("Lead Glass", 6, 5.2);
      leadGlass->DefineElement(0, 207.19,  82, .65994);
      leadGlass->DefineElement(1,  39.102, 19, .00799);
      leadGlass->DefineElement(2,  28.088, 14, .126676);
      leadGlass->DefineElement(3,  22.99,  11, .0040073);
      leadGlass->DefineElement(4,  15.999,  8, .199281);
      leadGlass->DefineElement(5,  74.922, 33, .00200485);
        
      TGeoMaterial* mat    = (fUseBgo ? bgo : leadGlass);
      Double_t      par[]  = { 0, 0, 0, 10., 1000., .05, .001, .001, 0, 0 };
      TGeoMedium*   medium = new TGeoMedium("Absorber", 1, mat,  par);
    }
	```	
	
	
and the actual geometry in  Simulation::Task::Register.

	```{cxx}
    void
    Example::BlocBuilder::Register(Option_t* option) 
    {
      TTree* tree = 0;
      if ((tree = GetBaseTree("Hits"))) 
        fBranch = tree->Branch(GetName(), &fCache);
    
      TGeoMedium*   medium   = gGeoManager->GetMedium("Absorber");
      TGeoMaterial* material = medium->GetMaterial();
      Double_t radl          = material->GetRadLen();
      
      TGeoTube*   blocShape  = new TGeoTube("BLOC", 0, 20 * radl / 4, 10 * radl);
      TGeoVolume* blocVolume = new TGeoVolume("BLOC", blocShape, medium);
      blocVolume->SetLineColor(kCyan);
      blocVolume->SetFillColor(kCyan);
      
      TGeoVolume* rtubVolume = blocVolume->Divide("RTUB", 1, fNR, 0, 0);
      rtubVolume->SetLineColor(kMagenta);
      rtubVolume->SetFillColor(kMagenta);
      
      TGeoVolume* ringVolume = rtubVolume->Divide("RING", 3, fNL, 0, 0);
      ringVolume->SetLineColor(kYellow);
      ringVolume->SetFillColor(kYellow);
      
      TGeoVolume* top        = gGeoManager->GetTopVolume();
      top->AddNode(blocVolume, 0, 0);
    }
	```
The simulation task is then in another class

- Simulation::Task::Initialize :
  In this member function, the sub class should register all
  needed mediums, as well as create any folders it should need. 
  
	```{cxx}
    void
    Example::BlocSimulator::Initialize(Option_t* option) 
    {
      Debug(14, "Initialize", "Setting up materials");
      Simulation::Task::Initialize(option);
    
      Debug(14, "Initialize", "Using TGeo classes for the geometry");
      TGeoMedium*   medium   = gGeoManager->GetMedium("Absorber");
      RegisterMedium(medium);
    }
	```

- Simulation::Task::Register :
  In this member function, the sub class register all needed
  volume should and register a branch for output in a tree. 

  If the derived class calls
  Simulation::Task::CreateDefaultHitArray() the constructor, then one
  can do 
  
	```{cxx}
    TTree* tree = 0;
    if ((tree = GetBaseTree("Hits"))) 
      fBranch = tree->Branch(GetName(), &fCache);
	``` 

  to register the output array (a TClonesArray of Simulation::Hit
  objects) in the output tree.
      

	```{cxx}
    void
    Example::BlocSimulator::Register(Option_t* option) 
    {
      TTree* tree = 0;
      if ((tree = GetBaseTree("Hits"))) 
        fBranch = tree->Branch(GetName(), &fCache);
    
    
      TGeoVolume* ringVolume = gGeoManager->GetVolume("RING");
      RegisterVolume(ringVolume);
    }
	```
	  
- Simulation::Task::Step :
  This member function is called when ever a hit is created
  by the simulation backend (TVirtualMC) in a medium registered
  with the task.  In this member function, the sub class should
  create a hit on the output branch if needed. 

  If the derived class calls Simulation::Task::CreateDefaultHitArray()
  the constructor, then the member function can call
  Simulation::Task::CreateDefaultHit() in this member function to
  create a hit in the output array (a TClonesArray of Simulation::Hit
  objects).  Otherwise we need to create the hit ourselves, for
  example

	```{cxx}
    void
    Example::BlocSimulator::Step() 
    {
      TVirtualMC* mc = TVirtualMC::GetMC();
      Int_t ring;
      Int_t vol = mc->CurrentVolID(ring);
      if (!IsSensitive(vol)) return;
      if (!mc->IsTrackAlive()) return;
      // if (TMath::Abs(mc->TrackCharge()) <= 0) return;
    
      Int_t tube;
      Int_t volOffId = mc->CurrentVolOffID(1, tube);

	    if (mc->Edep() <= 0) return;
      
      TClonesArray* hits = static_cast<TClonesArray*>(fCache);
      Int_t n = hits->GetEntriesFast();
      
      TLorentzVector v, p;
      mc->TrackPosition(v);
      mc->TrackMomentum(p);
  
		Int_t   nTrack = mc->GetStack()->GetCurrentTrackNumber();
      Int_t   pdg    = mc->TrackPid();
      Float_t edep   = mc->Edep() * 1000;
      new ((*hits)[n]) Hit(tube, ring, edep, nTrack, pdg, v, p);
      mc->GetStack()->GetCurrentTrack()->SetBit(Simulation::kKeepBit);
    }
	```
	  
- Simulation::Task::Field :
  
  This member function is called when ever the magnetic field needs to
  be known at a location $`\mathbf{x} = (x,y,x)`$ by the simulation
  backend (TVirtualMC) in a medium registered with the task.  In this
  member function, the sub class should calculate the magnetic field
  and return it the second argument. 
  
**Note**: It is important that for each medium that the detector want
to process hits in, that the sub class associates the medium to the
task by calling Simulation::Task::RegisterMedium.  This is a
one-to-one association, so the medium _must_ be unique to the
sub-class.  However, one can define a sub-class that deals with all
bulk volumes (i.e., volumes that do not correspond to active detector
volumes).  In this way, one can easily deal with hits that are not
really of intrest to the user.

### Tasks that corresponds to Event Generators

To create an event generator task, one should overload the member
function Simulation::Task::GeneratePrimaries.   The class
Simulation::Generator uses a TGenerator object to create
some primary particles, and push them onto the simulation backends
stack (Simulation::Stack which derives from TVirtualMCStack).  

There is a predefined TGenerator derived class, called
Simulation::Single that shoots single tracks of a fixed type, and with
a fixed momentum, as well as the class Simulation::Fixed for shooting
uniformly distributed particles of a fixed type.

The user can override the member function
Simulation::Generator::MakeVertex to make the primary particles come
from a different position than defined by the TGenerator derived
class.

If then generated particles should be output, then the generator sub
class should overload the member functions Simulation::Task::Register
to make an branch on the output branch.

### Auxiliary Tasks

There are a few pre-defined tasks that the users can use freely in
their code.   Please also refer to the class descriptions. 

- Simulation::Display :
  Event display.  See also [Visualisation](#Visualisation)

- Simulation::TreeReader : Reads a tree from a TFile.  This is a
  specialisation of Framework::TreeReader.
	
- Simulation::TreeWriter : Writes a tree to a TFile.  This is a
    specialisation of Framework::TreeReader.
	
- Simulation::TrackSaver : This task saves kept tracks from the
  simulation in a TBranch on the output TTree

- Simulation::Single : Technically not a Task, but listed here for
  completness.  This is a TGenerator (event generator) that shoots
  single particles of a fixed type ,with a fixed momentum.  One can
  set the momentum vector in various ways.  Good for testing the
  geometry.
	
- Simulation::Hit : Technically not a Task, but listed here for
  completness.  This is a data object, which can be used by the user
  code to store hits produced in the simulation.  It contains a
  position 4-vector, a momentum 4-vector, as well as a particle ID,
  and the energy lost in that step.  Note, that the user is in no way
  obligated to use this data structure. It's provided as a convenince
  for the user.  The member function
  Simulation::Task::CreateDefaultHitArray makes a task use this
  structure for it's hits, and Simulation::Task::CreateDefaultHit
  creates a hit of this type (should be called in the Step member
  function of the derived class).

### Other task related choirs 

There's a number of other member functions thatcan be overloaded in
the sub classes of Simulation::Task.  Below the member functions are
list, and a brief description is given.

- Simlation::Task::DefineParticles : If the simulation requires
  additional particles types, the sub class can overload this member
  function to define these member functions.  This is probably most
  useful in the context of an Simulation::Generator task.  The member
  function should call TVirtualMC::DefineParticle as many times as
  needed.
- Simulation::Task::BeginEvent : This message is sent once to each
  defined task at the beginning of an event.  This member function
  could be used to reset output arrays, or the like.
- Simulation::Task::BeginPrimary : This message is sent once to each
  defined task at the beginning transport of a primary particle.  Sub
  classes can override this member function to setup various stuff, or
  cuts based on the new primary particle.
- Simulation::Task::BeginTrack - This message is sent once to each
  defined task at the beginning transport of a track.  Sub classes can
  override this member function to setup various stuff, or cuts based
  on the new track.
- Simulation::Task::FinishTrack : This message is sent once to each
  defined task at the end of transport of a track.  Sub classes can
  override this member function to clear internal caches or the like.
- Simulation::Task::FinishPrimary : This message is sent once to each
  defined task at the end of transport of a primary particle.  Sub
  classes can override this member function to clear internal caches
  or the like.
- Simulation::Task::FinishEvent : This message is sent once to each
  defined task at the end of an event.  Sub classes can override this
  member function to do summaries, event display (Display) or the
  like.
- Simulation::Task::Finish : This message is sent once to each defined
  task at the end of an run.  Sub classes can override this member
  function to do output to file, and the like.

There's also some utility member functions defined in
Simulation::Task: 

- Simulation::Task::SetActive : Make the task active.  If the task is
  inactive, the `Step` member function is never called on that
  task. See also Browser for how to control this from the browser.
- Simulation::Task::IsActive : Test whether the task is active or not.
- Simulation::Task::RegisterVolume : Derived code can use this to
  register volumes as sensitive volumes. This only used internally,
  and is entirely optional. This member function should be called once
  for each senstive volume in the Simulation::Task::Register member
  function, or never at all.
- Simulation::Task::IsSensitive `(id)` : Check if the volume with `id`
  has been regisitered as a senstive volume by a call to
  Simulation::Task::RegisterVolume.  In the stepping function, one can
  use this to filter out steps in non-sensitive volumes in a user
  defined way. Note, that the use of this member function is entirely
  optional.
  
## Interface to simulation backend 


ROOT based simulations uses the abstract interface
[VMC](http://root.cern.ch/root/vmc/VirtualMC.html) classes to
interface with various simulation backends like [GEANT
3.21](http:cern.ch/wwwasd/geant) and [GEANT
4](http://cern.ch/geant4/).

This VMC interface dictates that the user should define a sub class of
TVirtualMCApplication to handle the various stages of the simulation
via call-backs.  In this framework, the class Simulation::Application
provides this interface.  On the other hand, it passes the calls on to
the user defined sub classes of Simulation::Task.

The VMC interface also dictates that the user defines a sub-class of
TVirtualMCStack that should provide a stack for the simulation
backend.  That is, the sub class should be apply push tracks on the
stack, and pop from the top of the stack in a FIFO
(First-In-First-Out) manner.  The class Simulation::Stack provides
such a sub-class.  The user code can pudh primary particles onto that
stack if needed.  If needed, a sub class of Simulation::Stack can be
defined to store referenced tracks in the output of the simulation.
The predefined task, Simulation::TrackSaver does just that.

The Simulation::Stack and Simulation::Application classes hooks into
the TGeo architecture of ROOT to provide visualisation, and mapping
from tracking mediums to Simulation::Task objects.

### Set-up of backend 

The scripts `G3Config.C` and `G4Config.C` sets up a GEANT 3.21 and
Geant 4, respectively, back-end.  These are installed in the data
directory of the package, and the main class Simulation::Main will
look for them there. 

If you need to configure the back-end, for example to use a different
physics list for Geant 4, then you should copy the relevant set-up
script to your working directory, or somewhere early in the ROOT
script path (`gROOT->GetMacroPath()`), and modify the script there. 

To make your configuration independent of the back-end, you may do
something like 

	```{cxx}
    void Config(Bool_t g3=true) {
	   ...
	   if (g3) gROOT->Macro("G3Config.C");
	   else    gROOT->Macro("G4Config.C");
	   ...
   }
   ```


### Visualisation 


The [VMC](http://root.cern.ch/root/vmc/VirtualMC.html) architecture
does not provide any visualisation on its own (though TGeant3 does).
However, the TGeo library of ROOT does provide extensive visualisation
options for a geometry described in this framework.

To glue these two, VMC and TGeo, together the Simulation::Stack reates
TVirtualGeoTrack objects each time a track is pushed on the stack, and
sets the current TVirtualGeoTrack each time a track is popped of the
stack.

If a primary particle is pushed on the stack, then the
TVirtualGeoTrack is registered with the TGeoManager singleton.

If a secondary particle is pushed on the stack, the
TVirtualGeoTrack is registered with it's mother TVirtualGeoTrack.

In Simulation::Application::Stepping, a point is added to the current
track when ever a hit is created by the simulation backend.

Currently it's not possible to turn off this feature of the
framework, but I hope to provide a simple switch for that later.

There is a simple predefined task called Simulation::Display that show
the current event after each event is complete.

This creates a window like the one shown below.   The buttons at
the bottom are 

- **Continue** : Continue to next event
- **+ ** :  Zoom in 
- **- ** :  Zoom out 
- **< ** :  Move display left 
- **> ** :  Move display right 
- **^ ** :  Move display up 
- **v ** :  Move display down 
- Draw **Hits** :  Draw the hits as created by the Simulation::Task sub
  classes.  
- Draw **Tracks** : Draw the traks (TVirtualGeoTrack) as created by
  the Simulation::Stack object
- Draw **Hits & Tracks** : First, animate the tracks, then draw the
  hits and tracks as above.
- **Animate** Tracks : This shows a short animation of the event as it
  traverses the example.
- **Pan** :  This shows a short animation of the event as it traverses
  the detector. It pans the camera at the same time 
- **Record  ** :  Records the above visualisation as GIFs, one per frame. 

![](display.png)
