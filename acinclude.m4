dnl
dnl $Id: acinclude.m4,v 1.6 2006-03-22 13:54:39 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
AC_DEFUN([AC_DEBUG],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to make debug objects)
  AC_ARG_ENABLE(debug,
    [AC_HELP_STRING([--enable-debug],[Enable debugging symbols in objects])])
  if test "x$enable_debug" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-g,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-g,,'`
  else
    case $CXXFLAGS in
    *-g*) ;;
    *)    CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case $CFLAGS in
    *-g*) ;;
    *)    CFLAGS="$CFLAGS -g" ;;
    esac
  fi
  AC_MSG_RESULT($enable_debug 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------
AC_DEFUN([AC_OPTIMIZATION],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])

  AC_ARG_ENABLE(optimization,
    [AC_HELP_STRING([--enable-optimization],[Enable optimization of objects])])

  AC_MSG_CHECKING(for optimiztion level)

  changequote(<<, >>)dnl
  if test "x$enable_optimization" = "xno" ; then
    CFLAGS=`echo   $CFLAGS   | sed 's,-O\([0-9][0-9]*\|\),,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-O\([0-9][0-9]*\|\),,'`
  elif test "x$enable_optimization" = "xyes" || 
	test "x$enable_optimization" = "x" ; then
    case $CXXFLAGS in
    *-O*) ;;
    *)    CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case $CFLAGS in
    *-O*) ;;
    *)    CFLAGS="$CXXFLAGS -O2" ;;
    esac
  else
    CFLAGS=`echo   $CFLAGS   | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
    CXXFLAGS=`echo $CXXFLAGS | sed "s,-O\([0-9][0-9]*\|\),-O$enable_optimization,"`
  fi
  changequote([, ])dnl
  AC_MSG_RESULT($enable_optimization 'CFLAGS=$CFLAGS')
])
dnl ------------------------------------------------------------------
AC_DEFUN([AC_PROFILE],
[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([AC_PROG_CXX])
  AC_MSG_CHECKING(whether to enable profiling objects)
  AC_ARG_ENABLE(profile,
    [AC_HELP_STRING([--enable-profile],[Enable profiling])])
  if test "x$enable_profile" = "xno" ; then
    CFLAGS=`echo $CFLAGS | sed 's,-pg,,'`
    CXXFLAGS=`echo $CXXFLAGS | sed 's,-pg,,'`
  else
    case $CXXFLAGS in
    *-pg*) ;;
    *)    CXXFLAGS="$CXXFLAGS -pg" ;;
    esac
    case $CFLAGS in
    *-pg*) ;;
    *)    CFLAGS="$CFLAGS -pg" ;;
    esac
  fi
  AC_MSG_RESULT($enable_profile 'CFLAGS=$CFLAGS')
])

dnl ------------------------------------------------------------------

dnl
dnl $Id: acinclude.m4,v 1.6 2006-03-22 13:54:39 cholm Exp $
dnl $Author: cholm $
dnl $Date: 2006-03-22 13:54:39 $
dnl
dnl Autoconf macro to check for existence or ROOT on the system
dnl Synopsis:
dnl
dnl  ROOT_PATH([MINIMUM-VERSION, [ACTION-IF-FOUND, [ACTION-IF-NOT-FOUND]]])
dnl
dnl Some examples: 
dnl 
dnl    ROOT_PATH(3.03/05, , AC_MSG_ERROR(Your ROOT version is too old))
dnl    ROOT_PATH(, AC_DEFINE([HAVE_ROOT]))
dnl 
dnl The macro defines the following substitution variables
dnl
dnl    ROOTCONF           full path to root-config
dnl    ROOTEXEC           full path to root
dnl    ROOTCINT           full path to rootcint
dnl    ROOTLIBDIR         Where the ROOT libraries are 
dnl    ROOTINCDIR         Where the ROOT headers are 
dnl    ROOTCFLAGS         Extra compiler flags
dnl    ROOTLIBS           ROOT basic libraries 
dnl    ROOTGLIBS          ROOT basic + GUI libraries
dnl    ROOTAUXLIBS        Auxilary libraries and linker flags for ROOT
dnl    ROOTAUXCFLAGS      Auxilary compiler flags 
dnl    ROOTRPATH          Same as ROOTLIBDIR
dnl
dnl The macro will fail if root-config and rootcint isn't found.
dnl
dnl Christian Holm Christensen <cholm@nbi.dk>
dnl
AC_DEFUN([ROOT_PATH],
[
  AC_ARG_WITH(rootsys,
  [  --with-rootsys          top of the ROOT installation directory],
    user_rootsys=$withval,
    user_rootsys="none")
  if test ! x"$user_rootsys" = xnone; then
    rootbin="$user_rootsys/bin"
  elif test ! x"$ROOTSYS" = x ; then 
    rootbin="$ROOTSYS/bin"
  else 
   rootbin=$PATH
  fi
  AC_PATH_PROG(ROOTCONF,  root-config , no, $rootbin)
  AC_PATH_PROG(ROOTEXEC,  root ,        no, $rootbin)
  AC_PATH_PROG(ROOTCINT,  rootcint ,    no, $rootbin)
  AC_PATH_PROG(ROOTCLING, rootcling ,   no, $rootbin)
	
  if test ! x"$ROOTCONF" = "xno" ; then 

    # define some variables 
    ROOTLIBDIR=`$ROOTCONF --libdir`
    ROOTINCDIR=`$ROOTCONF --incdir`
    ROOTCFLAGS=`$ROOTCONF --noauxcflags --cflags` 
    ROOTLIBS=`$ROOTCONF --noauxlibs --noldflags --libs`
    ROOTGLIBS=`$ROOTCONF --noauxlibs --noldflags --glibs`
    ROOTAUXCFLAGS=`$ROOTCONF --auxcflags`
    ROOTAUXLIBS=`$ROOTCONF --auxlibs`
    ROOTRPATH=$ROOTLIBDIR
    ROOTPREFIX=`$ROOTCONF --prefix`
    
    vers=`$ROOTCONF --version | tr './' ' ' | \
  awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`

   if test $1 ; then 
      AC_MSG_CHECKING(wether ROOT version >= [$1])

      requ=`echo $1 | tr './' ' ' | \
  awk 'BEGIN { FS = " "; } { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
      if test $vers -lt $requ ; then 
        AC_MSG_RESULT(no)
	no_root="yes"
      else 
        AC_MSG_RESULT(yes)
      fi
    fi
  else
    # otherwise, we say no_root
    no_root="yes"
  fi
  AM_CONDITIONAL(ROOT6, test $vers -gt 6000000)
     

  AC_SUBST(ROOTLIBDIR)
  AC_SUBST(ROOTINCDIR)
  AC_SUBST(ROOTCFLAGS)
  AC_SUBST(ROOTLIBS)
  AC_SUBST(ROOTGLIBS) 
  AC_SUBST(ROOTAUXLIBS)
  AC_SUBST(ROOTAUXCFLAGS)
  AC_SUBST(ROOTRPATH)
  AC_SUBST(ROOTPREFIX)

  if test "x$no_root" = "x" ; then 
    ifelse([$2], , :, [$2])     
  else 
    ifelse([$3], , :, [$3])     
  fi
])

dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: acinclude.m4,v 1.6 2006-03-22 13:54:39 cholm Exp $ 
dnl  
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_ROOT_FRAMEWORK([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ROOT_FRAMEWORK],
[
    AC_ARG_WITH([framework-prefix],
        [AC_HELP_STRING([--with-framework-prefix],
		[Prefix where Framework is installed])],
        framework_prefix=$withval, framework_prefix="")

    if test "x${FRAMEWORK_CONFIG+set}" != xset ; then 
        if test "x$framework_prefix" != "x" ; then 
	    FRAMEWORK_CONFIG=$framework_prefix/bin/framework-config
	fi
    fi   
    AC_ARG_WITH([framework-url],
        [AC_HELP_STRING([--with-framework-url],
		[Base URL where the Framework dodumentation is installed])],
        framework_url=$withval, framework_url="")

    if test "x${FRAMEWORK_CONFIG+set}" != xset ; then 
        if test "x$framework_prefix" != "x" ; then 
	    FRAMEWORK_CONFIG=$framework_prefix/bin/framework-config
	fi
    fi   
    AC_PATH_PROG(FRAMEWORK_CONFIG, framework-config, no)
    framework_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Framework version >= $framework_min_version)

    framework_found=no    
    if test "x$FRAMEWORK_CONFIG" != "xno" ; then 
       FRAMEWORK_CPPFLAGS=`$FRAMEWORK_CONFIG --cppflags`
       FRAMEWORK_INCLUDEDIR=`$FRAMEWORK_CONFIG --includedir`
       FRAMEWORK_LIBS=`$FRAMEWORK_CONFIG --libs`
       FRAMEWORK_LIBDIR=`$FRAMEWORK_CONFIG --libdir`
       FRAMEWORK_LDFLAGS=`$FRAMEWORK_CONFIG --ldflags`
       FRAMEWORK_PREFIX=`$FRAMEWORK_CONFIG --prefix`
       
       framework_version=`$FRAMEWORK_CONFIG -V` 
       framework_vers=`echo $framework_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       framework_regu=`echo $framework_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $framework_vers -ge $framework_regu ; then 
            framework_found=yes
       fi
    fi
    AC_MSG_RESULT($framework_found - is $framework_version) 
    AC_MSG_CHECKING(where the Framework documentation is installed)
    if test "x$framework_url" = "x" && \
	test ! "x$FRAMEWORK_PREFIX" = "x" ; then 
       FRAMEWORK_URL=${FRAMEWORK_PREFIX}/share/doc/framework/html
    else 
	FRAMEWORK_URL=$framework_url
    fi	
    AC_MSG_RESULT($FRAMEWORK_URL)
   
    if test "x$framework_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(FRAMEWORK_URL)
    AC_SUBST(FRAMEWORK_PREFIX)
    AC_SUBST(FRAMEWORK_CPPFLAGS)
    AC_SUBST(FRAMEWORK_INCLUDEDIR)
    AC_SUBST(FRAMEWORK_LDFLAGS)
    AC_SUBST(FRAMEWORK_LIBDIR)
    AC_SUBST(FRAMEWORK_LIBS)
])
dnl
dnl EOF
dnl 

AC_DEFUN([AC_ROOT_VMC],
[
  AC_ARG_WITH([vmc-prefix],
      [AC_HELP_STRING([--with-vmc-prefix],
                      [Prefix where VMC library is installed])],
        vmc_prefix=$withval, vmc_prefix=)

  if test "x${ROOTCONF+set}" = "xset" ; then
    AC_MSG_CHECKING(whether ROOT has VMC)
    has_root_vmc=`$ROOTCONF --has-vmc 2>/dev/null | tr -d '\n'`
    AC_MSG_RESULT($has_root_vmc)
    
    if test "x$has_root_vmc" = xyes ; then
      VMC_LIBS=-lVMC
      VMC_INCLUDEDIR=$ROOTINCDIR
      tmp_LIBDIRS=$ROOTLIBDIR
    else
      if test x$vmc_prefix = x || test x$vmc_prefix = xyes ; then
         vmc_prefix=`$ROOTCONF --prefix`
      fi
      
      AC_MSG_CHECKING(for standalone VMC library)
      AC_MSG_RESULT($vmc_prefix)
      VMC_INCLUDEDIR="${vmc_prefix}/include/vmc"
      tmp_LIBDIRS="${vmc_prefix}/lib ${vmc_prefix}/lib64"
      VMC_LIBS=-lVMCLibrary
    fi

    for tmpdir in $tmp_LIBDIRS ; do 
      save_CPPFLAGS=$CPPFLAGS
      save_LDFLAGS=$LDFLAGS
      save_LIBS=$LIBS
      save_CXXFLAGS=$CXXFLAGS
      CXXFLAGS="${ROOTAUXCFLAGS} ${CXXFLAGS}"
      CPPFLAGS="${ROOTCFLAGS} -I${VMC_INCLUDEDIR} ${CPPFLAGS}"
      LDFLAGS="-L${ROOTLIBDIR} -L${tmpdir} ${LDFLAGS}"
      LIBS="${VMC_LIBS} ${ROOTLIBS} ${LIBS}"
      
      
      AC_LANG_PUSH(C++)
      AC_CHECK_HEADERS([TVirtualMC.h],[vmc_header=yes],
                       [vmc_header=no])
      if test "x$vmc_header" = xno ; then
        VMC_INCLUDEDIR=
	continue 
      fi
      
      AC_MSG_CHECKING(for $VMC_LIBS)
      AC_LINK_IFELSE([AC_LANG_PROGRAM([#include <TMCManager.h>],
                                      [TMCManager::Instance()])],
      		     [vmc_lib=yes],[vmc_lib=no])
      AC_MSG_RESULT($vmc_lib)
      if test x$vmc_lib = xyes ; then
        VMC_LIBDIR=$tmpdir
	break
      fi
      
      AC_LANG_POP([C++])
      
      CXXFLAGS="$save_CXXFLAGS"
      CPPFLAGS="$save_CPPFLAGS"
      LDFLAGS="$save_LDFLAGS"
      LIBS="$save_LIBS"
    done
    
    if test "x$vmc_lib" = "xno" ; then
      VMC_LIBDIR=
      VMC_LIBS=
    fi
  fi 

  if test "x${VMC_LIBS+set}" = "xset" ; then
      ifelse([$1], , :, [$1])
  else 
      ifelse([$2], , :, [$2])
  fi
  if test "x${VMC_INCLUDEDIR+set}" = xset ; then
      VMC_CPPFLAGS=-I${VMC_INCLUDEDIR}
  fi
  echo "VMC_LIBDIR='${VMC_LIBDIR}'"
  if test "x${VMC_LIBDIR+set}" = xset && test x$VMC_LIBDIR != x; then
      VMC_LDFLAGS=-L${VMC_LIBDIR}
  fi
    
  AC_SUBST(VMC_INCLUDEDIR)
  AC_SUBST(VMC_CPPFLAGS)
  AC_SUBST(VMC_LIBDIR)
  AC_SUBST(VMC_LDFLAGS)
  AC_SUBST(VMC_LIBS)
])
     
#
# EOF
#
