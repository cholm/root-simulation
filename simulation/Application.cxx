//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Application.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Simulation::Application
*/
#include <simulation/Application.h>
#include <simulation/Stack.h>
#include <simulation/Main.h>
#include <simulation/Task.h>
#include <TGeoManager.h>
#include <TGeoMedium.h>
#include <TVirtualGeoTrack.h>
#include <TVirtualMC.h>
#include <TBrowser.h>
#include <TCanvas.h>
#include <TView.h>
#include <TString.h>
#include <TROOT.h>
#include <TStyle.h>
#include <iostream>
#include <iomanip>

//____________________________________________________________________
ClassImp(Simulation::Application);

//____________________________________________________________________
Simulation::Application::Application(const Char_t* name,
				     const Char_t* title)
  : TVirtualMCApplication(name, title),
    fGeoFileName("")
{
  fStack = new Stack(1000);
}


//____________________________________________________________________
Bool_t
Simulation::Application::RegisterMedium(Int_t medium, Simulation::Task* task) 
{
  Fatal("RegisterMedium", "This member function (with Int_t,Task* "
	"arguments) should not be used. Use the one with "
	"TGeoMedium*,Task* arguments.");
  return false;
  // if (!task || medium < 0) return kFALSE;
  // std::pair<MediumMap::iterator, bool> i = 
  //   fMediumMap.insert(std::pair<Int_t,Simulation::Task*>(medium, task));
  // if (!i.second) { 
  //   task->Fail("RegisterMedium", "Medium with ID %d already exists", medium);
  //   return kFALSE;
  // }
  // task->Debug(5,"RegisterMedium", "%d -> %s", medium, task->GetName());
  // return kTRUE;
}

//____________________________________________________________________
Bool_t
Simulation::Application::RegisterMedium(TGeoMedium* medium, 
					Simulation::Task* task) 
{
  if (!task || !medium) return -1;
  fGeoMediumMap.insert(std::pair{medium,task});
  task->Debug(5,"RegisterMedium", "%s -> %s", medium->GetName(), 
	      task->GetName());
  return kTRUE;
}

//____________________________________________________________________
Simulation::Application::MediumTaskRange
Simulation::Application::Medium2Tasks(Int_t mediumId) const
{
  return fMediumTaskMap.equal_range(mediumId);
}

//____________________________________________________________________
Int_t
Simulation::Application::MCVol2Geo(Int_t volId) const
{
  auto it = fMCGeoMap.find(volId);
  if (it == fMCGeoMap.end()) return -1;

  return it->second;
}

//____________________________________________________________________
void
Simulation::Application::MapMediums()
{
  TVirtualMC* mc = TVirtualMC::GetMC();

  // fMediumTaskMap.clear();
  for (auto mt : fGeoMediumMap) {
    TGeoMedium* med  = mt.first;
    Task*       task = mt.second;
    Int_t       mId  = mc->MediumId(med->GetName());
    if (mId < 0) {
      Warning("MapMediums", "Medium \"%s\" not known to VMC",med->GetName());
      continue;
    }
    fMediumTaskMap.insert(std::pair{mId,task});
  }
}

//____________________________________________________________________
void
Simulation::Application::MapVolumes(TObjArray* l)
{
  if (!l) return;

  TVirtualMC* mc = TVirtualMC::GetMC();
  
  for (auto pobj : *l) {
    TGeoVolume* vol   = dynamic_cast<TGeoVolume*>(pobj);
    if (!vol) continue;
    
    Int_t       volId = mc->VolId(vol->GetName());
    Int_t       geoId = MCVol2Geo(volId);
    
    if (geoId >= 0) {
      Simulation::Main::Instance()
	->Debug(10,
		"MapVolumes", "MC volume %d already mapped to %d (tried %d)",
		volId, geoId, vol->GetNumber());
      continue;
    }
    fMCGeoMap[volId] = vol->GetNumber();
  }
} 
	
//____________________________________________________________________
void
Simulation::Application::SetColors(TObjArray* l)
{
  if (!l) return;

  Int_t defColor = gStyle ? gStyle->GetFillColor() : 1;
  Int_t defStyle = gStyle ? gStyle->GetFillStyle() : 0;
  
  for (auto o : *l) {
    TGeoVolume*   vol = static_cast<TGeoVolume*>(o);
    if (!vol || (!vol->IsVolumeMulti() && vol->IsAssembly())) continue;
    
    TGeoMaterial* mat = vol->GetMaterial();
    TGeoMedium*   med = vol->GetMedium();
    Int_t         mco = med->GetId() % 6 + 2;
    
    if (vol->GetFillColor() == defColor || vol->GetLineColor() == mco) {
      vol->SetFillColor(mat->GetFillColor());
      vol->SetLineColor(mat->GetFillColor());
    }
    if (vol->GetFillStyle() == defStyle) {
      vol->SetFillStyle(mat->GetFillStyle());
    }
    if (vol->GetTransparency() == 0)
      vol->SetTransparency(mat->GetTransparency());
  }
}

//____________________________________________________________________
void
Simulation::Application::Init()
{
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(15, "Init", "From Application");

  TVirtualMC* mc = TVirtualMC::GetMC();
  if (!mc) {
    Fatal("Init", "No TVirtualMC instantized");
    return;
  }
  main->Debug(15, "Init", "From Application -> Configure VMC");
  main->MoreConfig(mc);
  main->Debug(15, "Init", "From Application -> Init VMC");
  mc->Init();//G3 calls construct geometry, G4 does it on CTOR

  main->Debug(15, "Init", "From Application -> Build physics");
  mc->BuildPhysics();
  main->Debug(15, "Init", "From Application -> show medium map");

  // Set visualisation attributes on volumes based on the mediums
  SetColors(gGeoManager->GetListOfVolumes());
  SetColors(gGeoManager->GetListOfUVolumes());
  MapVolumes(gGeoManager->GetListOfVolumes());
  MapVolumes(gGeoManager->GetListOfUVolumes());
  MapMediums();

  // Print mappings 
  Print("MV");  
}

//____________________________________________________________________
void
Simulation::Application::Event()
{
  TVirtualMC* mc = TVirtualMC::GetMC();
  if (!mc) {
    Fatal("Event", "No TVirtualMC instantized");
    return;
  }
  mc->ProcessEvent();
}

//____________________________________________________________________
Bool_t
Simulation::Application::Run(Int_t n)
{
  TVirtualMC* mc = TVirtualMC::GetMC();
  if (!mc) {
    Fatal("Run", "No TVirtualMC instantized");
    return kFALSE;
  }
  return mc->ProcessRun(n);
}

//____________________________________________________________________
void
Simulation::Application::Browse(TBrowser* b)
{
  if (fStack)              b->Add(fStack, "Stack");
  if (TVirtualMC::GetMC()) b->Add(TVirtualMC::GetMC());
  if (gGeoManager)         b->Add(gGeoManager);
}

//____________________________________________________________________
void
Simulation::Application::AddParticles()
{ 
  // Do AddParticles
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(15, "AddParticles", "From Application");
  
  Option_t option[] = { Task::kDefineParticles, '\0' };
  main->ExecuteTask(option);
}
//____________________________________________________________________
void
Simulation::Application::ConstructGeometry()
{ 
  // Do ConstructGeometry.  This calls the tasks Initialize member
  // function, which should define materials, mediums, and geometry.
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(15, "ConstructGeometry", "From Application");

  TVirtualMC* mc = TVirtualMC::GetMC();
  if (!mc) { 
    Fatal("ConstructGeometry", "TVirtalMC not instantised");
    return;
  }
  main->Debug(15, "ConstructGeometry", "From Application -> set stack");
  mc->SetStack(fStack);
}
//____________________________________________________________________
void
Simulation::Application::InitGeometry()
{ 
  // Do InitGeometry.  This calls the tasks Register member function
  // which should define branches and set parameters on mediums, VMC,
  // etc.
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(15, "InitGeometry", "From Application");

  if (!gGeoManager) {
    main->Fatal("InitGeometry", "No geometry manager defined.");
    return;
  }
  
  TVirtualMC* mc = TVirtualMC::GetMC();
  gGeoManager->CloseGeometry();

  main->Debug(15, "ConstructGeometry", "From Application -> Register");
  main->ExecRegister();
  
  if (fGeoFileName.IsNull()) return;
  main->Debug(15, "ConstructGeometry",
	      "From Application -> Export geometry to %s", fGeoFileName.Data());
  gGeoManager->Export(fGeoFileName);
}
//____________________________________________________________________
void
Simulation::Application::GeneratePrimaries()
{ 
  // Do GeneratePrimaries
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(20, "GeneratePrimaries", "From Application");
  Option_t option[] = { Task::kGeneratePrimaries, '\0' };
  main->ExecuteTask(option);
}
//____________________________________________________________________
void
Simulation::Application::BeginEvent()
{ 
  // Do BeginEvent
  // TVirtualMC* mc = TVirtualMC::GetMC();
  // if (mc->IsRootGeometrySupported()) gGeoManager->CloseGeometry();
  // mc->BuildPhysics();

  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(20, "BeginEvent", "From Application");
  fStack->Reset();
  Option_t option[] = { Task::kBeginEvent, '\0' };
  main->ExecuteTask(option);
}
//____________________________________________________________________
void
Simulation::Application::BeginPrimary()
{ 
  // Do BeginPrimary
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(25, "BeginPrimary", "From Application");
  Option_t option[] = { Task::kBeginPrimary, '\0' };
  main->ExecuteTask(option);
}
//____________________________________________________________________
void
Simulation::Application::PreTrack()
{ 
  // Do PreTrack
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(30, "PreTrack", "From Application");
  Option_t option[] = { Task::kBeginTrack, '\0' };
  main->ExecuteTask(option);
}
//____________________________________________________________________
void
Simulation::Application::Stepping()
{ 
  // Do Stepping
  Simulation::Main* main = Simulation::Main::Instance();
  TVirtualMC* mc = TVirtualMC::GetMC();
  if (fStack->IsKeepIntermediate() && gGeoManager) {
    TVirtualGeoTrack* track = gGeoManager->GetCurrentTrack();
    if (track) {
      TLorentzVector v;
      mc->TrackPosition(v);
      if (!(isnan(v.X()) || isnan(v.Y()) || isnan(v.Z())))
	track->AddPoint(v.X(), v.Y(), v.Z(), v.T());
    }
  }
  main->Debug(35, "Stepping", "In volume %s", mc->CurrentVolName());
  Int_t med   = mc->CurrentMedium();
  auto  range = Medium2Tasks(med);
  if (range.first == range.second) {
    //  Turns out the below TGeoManager::GetMedium is _really_
    //  expensive - especially with TGeant4 (probably because the
    //  calls goes via the VMC), and it gets called very often as most
    //  hits are in mediums without an associated task.  We therefore
    //  make this contingent on a high debug setting.
    if (main->GetDebug() >= 30) {
      const char* name = "???";
      if (gGeoManager && gGeoManager->GetMedium(med)) 
	name = gGeoManager->GetMedium(med)->GetName();
      main->Debug(30, "Stepping", "no task found for medium # %d (%s)",
		  med,name);
    }
    return;
  }
  for (auto it = range.first; it != range.second; ++it) {
    Simulation::Task* task = it->second;
    if (!task->IsActive()) {
      task->Debug(0, "Stepping", "This task is not active");
      continue;
    }
    task->Debug(35, "Stepping", "delegating to %s (medium %d)",
		task->GetName(), med);
    Option_t option[] = { Task::kStep, '\0' };
    task->Exec(option);
  }
}
//____________________________________________________________________
void
Simulation::Application::PostTrack()
{ 
  // Do PostTrack
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(30, "PostTrack", "From Application");
  Option_t option[] = { Task::kFinishTrack, '\0' };
  main->ExecuteTask(option);
}
//____________________________________________________________________
void
Simulation::Application::FinishPrimary()
{ 
  // Do FinishPrimary
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(25, "FinishPrimary", "From Application");
  Option_t option[] = { Task::kFinishPrimary, '\0' };
  main->ExecuteTask(option);
}
//____________________________________________________________________
void
Simulation::Application::FinishEvent()
{ 
  // Do FinishEvent
  Simulation::Main* main = Simulation::Main::Instance();
  main->Debug(20, "FinishEvent", "From Application");
  Option_t option[] = { Task::kFinishEvent, '\0' };
  main->ExecuteTask(option);
}
//____________________________________________________________________
void
Simulation::Application::Field(const Double_t* x, Double_t* b) const
{ 
  // Get the field
  TVector3 pos(x[0],x[1],x[2]);
  TVector3 vecb;

  auto range = Medium2Tasks(TVirtualMC::GetMC()->CurrentMedium());
  if (range.first == range.second) {
    // No matching tasks
    Simulation::Main::Instance()->Field(pos,vecb);
  }
  else {
    // Take the first task - they are orded in the same order as they
    // were added (i.e., the order added to the main task.).
    
    Simulation::Task* task = range.first->second;
    task->Debug(35, "Field", "From Application");
    task->Field(pos, vecb);
  }
  b[0] = vecb.X();
  b[1] = vecb.Y();
  b[2] = vecb.Z();
}

//____________________________________________________________________
void
Simulation::Application::Print(Option_t* option) const 
{
  TString opt(option);
  if (opt.Contains("V", TString::kIgnoreCase)) {
    if (fMCGeoMap.size() > 0) {
      TROOT::IndentLevel();
      std::cout << "Volume map (MC ID to TGeo ID):" << std::endl;
      TROOT::IncreaseDirLevel();
      for (auto vg : fMCGeoMap) {
	Int_t mc  = vg.first;
	Int_t geo = vg.second;
	std::cout << std::setw(5) << mc
		  << " [" << std::setw(5) << geo << "]: "
		  << gGeoManager->GetVolume(geo)->GetName()
		  << std::endl;
      }
      TROOT::DecreaseDirLevel();
    }
  }
  if (opt.Contains("M", TString::kIgnoreCase)) {
    if (fMediumTaskMap.size() > 0) {
      TROOT::IndentLevel();
      std::cout << "Medium map (ID to task):" << std::endl;
      TROOT::IncreaseDirLevel();
      for (auto it : fMediumTaskMap) {
	TROOT::IndentLevel();
	Simulation::Task* task = it.second;
	std::cout << std::setw(5) << it.first << ": "
		  << task->GetName() << std::endl;
      }
      TROOT::DecreaseDirLevel();    
    }
    
    if (fGeoMediumMap.size() > 0) {
      TROOT::IndentLevel();
      std::cout << "Medium map (TGeoMedium to task):" << std::endl;
      TROOT::IncreaseDirLevel();    

      for (auto mt : fGeoMediumMap) {
	TROOT::IndentLevel();
	TGeoMedium*       medium = mt.first;
	Simulation::Task* task   = mt.second;
	std::cout << std::setw(5)  << medium->GetId()   << " ["
		  << std::setw(16) << medium->GetName() << "]: "
		  << task->GetName() << std::endl;
      }
      TROOT::DecreaseDirLevel();    
    }
  }
}

//____________________________________________________________________
void
Simulation::Application::Draw(Option_t* option,
			      Double_t  theta,
			      Double_t  phi, 
			      Double_t  psi,
			      Double_t  u0,
			      Double_t  v0, 
			      Double_t  ul,
			      Double_t  vl)
{
  TString opt(option);
  Int_t idx = opt.Index(":");
  TString det;
  if (idx < 0) 
    det = "";
  else { 
    det = opt(0, idx);
    opt.Remove(0, idx+1);
  }
  if (gGeoManager) {
    if (!gPad) {
      TCanvas::MakeDefCanvas();
      gPad->SetFillColor(1);
    }
    gPad->Clear();
    TGeoVolume* vol = 0;
    if (!det.IsNull()) vol = gGeoManager->GetVolume(det.Data());
    if (!vol) vol = gGeoManager->GetTopVolume();
    vol->Draw(opt);
    TView* v = gPad->GetView();
    if (!v) return;
    Int_t irep;
    v->SetView(phi, theta, psi, irep);
    // v->SetWindow(u0, v0, ul, vl);
    v->SetViewChanged();
    gPad->Modified();
    gPad->Update();
    gPad->cd();
  }
  else {
   TVirtualMC* mc = TVirtualMC::GetMC();
   if (det.IsNull()) det = mc->VolName(1);
   // mc->Gdraw(det.Data(), theta, phi, psi, u0, v0, ul, vl);
  }
}



//____________________________________________________________________
//
// EOF
//
