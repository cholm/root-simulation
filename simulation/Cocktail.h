// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/Cocktail.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Cocktail
*/
#ifndef Simulation_Cocktail
#define Simulation_Cocktail
#include <TGenerator.h>
#include <TObjArray.h>
class TClonesArray;
class TBrowser;

namespace Simulation 
{
  /** @defgroup sim_gen Event generator interfaces 
      @brief This group contains some event generators that can be
      used in the user code.  */
  /** @class Cocktail simulation/Cocktail.h <simulation/Cocktail.h>
      @ingroup sim_gen
      @brief A cocktail of event generators. 

      One can simple make an instance of this class, and add other
      generators to this instance.  The resulting event will be the
      sum of the event generators. 
  */
  class Cocktail : public TGenerator 
  {
  public:
    /** CTOR */
    Cocktail();
    /** Add another generator to the internal list 
	@param gen Generator to add */
    void Add(TGenerator* gen);
    
    /** Generate one event, and return it in @a a 
	@param a Return array 
	@param option Not used 
      @return  # of particles made */
    Int_t ImportParticles(TClonesArray* a, Option_t* option="");
    /** Generate one event, and return it
	@param option Not used 
	@return  particles made */
    TObjArray* ImportParticles(Option_t* option="");

    /** @param b Browser to use */
    virtual void Browse(TBrowser* b);

    /** @param option Option */
    virtual void Print(Option_t* option="") const; //*MENU*

    /** Is a folder */
    Bool_t IsFolder() const { return kTRUE; }

  private:
    /** List of generators in cocktail. */
    TObjArray fGenerators;
    /** List of generators in cocktail. */
    TObjArray fStacks;
    ClassDef(Cocktail, 0);
  };
}
#endif
