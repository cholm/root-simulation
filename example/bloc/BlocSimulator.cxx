//____________________________________________________________________ 
//  
// $Id: BlocSimulator.cxx,v 1.2 2007-08-05 14:26:34 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/BlocSimulator.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Example::BlocSimulator
*/
#include "BlocSimulator.h"
#include "Hit.h"
#include <simulation/Main.h>
#include <TParticle.h>
#include <TGeoVolume.h>
#include <TGeoTube.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TTree.h>
#include <TLorentzVector.h>
#include <TVirtualMC.h>
#include <TVirtualMC.h>
#include <TFolder.h>
#include <TClonesArray.h>
#include <TCollection.h>

//____________________________________________________________________
ClassImp(Example::BlocSimulator);

//____________________________________________________________________
Example::BlocSimulator::BlocSimulator() 
  : Simulation::Task("BlocSimulator", "Time Projection Chamber")
{
  fCache = new TClonesArray("Example::Hit");
}

//____________________________________________________________________
void
Example::BlocSimulator::Initialize(Option_t* option) 
{
  Debug(14, "Initialize", "Setting up materials");
  Simulation::Task::Initialize(option);

  // We want to process hits in the absorber material 
  TGeoMedium*   medium   = gGeoManager->GetMedium("Absorber");
  RegisterMedium(medium);

  // Make the ring volumes (the inner most) be active 
  TGeoVolume* ringVolume = gGeoManager->GetVolume("RING");
  RegisterVolume(ringVolume);
}

//____________________________________________________________________
void
Example::BlocSimulator::Register(Option_t* option) 
{
  Debug(14, "Register", "Setting up volume");
  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits"))) 
    fBranch = tree->Branch(GetName(), &fCache);
}

//____________________________________________________________________
void
Example::BlocSimulator::Step() 
{
  Debug(34, "Step", "Processing hit");
  TVirtualMC* mc = TVirtualMC::GetMC();
  Int_t ring;
  Int_t vol = mc->CurrentVolID(ring);
  if (!IsSensitive(vol)) return;
  if (!mc->IsTrackAlive()) return;

  Int_t tube;
  Int_t volOffId = mc->CurrentVolOffID(1, tube);
  if (fDebug >= 30) {
    TString volName(mc->CurrentVolName());
    TString volOffName(mc->CurrentVolOffName(1));
    Debug(30, "Step", "Hit in %s (%d) copy # %d (in %s (%d) copy # %d)", 
	  volName.Data(), vol, ring, volOffName.Data(), volOffId, tube);
  }
  if (mc->Edep() <= 0) return;
  TClonesArray*  hits   = static_cast<TClonesArray*>(fCache);
  Int_t          n      = hits->GetEntriesFast();
  Int_t          nTrack = mc->GetStack()->GetCurrentTrackNumber();
  Int_t          pdg    = mc->TrackPid();
  Double_t       edep   = mc->Edep();
  TLorentzVector v, p;
  mc->TrackPosition(v);
  mc->TrackMomentum(p);
  new ((*hits)[n]) Hit(tube, ring, edep, nTrack, pdg, v, p);
  mc->GetStack()->GetCurrentTrack()->SetBit(Simulation::kKeepBit);
}

//____________________________________________________________________
void
Example::BlocSimulator::FinishEvent() 
{
  TClonesArray* hits = static_cast<TClonesArray*>(fCache);
  Int_t n = hits->GetEntriesFast();
  Verbose(1, "FinishEvent", "%d hits in this event", n);
}

//____________________________________________________________________
//
// EOF
//
