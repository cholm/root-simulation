#
# $Id: Makefile.am,v 1.13 2006-06-20 10:31:21 cholm Exp $
#
#  ROOT generic simulation framework 
#  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation; either version 2.1
#  of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free
#  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
#  02111-1307 USA
#
#
LIBNAME				= Example
PCM				= $(LIBNAME)_rdict.pcm	
MAP				= $(LIBNAME).rootmap
bogusdir			= $(addprefix $(PWD)/, \
				    $(top_builddir)/example/.bogus)
bogus_LTLIBRARIES		= Example.la
noinst_HEADERS			= ECalBuilder.h 	\
				  LeakBuilder.h		\
				  LatrBuilder.h		\
				  BlocBuilder.h		\
				  BlocSimulator.h	\
				  Gun.h			\
				  Hit.h			\
				  Profiler.h		\
				  Linkdef.h

Example_la_SOURCES		= ECalBuilder.cxx 	\
				  LeakBuilder.cxx	\
				  LatrBuilder.cxx	\
				  BlocBuilder.cxx	\
				  BlocSimulator.cxx	\
				  Gun.cxx		\
				  Hit.cxx		\
				  Profiler.cxx		
exadir				= $(pkgdatadir)/examples/bloc
exa_DATA			= $(noinst_HEADERS) 	\
				  $(Example_la_SOURCES)	\
			   	  bloc.mk		\
				  README.md
nodist_Example_la_SOURCES	= Example_dict.cxx
Example_la_LDFLAGS		= -L@ROOTLIBDIR@ 	\
				  -R @ROOTLIBDIR@ 	\
				  -L@FRAMEWORK_LIBDIR@  \
				  -avoid-version 	\
				  -module
Example_la_LIBADD		= -lCore -lTree -lEG -lFramework \
				  $(top_builddir)/simulation/libSimulation.la
CLEANFILES			= $(LIBNAME)_dict.cxx $(LIBNAME)_dict.h \
				  *~ *_tmp_* $(PCM) $(MAP) \
				  *.root gphysi.dat *.gif

AM_CPPFLAGS			= -I$(top_srcdir)  			\
				  @VMC_CPPFLAGS@ 			\
				  @FRAMEWORK_CPPFLAGS@ 			\
				  -I@ROOTINCDIR@ 
AM_CXXFLAGS			= $(ROOTAUXCFLAGS)
EXTRA_DIST			= BlocConfig.C 		\
				  Profile.C 		\
				  BlocRun.C 		\
				  Summary.C		\
				  Muon.C 		\
				  MuonConfig.C		\
				  MounRun.C		\
				  run.sh

TESTS				= BlocRun.C MuonRun.C

dict_verbose			= $(dict_verbose_@AM_V@)
dict_verbose_			= $(dict_verbose_@AM_DEFAULT_V@)
dict_verbose_0			= @echo "  DICT     $@";

LOG_COMPILER = $(srcdir)/run.sh
AM_LOG_FLAGS = -L $(top_builddir)/$(PACKAGE)/.libs:$(builddir)/.libs:$(builddir)	\
	       -I $(top_srcdir):$(srcdir) \
	       -M $(srcdir) \
	       -c $(ROOTEXEC) 

$(LIBNAME)_dict.h $(PCM) $(MAP):$(LIBNAME)_dict.cxx
	$(AM_V_at)if test -f $@; then :; else rm -f $^ ; \
	  $(MAKE) $(AM_MAKEFLAGS) $^; fi

$(LIBNAME)_dict.cxx:$(pkginclude_HEADERS) $(noinst_HEADERS)
if ROOT6
# The commented out was an early attempt, wich seems wrong.
# If used, then the last argument for rootcling should be
# $(notdir $^) 
# $(AM_V_at)$(if $(findstring $(srcdir),.),, cp $^ .)
	$(dict_verbose)@ROOTCLING@ -f $@ \
	  -s   $(LIBNAME)$(MODEXT) \
	  -rml $(LIBNAME)$(MODEXT) \
	  -rmf $(LIBNAME).rootmap \
	  -DCLING $(AM_CPPFLAGS) $(INCLUDES) $(DEFS) $^ && \
	  (cd .libs && rm -f $(PCM) && ln -s ../$(PCM) .)
# $(AM_V_at)$(if $(findstring $(srcdir),.),, rm $(notdir $^))
else 
	@ROOTCINT@ -f $@ -c $(AM_CPPFLAGS) $(INCLUDES) $(DEFS) $^
endif
#
# EOF
#
