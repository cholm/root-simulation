// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/TrackReader.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for TrackReader
*/
#ifndef Simulation_TrackReader
#define Simulation_TrackReader
#include <framework/Task.h>
class TClonesArray;

namespace Simulation
{
  /** @class TrackReader TrackReader.h <simulation/TrackReader.h>
      @brief Put stack particles in output tree 
      @ingroup sim_special_tasks
  */
  class TrackReader : public Framework::Task
  {
  public:
    /** Constructor 
	@param name       Name of task and folder 
	@param branchName Name of branch to read from 
	@param treeName   Name of tree to read from
    */
    TrackReader(const char* name       = "Tracks",
		const char* branchName = "Tracks",
		const char* treeName   = "Hits");
    /** Initialize. Register folders.  
	@param option Not used */
    void Initialize(Option_t* option="");
    /** Register input branches. 
	@param option Not used */
    void Register(Option_t* option="");
    /** Executed on each event.  This will loop over the real tracks
	and map the index to the read particles.  Thus, a client task
	may pick up the collection "<name>/Tracks" and use that to
	retrieve particles by track number.  Note that some entries
	may be null */
    void Exec(Option_t* option="");
    /** Access the particles array directly */
    TClonesArray* GetParticles() const { return fParticles; }
    /** Access the mapping array directly */
    TClonesArray* GepMap() const  { return fMap; }
    /** Access the tracks array directly */
    TObjArray* GetTracks() const { return fTracks; }
  protected:
    /** Tree name */
    TString fTreeName;
    /** Branch name */
    TString fBranchName;
    /** The read particles */
    TClonesArray* fParticles;
    /** Map trackno to cache index */
    TClonesArray* fMap;
    /** Mapped track number to TParticle */
    TObjArray* fTracks;
    ClassDef(TrackReader,0); //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
