//
//   ROOT generic simulation framework
//   Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   doc/Examples.cxx
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Examples. */


/** @page examples Examples 
    Here are the various examples.
 */
/** @example example/muon/Muon.C
    @par A simple @f$ \mu^\pm@f$ life-time experiment */
/** @example example/muon/Config.C
    @par Configuration for a simple @f$ \mu^\pm@f$ life-time experiment */
/** @example example/bloc/Hit.h
    @par Declaration of Hit data class. */
/** @example example/bloc/Hit.cxx
    @par Implementation of Hit data class. */
/** @example example/bloc/ECalBuilder.h
    @par Declaration of ECAL geometry builder class. */
/** @example example/bloc/ECalBuilder.cxx
    @par Implementation of ECAL geometry builder class. */
/** @example example/bloc/LeakBuilder.h
    @par Declaration of LEAK geometry builder class. */
/** @example example/bloc/LeakBuilder.cxx
    @par Implementation of LEAK geometry builder class. */
/** @example example/bloc/LatrBuilder.h
    @par Declaration of LATR geometry builder class. */
/** @example example/bloc/LatrBuilder.cxx
    @par Implementation of LATR geometry builder class. */
/** @example example/bloc/BlocBuilder.h
    @par Declaration of BLOC geometry builder class. */
/** @example example/bloc/BlocBuilder.cxx
    @par Implementation of BLOC geometry builder class. */
/** @example example/bloc/BlocSimulator.h
    @par Declaration of BLOC simulation task. */
/** @example example/bloc/BlocSimulator.cxx
    @par Implementation of BLOC simulation task. */
/** @example example/bloc/Gun.h
    @par Declaration of Gun task class. */
/** @example example/bloc/Gun.cxx
    @par Implementation of Gun task class. */
/** @example example/bloc/Profiler.h
    @par Declaration of Profiler task (a plain Framework::Task) to
    make profile histograms of the output of the simulation. 
    @image html profile.png
*/
/** @example example/bloc/Profiler.cxx
    @par Implementation of Profiler task (a plain Framework::Task) to
    make profile histograms of the output of the simulation. */
/** @example example/bloc/Config.C
    @par Configuration script. */
/** @example example/bloc/Analysis.C
    @par Framework::Main configuration script to make profile
    histograms. */ 
/** @example example/bloc/Summary.C
    @par Script to make Summary (profile) histograms directly from the
    output tree . 
*/

//
// EOF
//
