dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: simulation.m4,v 1.5 2006-03-22 13:56:21 cholm Exp $ 
dnl  
dnl  ROOT generic simulation framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_ROOT_SIMULATION([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ROOT_SIMULATION],
[
    AC_REQUIRE([AC_ROOT_FRAMEWORK])
    AC_ARG_WITH([simulation-prefix],
        [AC_HELP_STRING([--with-simulation-prefix],
		[Prefix where Simulation is installed])],
        simulation_prefix=$withval, simulation_prefix="")

    AC_ARG_WITH([simulation-url],
        [AC_HELP_STRING([--with-simulation-url],
		[Base URL where the Simulation dodumentation is installed])],
        simulation_url=$withval, simulation_url="")
    if test "x${SIMULATION_CONFIG+set}" != xset ; then 
        if test "x$simulation_prefix" != "x" ; then 
	    SIMULATION_CONFIG=$simulation_prefix/bin/simulation-config
	fi
    fi   
    AC_PATH_PROG(SIMULATION_CONFIG, simulation-config, no)
    simulation_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Simulation version >= $simulation_min_version)

    simulation_found=no    
    if test "x$SIMULATION_CONFIG" != "xno" ; then 
       SIMULATION_CPPFLAGS=`$SIMULATION_CONFIG --cppflags`
       SIMULATION_INCLUDEDIR=`$SIMULATION_CONFIG --includedir`
       SIMULATION_LIBS=`$SIMULATION_CONFIG --libs`
       SIMULATION_LIBDIR=`$SIMULATION_CONFIG --libdir`
       SIMULATION_LDFLAGS=`$SIMULATION_CONFIG --ldflags`
       SIMULATION_PREFIX=`$SIMULATION_CONFIG --prefix`
       
       simulation_version=`$SIMULATION_CONFIG -V` 
       simulation_vers=`echo $simulation_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       simulation_regu=`echo $simulation_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $simulation_vers -ge $simulation_regu ; then 
            simulation_found=yes
       fi
    fi
    AC_MSG_RESULT($simulation_found - is $simulation_version) 
    AC_MSG_CHECKING(where the Framework documentation is installed)
    if test "x$simulation_url" = "x" && \
	test ! "x$SIMULATION_PREFIX" = "x" ; then 
       SIMULATION_URL=${SIMULATION_PREFIX}/share/doc/simulation/html
    else 
	SIMULATION_URL=$simulation_url
    fi	
    AC_MSG_RESULT($SIMULATION_URL)
   
    if test "x$simulation_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(SIMULATION_URL)
    AC_SUBST(SIMULATION_PREFIX)
    AC_SUBST(SIMULATION_CPPFLAGS)
    AC_SUBST(SIMULATION_INCLUDEDIR)
    AC_SUBST(SIMULATION_LDFLAGS)
    AC_SUBST(SIMULATION_LIBDIR)
    AC_SUBST(SIMULATION_LIBS)
])

dnl
dnl EOF
dnl 
