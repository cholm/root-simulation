//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/DefaultHit.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Simulation::DefaultHit
*/
#include "simulation/DefaultHit.h"
#include <TMath.h>
#include <TMarker3DBox.h>
#include <TROOT.h>
#include <TDatabasePDG.h>
#include <TString.h>
#include <iostream>
#include <iomanip>

//____________________________________________________________________
ClassImp(Simulation::DefaultHit);

//____________________________________________________________________
Simulation::DefaultHit::DefaultHit() 
  : Hit(),
    fNTrack(0),
    fPdg(0),
    fV(0, 0, 0, 0),
    fP(0, 0, 0, 0),
    fEnergyLoss(0),
    fName(""),
    fTitle("")
{}

//____________________________________________________________________
Simulation::DefaultHit::DefaultHit(Int_t    nTrack,
				   Int_t    pdg,
				   Double_t x,
				   Double_t y,
				   Double_t z,
				   Double_t t,
				   Double_t px,
				   Double_t py,
				   Double_t pz,
				   Double_t e,
				   Double_t energyLoss)
  : Hit(),
    fNTrack(nTrack),
    fPdg(pdg),
    fV(x, y, z, t),
    fP(px, py, pz, e),
    fEnergyLoss(energyLoss)
{}

//____________________________________________________________________
Simulation::DefaultHit::DefaultHit(Int_t                 nTrack,
				   Int_t                 pdg,
				   const TLorentzVector& v,
				   const TLorentzVector& p,
				   Double_t               energyLoss)
  : Hit(),
    fNTrack(nTrack),
    fPdg(pdg),
    fV(v),
    fP(p),
    fEnergyLoss(energyLoss),
    fName(""),
    fTitle("")
{}

//____________________________________________________________________
Simulation::DefaultHit::DefaultHit(const DefaultHit& other) 
  : Hit(other),
    fNTrack(other.fNTrack),
    fPdg(other.fPdg),
    fV(other.fV),
    fP(other.fP),
    fEnergyLoss(other.fEnergyLoss),
    fName(other.fName),
    fTitle(other.fTitle)
{}

//____________________________________________________________________
Simulation::DefaultHit&
Simulation::DefaultHit::operator=(const DefaultHit& other) 
{
  this->Hit::operator=(other);
  fNTrack      = other.fNTrack;
  fPdg         = other.fPdg;
  fV           = other.fV;
  fP           = other.fP;
  fEnergyLoss  = other.fEnergyLoss;
  return *this;
}
//____________________________________________________________________
const char*
Simulation::DefaultHit::GetName() const
{
  if (fName.IsNull()) {
    TParticlePDG* pdg = TDatabasePDG::Instance()->GetParticle(fPdg);
    fName = Form("%6s (%6d)",(pdg ? pdg->GetName() : "?"), fNTrack);
  }
  return fName.Data();
}
//____________________________________________________________________
const char*
Simulation::DefaultHit::GetTitle() const
{
  if (fTitle.IsNull()) fTitle = Form("%10g GeV",fEnergyLoss);
  return fTitle.Data();
}


//____________________________________________________________________
TObject*
Simulation::DefaultHit::Marker(Option_t* option) 
{
  Double_t size  = TMath::Min(TMath::Max(fEnergyLoss * .1, 1.), .1);
  return Marker3D(fV.X(), fV.Y(), fV.Z(), 
		  size, size, size, 
		  fP.Theta(), fP.Phi(),
		  option);
}

//____________________________________________________________________
void
Simulation::DefaultHit::Print(Option_t* option) const
{
  std::cout << "Simulation::DefaultHit: " << fEnergyLoss << "GeV" << std::endl;
  TString opt(option);
  if(opt.Contains("P", TString::kIgnoreCase)) {
    TDatabasePDG* pdgDb = TDatabasePDG::Instance();
    TParticlePDG* pdgP  = pdgDb->GetParticle(fPdg);
    std::cout << "  Particle: " << pdgP->GetName() 
	      << " (track # " << fNTrack << ") x=[" 
	      << std::setw(10) << fV.X() << "," 
	      << std::setw(10) << fV.Y() << "," 
	      << std::setw(10) << fV.Z() << "," 
	      << std::setw(10) << fV.T() << "](cm,s) p=[" 
	      << std::setw(10) << fP.Px() << "," 
	      << std::setw(10) << fP.Py() << "," 
	      << std::setw(10) << fP.Pz() << "," 
	      << std::setw(10) << fP.E() << "](GeV)" << std::endl;
  }
}

    
//____________________________________________________________________
//
// EOF
//
