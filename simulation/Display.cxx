//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/Display.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Simulation::Display
*/
#include "simulation/Display.h"
#include "simulation/Main.h"
#include "simulation/Hit.h"
#include <TClass.h>
#include <TClonesArray.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TTimer.h>
#include <TView.h>
#include <TPaveLabel.h>
#include <TLatex.h>
#include <TApplication.h>
#include <TMarker3DBox.h>
#include <TGeoManager.h>
#include <TGeoVolume.h>
#include <TVirtualGeoTrack.h>
#include <TFolder.h>
#include <TButton.h>
#include <TF1.h>
#include <TParticle.h>
#include <TVirtualMC.h>
#include <TTree.h>
#include <TROOT.h>
#include <TSlider.h>
#include <TObjString.h>
#include <iostream>

namespace {
  //____________________________________________________________________
  struct Listener : public TObject
  {
    Listener(Simulation::Display* display) : fDisplay(display) {}
    void Draw(Option_t* option="") { AppendPad(option); }
    void Paint(Option_t* option="") { fDisplay->Idle(); }
    Simulation::Display* fDisplay;
  };

  //____________________________________________________________________
  struct Marker : public TObject
  {
    Marker(TObject* marker, Simulation::Hit* hit)
      : fMarker(marker), fHit(hit)
    {
    }
    ~Marker()
    {
      if (fMarker) delete fMarker;
    }
    void Draw(Option_t* option="")
    {
      AppendPad(option);
    }
    void Paint(Option_t* option="")
    {
      Double_t minT, maxT, t = fHit->T();
      Bool_t cutT            = gGeoManager->GetTminTmax(minT,maxT);
      Bool_t drawIt          = (t <= -999 || !cutT ||
				(t <= maxT && t >= minT));
      // Info("Paint","t<=-999: %d   !cutT: %d   t<=maxT: %d  t>=minT: %d",
      // 	   (t<=-999), (!cutT), (t<=maxT), (t>=minT));
      // Info("Paint","Hit t=%6g cut=[%6g,%6g] enable=%d -> draw=%d",
      // 	   t, minT, maxT, cutT,drawIt);
      if (drawIt) fMarker->Paint(option);
    }
    Int_t DistancetoPrimitive(Int_t px,Int_t py) {
      return fMarker->DistancetoPrimitive(px,py);
    }
    const Char_t* GetName() const { return fHit->GetName(); }
    const Char_t* GetTitle() const { return fHit->GetTitle(); }
    void Print(Option_t* option="") const { return fHit->Print(option); }
    TObject* fMarker;
    Simulation::Hit* fHit;
  };
  //____________________________________________________________________
  struct TimeDisplay : public TLatex
  {
    TimeDisplay(Double_t x, Double_t y, const char* txt)
      : TLatex(x,y,txt)
    {
      SetTextColor(kCyan+1);
      SetTextSize(.03);
      SetTextAlign(33); // Top-right
    }
    void Draw(Option_t* option="") {
      TLatex::Draw(option);
      ResetBit(kMustCleanup);
    }
    void Paint(Option_t* option="") {
      Double_t minT, maxT;
      Bool_t cutT = gGeoManager->GetTminTmax(minT,maxT);
      if (!cutT) SetText(GetX(),GetY(),"all times"); 
      else    	 SetText(GetX(),GetY(),Form("t#in[%g,%g]",minT,maxT));
      TLatex::Paint(option);
    }
  };
}

//____________________________________________________________________
ClassImp(Simulation::Display);

//____________________________________________________________________
Simulation::Display* Simulation::Display::fgInstance = 0;

//====================================================================
Simulation::Display* 
Simulation::GetDisplay()
{
  return Simulation::Display::Instance();
}
#define GD TString("Simulation::Display::Instance()")

//____________________________________________________________________
Simulation::ToggleButton::ToggleButton(const char* text,
				       Bool_t      down,
				       Double_t    llx,
				       Double_t    lly,
				       Double_t    urx,
				       Double_t    ury,
				       Display*    display,
				       Option_t*   option)
  : TButton(text,"",llx,lly,urx,ury),
    fIsDown(down),
    fDisplay(display),
    fToggleOption(option)
{
  SetMethod(Form("((Simulation::ToggleButton*)%p)->Clicked()",this));
  SetColor();
}
//____________________________________________________________________
void
Simulation::ToggleButton::SetColor()
{
  SetFillColor(fIsDown ? kGreen-10 : kRed-10);
  SetLineColor(kGray);
}  
//____________________________________________________________________
void
Simulation::ToggleButton::Clicked()
{
  fIsDown = !fIsDown;
  SetColor();
  fDisplay->ToggleOption(fToggleOption, fIsDown);  
}
				       
//====================================================================
Simulation::Display* 
Simulation::Display::Instance()
{
  return fgInstance;
}


//====================================================================
Simulation::Display::Display(const Char_t* name, const Char_t* title) 
  : Framework::Task(name, title),
    fCanvas(0), 
    f3DView(0),
    fLabel(0),
    fTime(0),
    fEnabled(0xffff),
    fContinue(0),
    fZoom(0),
    fUnZoom(0),
    fLeft(0),
    fRight(0),
    fUp(0),
    fDown(0),
    fDrawHits(0),
    fDrawTracks(0),
    fAnimate(0),
    fPan(0),
    fRecord(0),
    fWait(kFALSE),
    fFirst(kTRUE),
    fWidth(800), 
    fMinT(0),
    fMaxT(2E-8), 
    fNFrames(100), 
    fSlider(0),
    fZoomFactor(1),
    fLatitude(50),
    fLongitude(-200),
    fPsi(110),
    fEventNo(0),
    fTScale(0), 
    fZoomMode(kTRUE),
    fX0(0),
    fY0(0),
    fX1(0),
    fY1(0),
    fOldXPixel(0),
    fOldYPixel(0),
    fLineDrawn(kFALSE),
    fExtraDraw(),
    fMarkers()
{
  fOption    = "HT";
  fgInstance = this;
  // fTScale    = new TF1("scale_t", "exp(x*[0])/exp([0])", 0, 1);
  // fTScale->SetParameter(0, 10);
  // fTScale    = new TF1("scale_t", "[0]*exp(x*[4])/", 0, 1);
  // fTScale = new TF1("scale_t","([1]-[0])*exp(x*[2])/exp([2])+[0]",0,1);
  // fTScale = new TF1("scale_t","([1]-[0])*x+[0]",0,1);
  // fTScale = new TF1("scale_t","([1]-[0])*pow(x,2)-[0]",0,1);//Was quadratic
  // fTScale->SetParameters(0,1,5);
  fTScale = new TF1("scale_t","pow(x,2)",0,1);

  fCache = new TObjArray();
}


//____________________________________________________________________
Simulation::Display::~Display() 
{
  if (fCanvas) delete fCanvas;
  fgInstance = 0;
}

//____________________________________________________________________
void
Simulation::Display::SetTScale(const char* expr)
{
  if (fTScale) delete fTScale;
  fTScale = new TF1("scale_t",expr,0,1);
}
//____________________________________________________________________
void
Simulation::Display::SetTScale(const TF1& f)
{
  if (fTScale) delete fTScale;
  fTScale = new TF1(f);
  fTScale->SetRange(0,1);
}

//____________________________________________________________________
void
Simulation::Display::DrawHit(TObject* o)
{
  Simulation::Hit* hit = static_cast<Simulation::Hit*>(o);
  TObject* marker = hit->Marker();
  if (!marker) return;
  Marker* mhit = new Marker(marker, hit);
  fMarkers.Add(mhit); // mhit->Draw();

  // Debug(50, "DrawFolder", "Drawning hit at t=%f if it's within [%f,%f]", 
  //    hit->T(), fMaxT, fMinT);
  // if (hit->T() <= fMaxT && hit->T() >= fMinT) hit->Draw();
  // hit->Draw();
}

//____________________________________________________________________
void
Simulation::Display::DrawFolder(TFolder* folder) 
{
  if (!folder) return;
  Debug(10, "DrawFolder", "Drawing folder %s", folder->GetName());
  folder->Draw();
  TCollection* folders = folder->GetListOfFolders();
  TIter        next(folders);
  TObject* o = 0;
  while ((o = next())) {
    Debug(15, "DrawFolder", "Drawing object %s", o->GetName());
    if (o->IsA() == TTree::Class()) continue;
    if (o->IsA() == TParticle::Class()) continue;
    if (o->IsA() == TClonesArray::Class()) {
      TClonesArray* cl = static_cast<TClonesArray*>(o);
      if (cl->GetClass() == TParticle::Class()) continue;
      if (cl->GetClass()->InheritsFrom(Simulation::Hit::Class())) {
	Debug(50, "DrawFolder", "Drawing TClonesArray of hits");
	Int_t n = cl->GetEntries();
	for (Int_t i = 0; i < n; i++) DrawHit(cl->At(i));
	continue;
      }
    }
    if (o->IsA()->InheritsFrom(Simulation::Hit::Class())) {
      DrawHit(o);
      continue;
    }
    o->Draw();
    if (o->IsA() == TFolder::Class())
      DrawFolder(static_cast<TFolder*>(o));
  }
}
//____________________________________________________________________
void
Simulation::Display::DrawFolders()
{
  TFolder* top = 0;
  if (!(top = GetBaseFolder())) {
    Warning("DrawFolders", "couldn't get top-level folder");
    return;
  }
  fMarkers.Delete(); // Clear markers array
  f3DView->cd();
  // std::swap(minT,fMinT);
  // std::swap(maxT,fMaxT);
  DrawFolder(top);
  Debug(1,"DrawFolders","Drawing %d markers",fMarkers.GetEntries());
  fMarkers.Draw();
  // std::swap(fMinT,minT);
  // std::swap(fMaxT,maxT);
  f3DView->Modified();
  f3DView->cd();
  // if (fDebug > 1) f3DView->GetListOfPrimitives()->ls();
}

//____________________________________________________________________
void
Simulation::Display::Register(Option_t* option)
{
}
//____________________________________________________________________
void
Simulation::Display::Exec(Option_t* option) 
{
  // If we're running in batch mode, don't do anything 
  if (gROOT->IsBatch()) return;
  // If we're executed on some step that is not FinishEvent, don't do
  // anything 
  if (option[0] == Simulation::Task::kDefineParticles   || 
      option[0] == Simulation::Task::kGeneratePrimaries ||
      option[0] == Simulation::Task::kBeginEvent        ||
      option[0] == Simulation::Task::kBeginPrimary      ||
      option[0] == Simulation::Task::kBeginTrack        ||
      option[0] == Simulation::Task::kStep              ||
      option[0] == Simulation::Task::kField             ||
      option[0] == Simulation::Task::kFinishTrack       ||
      option[0] == Simulation::Task::kFinishPrimary) return;
  // If there's geometry defined, then don't do anything, and fail. 
  if (!gGeoManager) {
    Warning("Exec", "No TGeoManager defined");
    return;
  }
  if  (!gGeoManager->GetTopVolume()) {
    Warning("Exec", "No top volume set in the TGeoManager");
    return;
  }
  fEventNo++;
  FindT();
  SetupCanvas();

  // fFirst = kTRUE;
  Draw(fOption.Data());
  fWait = kTRUE;
  while (fWait) {
    gApplication->StartIdleing();
    gSystem->InnerLoop();
    gApplication->StopIdleing();
  }
}

//____________________________________________________________________
void
Simulation::Display::ToggleOption(Option_t* option, Bool_t on)
{
  if (!on)                            fOption.ReplaceAll(option,"");
  else if (!fOption.Contains(option)) fOption.Append(option);
  Debug(10,"ToggleOption", "Toggle '%s' %s -> \"%s\"",
	option, (on ? "on" : "off"), fOption.Data());
  DrawAgain();
}

//____________________________________________________________________
void
Simulation::Display::SetupCanvas()
{
  if (fCanvas) return;
  Int_t w = fWidth;
  Int_t h = Int_t(w * 1.1);
  fCanvas = new TCanvas(GetName(), GetTitle(), w, h);
  fCanvas->SetFillColor(1);
  fCanvas->SetBorderMode(0);
  fCanvas->ToggleEventStatus();
  
  f3DView    = new TPad("view3D", "3DView", 0.0, 0.1, 1.0, 1.0, 1, 0, 0);
  fCanvas->cd();
  f3DView->Draw();

  fLabel = 
    new TPaveLabel(f3DView->GetUxmin(), .85 * f3DView->GetUymax(), 
		   .5  * f3DView->GetUxmin(), f3DView->GetUymax(), 
		   Form("Event # ?\n"));
  fLabel->SetFillColor(0);
  fLabel->SetFillStyle(0);
  fLabel->SetTextColor(kWhite);
  fLabel->SetBorderSize(0);
  fLabel->Draw();

  fTime = new TimeDisplay(.95*f3DView->GetUxmax(),.95*f3DView->GetUymax(),"");
  fCache->Add(fTime);
  fTime->Draw();

  DrawExtra();
  
  Int_t m = 3;
  Int_t n = 3;
  if (TESTBIT(fEnabled,kHits))       n++;
  if (TESTBIT(fEnabled,kTracks))     n++;
  if (TESTBIT(fEnabled,kPan))        n++;
  if (TESTBIT(fEnabled,kRecord))     n++;
  if (TESTBIT(fEnabled,kAnimate))    n++;

  SetupNavigation(n);
  SetupActions(n, m);
  SetupSlider();
}

//____________________________________________________________________
void
Simulation::Display::SetupNavigation(Int_t n)
{
  fContinue=new TButton("Continue",GD+"->Cont()",0./n, .03,  1.0/n, .1);
  fContinue->SetTextSize(.3);
  fContinue->Draw();
  fZoom    =new TButton("+", GD+"->Zoom(1)",	1./n, .065, 1.5/n, .1);
  fZoom->Draw();
  fUnZoom  =new TButton("-", GD+"->Zoom(-1)",	1./n, .03,  1.5/n,  .065);
  fUnZoom->Draw();
  fLeft    =new TButton("<", GD+"->Move('l')",	1.5/n,.03,  2.0/n, .1);
  fLeft->Draw();
  fUp      =new TButton("^", GD+"->Move('i')",	2.0/n,.065, 2.5/n, .1);
  fUp->Draw();
  fDown    =new TButton("v", GD+"->Move('u')",	2.0/n,.03,  2.5/n, .065);
  fDown->Draw();
  fRight   =new TButton(">", GD+"->Move('h')",	2.5/n,.03,  3.0/n, .1);
  fRight->Draw();
}

//____________________________________________________________________
void
Simulation::Display::SetupActions(Int_t n, Int_t m)
{
  ToggleButton* buttons[] = { fDrawHits,     fDrawTracks,
			      fAnimate,      fPan,          fRecord };
  const char* names[]     = { "Hits",        "Tracks", 
			      "Animate",     "Pan",         "Record" };
  const char* option[]    = { "H", "T", "A", "P", "R" };
  Int_t j = m;
  for (Int_t i = 0; i < 5; i++) {
    fCanvas->cd();
    if (!TESTBIT(fEnabled, i+1)) continue;
    Bool_t   down = fOption.Contains(option[i]);
    Double_t llx  = Double_t(j) / n;
    Double_t urx  = Double_t(j+1) / n;
    Double_t lly  = .03;
    Double_t ury  = .1;
    Debug(10,"SetupActions",
	  "Generating toggle button %s (%d) (%f,%f,%f,%f) w/%s",
	  names[i],down,llx,lly,urx,ury,option[i]);
    buttons[i] = new ToggleButton(names[i],down,llx,lly,urx,ury,this,option[i]);
    buttons[i]->SetTextSize(.3);
    buttons[i]->Draw();
    j++;
  }
}

//____________________________________________________________________
void
Simulation::Display::SetupSlider()
{
  fSlider = new TSlider("time", "Adjust time", 0, 0, 1, .03);
  fSlider->SetMethod(GD+"->DrawAgain()");
}


//____________________________________________________________________
void
Simulation::Display::Cont() 
{ 
  fFirst = kFALSE; 
  fWait = kFALSE; 
}

//____________________________________________________________________
void
Simulation::Display::Zoom(Int_t d) 
{
  if (f3DView) {
    if (d > 0) { f3DView->GetView()->Zoom();   fZoomFactor *= 1.25; }
    else       { f3DView->GetView()->UnZoom(); fZoomFactor /= 1.25; }
    f3DView->Modified();
    f3DView->cd();
  }
}
//____________________________________________________________________
void
Simulation::Display::Move(char d) 
{
  if (f3DView) { 
    f3DView->GetView()->MoveWindow(d);
    f3DView->Modified();
    f3DView->cd();
  }
}

//____________________________________________________________________
Int_t
Simulation::Display::GetEventNo() 
{
  TVirtualMC* mc = TVirtualMC::GetMC();
  if (mc) return mc->CurrentEvent();
  return fEventNo;
}

//____________________________________________________________________
void
Simulation::Display::FindT()
{
  FindTTracks();
  // Verbose(0,"FindT","Time range from tracks [%g,%g]",fMinT,fMaxT);
  // FindTFolders();
  // Verbose(0,"FindT","Time range from folders [%g,%g]",fMinT,fMaxT);
}
  
//____________________________________________________________________
void
Simulation::Display::FindTTracks() 
{
  fMaxT = -1e30;
  fMinT = 1e30;
  TObjArray* tracks = gGeoManager->GetListOfTracks();
  if (!tracks) { 
    fMinT = 0;
    fMaxT = 1;
    return;
  }
  TVirtualGeoTrack* track;
  TIter next(tracks);
  while ((track = static_cast<TVirtualGeoTrack*>(next()))) FindT(track);

  // fTScale->SetParameter(0,fMinT);
  // fTScale->SetParameter(1,fMaxT);
  // fTScale->Print("V");
}

//____________________________________________________________________
void
Simulation::Display::FindT(TVirtualGeoTrack* track) 
{
  if (!track) return;
  Double_t x, y, z, t1, t2;
  track->GetPoint(0, x, y, z, t1);
  fMinT = TMath::Min(fMinT, t1);
  track->GetLastPoint(x, y, z, t2);
  fMaxT = TMath::Max(fMaxT, t2);
  Debug(10,"FindT","t = %g,%g -> %g,%g",t1,t2,fMinT,fMaxT);
  for (Int_t i = 0; i < track->GetNdaughters(); i++) 
    FindT(track->GetDaughter(i));
}
//____________________________________________________________________
void
Simulation::Display::FindTFolders()
{
  fMaxT = -1e30;
  fMinT = 1e30;
  TFolder* top = 0;
  if (!(top = GetBaseFolder())) {
    Warning("FindTFolders", "couldn't get top-level folder");
    return;
  }
  FindT(top);
}
//____________________________________________________________________
void
Simulation::Display::FindT(TFolder* folder) 
{
  if (!folder) return;
  Debug(10, "FindT", "Find min/max in folder %s", folder->GetName());
  TCollection* folders = folder->GetListOfFolders();
  TIter        next(folders);
  TObject* o = 0;
  while ((o = next())) {
    Debug(15, "DrawFolder", "Drawing object %s", o->GetName());
    if (o->IsA() == TTree::Class()) continue;
    if (o->IsA() == TParticle::Class()) continue;
    if (o->IsA() == TClonesArray::Class()) {
      TClonesArray* cl = static_cast<TClonesArray*>(o);
      if (cl->GetClass() == TParticle::Class()) continue;
      if (cl->GetClass()->InheritsFrom(Simulation::Hit::Class())) {
	Debug(50, "DrawFolder", "Drawing TClonesArray of hits");
	Int_t n = cl->GetEntries();
	for (Int_t i = 0; i < n; i++) FindT(cl->At(i));
	continue;
      }
    }
    if (o->IsA()->InheritsFrom(Simulation::Hit::Class())) {
      FindT(o);
      continue;
    }
    o->Draw();
    if (o->IsA() == TFolder::Class())
      FindT(static_cast<TFolder*>(o));
  }
}
//____________________________________________________________________
void
Simulation::Display::FindT(TObject* o)
{
  Simulation::Hit* hit = static_cast<Simulation::Hit*>(o);
  fMinT = TMath::Min(fMinT, hit->T());
  fMaxT = TMath::Max(fMaxT, hit->T());
}

//____________________________________________________________________
void
Simulation::Display::Idle()
{
  return;
}


//____________________________________________________________________
void
Simulation::Display::DrawAgain()
{
  Draw(fOption);
}

//____________________________________________________________________
void
Simulation::Display::Draw(Option_t* option) 
{
  Double_t rmin[3], rmax[3];
  if (!fFirst) {
    fLatitude    = f3DView->GetView()->GetLatitude();
    fLongitude   = f3DView->GetView()->GetLongitude();
    fPsi         = f3DView->GetView()->GetPsi();
    f3DView->GetView()->GetRange(rmin,rmax);
  }
  fOption = option;

  f3DView->Clear();
  f3DView->cd();
  Int_t irep;
  TGeoVolume* top = gGeoManager->GetMasterVolume();
  top->Draw();  
  f3DView->GetView()->SetView(fLongitude, fLatitude, fPsi, irep);
  if (!fFirst) f3DView->GetView()->SetRange(rmin,rmax);
  f3DView->GetView()->ZoomView(f3DView, fZoomFactor);
  Int_t event = GetEventNo();
  f3DView->cd();
  fLabel->SetLabel(Form("Event # %d\n",event));
  fLabel->SetX1(f3DView->GetUxmin());
  fLabel->SetY1(.85 * f3DView->GetUymax());
  fLabel->SetX2(.5  * f3DView->GetUxmin());
  fLabel->SetY2(f3DView->GetUymax());
  fLabel->Draw();
  fLabel->ResetBit(kMustCleanup);
  fTime->SetX(.95*f3DView->GetUxmax());
  fTime->SetY(.95*f3DView->GetUymax());
  fTime->ResetBit(kMustCleanup);
  fTime->Draw();

  DrawExtra();
  
  f3DView->Modified();
  f3DView->cd();

  
  Double_t minT = fTScale->Eval(TMath::Max(fSlider->GetMinimum(),0.));
  Double_t maxT = fTScale->Eval(TMath::Min(fSlider->GetMaximum(),1.));
  Double_t dT   = fMaxT - fMinT;
  minT          *= dT;
  maxT          *= dT;
  minT          += fMinT;
  maxT          += fMinT;
  Debug(10,"Draw()","Time range: %g -> %g  Slider %f -> %f  Real %g -> %g",
	fMinT, fMaxT,
	fSlider->GetMinimum(), fSlider->GetMaximum(),
	minT, maxT);
  // Set the time range based on the slider, but only if not full
  // range.
  if (fSlider->GetMinimum() > 0 or fSlider->GetMaximum() < 1)  {
    Debug(10,"Draw","Slider=[%g,%g] T=[%g,%g] -> [%g,%g]",
     	  fSlider->GetMinimum(),fSlider->GetMaximum(),
     	  fMinT, fMaxT, minT, maxT);
    gGeoManager->SetTminTmax(minT, maxT);
  }
  else {
    Debug(10,"Draw","Slider all the way out, no T cut");
    // No time cut
    gGeoManager->SetTminTmax();
  }

  Bool_t animate = fOption.Contains("A", TString::kIgnoreCase);
  Bool_t pan     = fOption.Contains("P", TString::kIgnoreCase);
  Bool_t record  = fOption.Contains("R", TString::kIgnoreCase);
  Bool_t hits    = fOption.Contains("H", TString::kIgnoreCase);
  Bool_t tracks  = fOption.Contains("T", TString::kIgnoreCase);
  
  if (animate) {
    Debug(10,"Draw(A)","Animate tracks over %d frames", fNFrames);

    // Hack to force canvas update 
    if (!pan and !record) gROOT->SetFromPopUp(true);

    // Actually animate the tracks
    if (hits) DrawFolders();
    if (tracks) {
      TString aopt("/*");
      if (pan)    aopt.Append("/G");
      if (record) aopt.Append("/S");
      Debug(10,"Draw","Animate tracks with options %s",aopt.Data());
      gGeoManager->AnimateTracks(minT, maxT, fNFrames, aopt.Data());
    }
    // Revert hack
    if (!pan and !record) gROOT->SetFromPopUp(false);
  }
  else {  
    if (tracks) gGeoManager->DrawTracks("/*");
    if (hits)   DrawFolders();
  }
  f3DView->Modified();
  f3DView->cd();
  //  AppendPad();
  
  fCanvas->Modified();
  fCanvas->Update();
  fCanvas->cd();
  fFirst = kFALSE;
}

//____________________________________________________________________
void
Simulation::Display::AddDraw(const char* cmd)
{
  fExtraDraw.Add(new TObjString(cmd));
}
//____________________________________________________________________
void
Simulation::Display::AddDraw(TObject* o)
{
  fExtraDraw.Add(o);
}

//____________________________________________________________________
void
Simulation::Display::DrawExtra()
{
  for (auto* o : fExtraDraw) {
    if (o->InheritsFrom(TObjString::Class()))
      gROOT->ProcessLine(o->GetName());
    else
      o->Draw();
  }
}

//____________________________________________________________________
void
Simulation::Display::ExecuteEvent(Int_t event, Int_t px, Int_t py)
{
  // AliInfo(Form("Event %d, at (%d,%d)", px, py));
  Debug(10,"ExecuteEvent","Executing event %d at (%d,%d)",event,px,py);
  if (px == 0 && py == 0) return;
  if (!fZoomMode && f3DView->GetView()) {
    f3DView->GetView()->ExecuteRotateView(event, px, py);
    return;
  }
  f3DView->SetCursor(kCross);
  switch (event) {
  case kButton1Down:
    f3DView->TAttLine::Modify();
    fX0        = f3DView->AbsPixeltoX(px);
    fY0        = f3DView->AbsPixeltoY(py);
    fXPixel    = fOldXPixel = px;
    fYPixel    = fOldYPixel = py;
    fLineDrawn = kFALSE;
    return;
  case kButton1Motion:
    // if (fLineDrawn)
    //   gVirtualX->DrawBox(fXPixel, fYPixel, fOldXPixel, fOldYPixel,
    //                      TVirtualX::kHollow);
    fOldXPixel = px;
    fOldYPixel = py;
    fLineDrawn = kTRUE;
    // gVirtualX->DrawBox(fXPixel, fYPixel, fOldXPixel, fOldYPixel,
    //                    TVirtualX::kHollow);
    return;
  case kButton1Up:
    f3DView->GetCanvas()->FeedbackMode(kFALSE);
    if (px == fXPixel || py == fYPixel) return;
    f3DView->AbsPixeltoXY(px, py, fX1, fY1);
    if (fX1 < fX0) std::swap(fX0, fX1);
    if (fY1 < fY0) std::swap(fY0, fY1);
    Double_t u0, v0, du, dv;
    f3DView->GetView()->GetWindow(u0, v0, du, dv);
    Double_t ndu = du * (fX1 - fX0) / 2;
    Double_t ndv = dv * (fY1 - fY0) / 2;
    Double_t nu0 = u0 + du * (fX0 + 1) / 2;
    Double_t nv0 = v0 + dv * (fY0 + 1) / 2;
    Verbose(0, "ExecuteEvent", "was (%f,%f)x(%f,%f), now (%f,%f)x(%f,%f)",
	    u0, v0, du, dv, nu0, nv0, ndu, ndv);
    f3DView->GetView()->SetWindow(nu0, nv0, ndu, ndv);
    f3DView->GetView()->DefinePerspectiveView();
    f3DView->Modified();
    return;
  }
}

//____________________________________________________________________
Int_t
Simulation::Display::DistancetoPrimitive(Int_t px, Int_t)
{
  // AliInfo(Form("@ (%d,%d)", px, py));
  f3DView->SetCursor(kCross);
  Float_t xmin = f3DView->GetX1();
  Float_t xmax = f3DView->GetX2();
  Float_t dx   = .02 * (xmax - xmin);
  Float_t x    = f3DView->AbsPixeltoX(px);
  if (x < xmin + dx || x > xmax - dx) return 9999;
  return (fZoomMode ? 0 : 7);
}


//____________________________________________________________________
void
Simulation::Display::SetT(Double_t min, Double_t max) 
{
  fMinT = min; 
  fMaxT = max;
}

//____________________________________________________________________
void
Simulation::Display::DrawMan(Double_t x0, Double_t y0, 
			     Double_t z0, Int_t color) const
{
  Double_t height = 180;
  TVector3 head(20, 25, 20);
  TVector3 neck(15, 8, 15);
  TVector3 torso(40, 66, 30);
  TVector3 upperArm(8,36,8);
  TVector3 lowerArm(8,30,8);
  TVector3 thigh(15,46,15);
  TVector3 shin(12,30,12);
  TVector3 foot(10,5,25);
  Double_t degrad = TMath::Pi()/180.;
  
  Double_t x = x0;
  Double_t y = y0 + height-head.Y()/2;
  Double_t z = z0;
  TMarker3DBox* headB = 
    new TMarker3DBox(x, y, z,head.X()/2,head.Y()/2,head.Z()/2,10,2);
  headB->SetLineColor(color);
  headB->Draw();

  y -= head.Y()/2+neck.Y()/2;
  TMarker3DBox* neckB = 
    new TMarker3DBox(x,y,z, neck.X()/2,neck.Y()/2,neck.Z()/2,0,0);
  neckB->SetLineColor(color);
  neckB->Draw();

  Double_t y1 = y - neck.Y() - TMath::Cos(30*degrad)*upperArm.Y()/2;
  Double_t x1 = ((torso.X()+upperArm.X())/2+
		 TMath::Sin(30*degrad)*upperArm.Y()/2);
  TMarker3DBox* upperArmB1 = 
    new TMarker3DBox(x0-x1,y1,z, upperArm.X()/2,upperArm.Y()/2,upperArm.Z()/2,
		     0,-30);
  upperArmB1->SetLineColor(color);
  upperArmB1->Draw();
  TMarker3DBox* upperArmB2 = 
    new TMarker3DBox(x0+x1,y1,z, upperArm.X()/2,upperArm.Y()/2,upperArm.Z()/2,
		     0,30);
  upperArmB2->SetLineColor(color);
  upperArmB2->Draw();

  y1 -= upperArm.Y()/2 + lowerArm.Y()/2;
  TMarker3DBox* lowerArmB1 = 
    new TMarker3DBox(x0-x1,y1,z, lowerArm.X()/2,lowerArm.Y()/2,lowerArm.Z()/2,
		     0,30);
  lowerArmB1->SetLineColor(color);
  lowerArmB1->Draw();
  x1 += lowerArm.X();
  TMarker3DBox* lowerArmB2 = 
    new TMarker3DBox(x0+x1,y1,z, lowerArm.Z()/2,lowerArm.X()/2,lowerArm.Y()/2,
		     -80,80);
  lowerArmB2->SetLineColor(color);
  lowerArmB2->Draw();
  

  y -= neck.Y()/2+torso.Y()/2;
  TMarker3DBox* torsoB = 
    new TMarker3DBox(x, y, z, torso.X()/2,torso.Y()/2,torso.Z()/2,0,0);
  torsoB->SetLineColor(color);
  torsoB->Draw();

  
  y -= torso.Y()/2+thigh.Y()/2;
  x =  (torso.X() - thigh.X())/2;
  TMarker3DBox* thighB1 = 
    new TMarker3DBox(x0-x,y,z, thigh.X()/2,thigh.Y()/2,thigh.Z()/2,0,0);
  thighB1->SetLineColor(color);
  thighB1->Draw();

  TMarker3DBox* thighB2 = 
    new TMarker3DBox(x0+x,y,z,thigh.Z()/2,thigh.X()/2,thigh.Y()/2,80,-80);
  thighB2->SetLineColor(color);
  thighB2->Draw();


  y -= thigh.Y()/2+shin.Y()/2;
  TMarker3DBox* shinB1 = 
    new TMarker3DBox(x0-x,y,z, shin.X()/2,shin.Y()/2,shin.Z()/2,0,0);
  shinB1->SetLineColor(color);
  shinB1->Draw();
  x += TMath::Sin(10*degrad)*thigh.X();
  z += TMath::Sin(10*degrad)*thigh.Z();
  TMarker3DBox* shinB2 = 
    new TMarker3DBox(x0+x,y,z, shin.X()/2,shin.Y()/2,shin.Z()/2,0,0);
  shinB2->SetLineColor(color);
  shinB2->Draw();

  y -= shin.Y()/2+foot.Y()/2;
  x =  (torso.X() - thigh.X())/2;
  z = z0 + foot.Z()/2 - shin.Z()/2;
  TMarker3DBox* footB1 = 
    new TMarker3DBox(x0-x,y,z, foot.X()/2,foot.Y()/2,foot.Z()/2,0,0);
  footB1->SetLineColor(color);
  footB1->Draw();
  x += TMath::Sin(10*degrad)*thigh.X();
  z += TMath::Sin(10*degrad)*thigh.X();
  TMarker3DBox* footB2 = 
    new TMarker3DBox(x0+x,y,z, foot.X()/2,foot.Y()/2,foot.Z()/2,0,0);
  footB2->SetLineColor(color);
  footB2->Draw();
}
    
//____________________________________________________________________
//
// EOF
//
