// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Hit.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Simulation::Hit
*/
#ifndef Simulation_Hit
#define Simulation_Hit
#include <TObject.h>
#include <TAttMarker.h>
class TMarker3DBox;

namespace Simulation 
{
  /** @defgroup sim_data Data classes. 
      @ingroup sim_steer 
      @brief Code in this group deals with the data */
  /** @class Hit simulation/Hit.h <Simulation/Hit.h>
      @ingroup sim_data
      @brief Base for hit structure. 
   */
  class Hit : public TObject, public TAttMarker 
  {
  public:
    /** Constructor */
    Hit();
    /** Copy constructor 
	@param other Hit to copy from  */
    Hit(const Hit& other);
    /** Destructor */ 
    virtual ~Hit() {}
    /** Assignment operator 
	@param other Hit to assign from
	@return Reference to this object */
    Hit& operator=(const Hit& other);
    
    /** @return Name of object */
    virtual const char* GetName() const { return ""; }
    /** @return Title of object */
    virtual const char* GetTitle() const { return ""; }
    /** Draw this hit.  
	@param option Not used */
    virtual void Draw(Option_t* option="");
    /** Get 3D marker */
    virtual TObject* Marker(Option_t* option="") { return 0; }
    /** Get time of the hit in seconds */
    virtual Double_t T() const { return -999; }
  protected:
    virtual TMarker3DBox* Marker3D(Double_t  x,
				   Double_t  y,
				   Double_t  z,
				   Double_t  dx,
				   Double_t  dy,
				   Double_t  dz,
				   Double_t  theta,
				   Double_t  phi,
				   Option_t* option="");

    ClassDef(Hit,2);
  };
}
#endif
//____________________________________________________________________
//
// EOF
//
