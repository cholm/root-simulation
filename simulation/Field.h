// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/Field.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Field
*/
#ifndef Simulation_Field
#define Simulation_Field
#include <TNamed.h>
#include <TVector3.h>

namespace Simulation 
{
  
  /** @class Field simulation/Field.h <simulation/Field.h>
      @ingroup sim_steer
      @brief A magnetic field configuration.

      Objects of this type can be attached to a task or the main task.
      If a task associated with the current tracking medium has an
      object of this kind, then it will give the current magnetic
      field in that object.  Otherwise, the resolution of the magnetic
      field at the current point is delegated to its parent task,
      eventually ending at the main task, which may then give a
      magnetic field.
  */
  class Field : public TObject 
  {
  public:
    /** Construct from vector */
    Field(const TVector3& b) : fB(b) {}
    /**
     * Default constructor
     */
    Field(Double_t bx=0, Double_t by=0, Double_t bz=0) : fB(bx,by,bz) {}
    
    /**
     * Return the magnetic field at position @a pos
     *
     * @param pos Current global position to evaluate field at
     * @param b   Vector to return B-field in 
     */
    virtual void B(const TVector3& pos, TVector3& b)
    {
      b = fB;
    }
    /**
     * Return the magnetic field at position @a pos
     *
     * @param pos Current global position to evaluate field at
     *
     * @return The B-field as a 3-vector
     */
    virtual TVector3 B(const TVector3& pos)
    {
      TVector3 b;
      B(pos,b);
      return b;
    }
  protected:
    TVector3 fB;
    
    ClassDef(Field,1);
  };
}
#endif
//
// EOF
//


