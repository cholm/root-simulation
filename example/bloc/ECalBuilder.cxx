//____________________________________________________________________ 
//  
// $Id: ECalBuilder.cxx,v 1.1 2005-12-13 18:56:08 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/ECalBuilder.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Example::ECalBuilder
*/
#include "ECalBuilder.h"
#include <TGeoVolume.h>
#include <TGeoTube.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TVirtualMC.h>
#include <TFolder.h>

//____________________________________________________________________
ClassImp(Example::ECalBuilder);

//____________________________________________________________________
Example::ECalBuilder::ECalBuilder(Bool_t useBgo) 
  : Framework::Task("ECalBuilder", "The ECalBuilder"), fUseBgo(useBgo)
{
  fCache = 0;
}

//____________________________________________________________________
void
Example::ECalBuilder::Initialize(Option_t* option) 
{
  Debug(14, "Initialize", "Setting up materials");
  Framework::Task::Initialize(option);

  // Since we will use the radiation length of our aborber material as
  // a parameter to decide on the size of elements, we need tp create
  // that medium up-front.
  TGeoMixture* bgo = new TGeoMixture("BGO", 3, 7.1);
  bgo->DefineElement(0, 208.98,  83, .1818);
  bgo->DefineElement(1,  72.59,  32, .3636);
  bgo->DefineElement(2,  15.999,  8, .4546);
  
  TGeoMixture* leadGlass = new TGeoMixture("Lead Glass", 6, 5.2);
  leadGlass->DefineElement(0, 207.19,  82, .65994);
  leadGlass->DefineElement(1,  39.102, 19, .00799);
  leadGlass->DefineElement(2,  28.088, 14, .126676);
  leadGlass->DefineElement(3,  22.99,  11, .0040073);
  leadGlass->DefineElement(4,  15.999,  8, .199281);
  leadGlass->DefineElement(5,  74.922, 33, .00200485);
    
  TGeoMaterial* material = (fUseBgo ? bgo : leadGlass);
  material->SetFillColor(kCyan);
  
  TGeoMixture* ecalAir = new TGeoMixture("Air", 4, .00120479);
  ecalAir->DefineElement(0, 12.0107,  6., 0.000124);
  ecalAir->DefineElement(1, 14.0067,  7., 0.755267);
  ecalAir->DefineElement(2, 15.9994,  8., 0.231781);
  ecalAir->DefineElement(3, 39.948,  18., 0.012827);
  ecalAir->SetFillColor(kRed);

  // Parameters are 
  //	0	isvol	Is active (1) or not (0)
  //	1	ifield	Type of magnetic field 
  //	2	fieldm	Maximum magnetic field
  //	3	tmaxfd	Maxium deflection due to magnetic field
  //	4	stemax	Maximum step value
  //	5	deemax	Maximum energy loss per step
  //	6	epsil	Precision
  //	7	stmin	Minimum step value 
  //    8       -----   Ignored 
  //    9       -----   Ignored 
  
  Double_t      absPar[]   = { 0, 0, 0, 10., 1000., .05, .001, .001, 0, 0 };
  TGeoMedium*   absorber   = new TGeoMedium("Absorber", 1, material,  absPar);
  Double_t      airPar[]   = { 1., 0., 1., 1., .001, 1., .001, .001, .001,0,0 };
  TGeoMedium*   air        = new TGeoMedium("Air", 1, ecalAir,  airPar);

  Double_t      radl       = material->GetRadLen();
  TGeoTube*     ecalShape  = new TGeoTube("ECAL",0,21*radl/4,11*radl);
  TGeoVolume*   ecalVolume = new TGeoVolume("ECAL",ecalShape, air);
  ecalVolume->SetVisContainers(true);
  gGeoManager->SetTopVolume(ecalVolume);
}


//____________________________________________________________________
//
// EOF
//
