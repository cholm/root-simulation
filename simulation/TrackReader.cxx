//____________________________________________________________________ 
//  
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/TrackReader.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for TrackReader
*/
#include <simulation/TrackReader.h>
#include <TTree.h>
#include <TParticle.h>
#include <TClonesArray.h>
#include <TFolder.h>
#include <map>

//____________________________________________________________________
ClassImp(Simulation::TrackReader);

//____________________________________________________________________
Simulation::TrackReader::TrackReader(const char* name,
				     const char* branchName,
				     const char* treeName) 
  : Framework::Task(name, "TParticle"),
    fBranchName(branchName),
    fTreeName(treeName),
    fParticles(0),
    fMap(0),
    fTracks(0)
{
  fCache  = new TList();
  fMap    = new TClonesArray("TObject");
  fTracks = new TObjArray();
  fTracks->SetOwner(false);
}
//____________________________________________________________________
void
Simulation::TrackReader::Initialize(Option_t* option)
{
  Framework::Task::Initialize(option);
  fFolder->AddFolder("Particles","Read particles",         fParticles);
  fFolder->AddFolder("Map","Map entry to trackNo",         fMap);
  fFolder->AddFolder("Tracks","Array of tracks by trackNo",fTracks);
}
  
//____________________________________________________________________
void
Simulation::TrackReader::Register(Option_t* option) 
{
  if (fTreeName.IsNull()) {
    Fail("Register", "no tree name set");
    return;
  }
  if (fBranchName.IsNull()) {
    Fail("Register", "no branch name set");
    return;
  }

  TTree* tree = 0;
  if (!(tree = GetBaseTree(fTreeName.Data()))) return;

  fBranch = tree->GetBranch(fBranchName);
  if (!fBranch) {
    Fail("Regiter","Couldn't get branch %s from tree %s",
	 fBranchName.Data(), fTreeName.Data());
    return;
  }

  tree->SetBranchAddress(fBranchName.Data(), &fParticles);

  TString  mapName   = Form("%sMap",fBranchName.Data());
  TBranch* mapBranch = tree->GetBranch(mapName.Data());
  if (!mapBranch) {
    Warning("Register","No map branch: \"%s\"",mapName.Data());
    return;
  }

  tree->SetBranchAddress(mapName.Data(), &fMap);
}

//____________________________________________________________________
void
Simulation::TrackReader::Exec(Option_t* option)
{
  // Clear the mapped collection 
  fTracks->Clear();

  // Get number of real tracks, and make mapped collection that size
  Int_t nTrack = fMap->GetEntriesFast();
  fTracks->Expand(nTrack);

  // Loop over the mapping and get the real index into fCache.  Then
  // if the index is valid (not 0xFFFFFFFF), set the mapped entry to
  // point to the real particle.
  for (Int_t i = 0; i < nTrack; i++) {
    UInt_t idx = fMap->At(i)->GetUniqueID();
    if (idx == 0xFFFFFFFF) continue;

    fTracks->AddAt(fParticles->At(idx),i);
  }
}
  
//____________________________________________________________________
//
// EOF
//
