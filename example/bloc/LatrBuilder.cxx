//____________________________________________________________________ 
//  
// $Id: LatrBuilder.cxx,v 1.1 2005-12-13 18:56:09 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/LatrBuilder.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Example::LatrBuilder
*/
#include "LatrBuilder.h"
#include <TGeoVolume.h>
#include <TGeoTube.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TTree.h>
#include <TVirtualMC.h>
#include <TFolder.h>

//____________________________________________________________________
ClassImp(Example::LatrBuilder);

//____________________________________________________________________
Example::LatrBuilder::LatrBuilder() 
  : Framework::Task("LatrBuilder", "Time Projection Chamber")
{}

//____________________________________________________________________
void
Example::LatrBuilder::Initialize(Option_t* option) 
{
  Debug(14, "Register", "Setting up volume");

  TGeoMedium*   medium     = gGeoManager->GetMedium("Air");
  TGeoMedium*   absorber   = gGeoManager->GetMedium("Absorber");
  TGeoMaterial* material   = absorber->GetMaterial();
  Double_t      radl       = material->GetRadLen();
  TGeoTube*     latrShape  = new TGeoTube("LATR", 20*radl/4,21*radl/4,10*radl);
  TGeoVolume*   latrVolume = new TGeoVolume("LATR", latrShape, medium);
  TGeoVolume*   top        = gGeoManager->GetTopVolume();
  top->AddNode(latrVolume, 0, 0);
}


//_____________________________________________________________________
//
// EOF
//
