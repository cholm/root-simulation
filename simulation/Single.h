// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
//
/** @file    simulation/Single.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Single
*/
#ifndef Simulation_Single
#define Simulation_Single
#include <TGenerator.h>
#include <TLorentzVector.h>
class TClonesArray;
class TObjArray;
class TParticlePDG;

namespace Simulation 
{
  
  /** @class Single simulation/Single.h <simulation/Single.h>
      @ingroup sim_gen
      @brief A simple event generator that generates single particles of
      a fixed type, with a fixed transverse momentum, @f$ \theta@f$, 
      and @f$ \phi@f$. 
  */
  class Single : public TGenerator 
  {
  public:
    /** CTOR */
    Single();
    /** Set the particle type identifier - default to pion
	@param pdg Particle type ID code */
    void SetPdgCode(Int_t pdg=211); //*MENU*
    /** Set the particle type by name 
	@param pdg Particle type name  */
    void SetPdg(const char* pdg="pi+"); //*MENU*
    /** Get the particle type identifier 
	@return Get the PDG code */
    Int_t GetPdgCode() const { return fPdgCode; }
    /** Set the polar angle 
	@param x @f$ \theta@f$  */
    void SetTheta(Double_t x=90); //*MENU*
    /** Get the polar angle 
	@return Get @f$ \theta @f$ */
    Double_t GetTheta() const { return fP.Theta(); }
    /** Set the azimuthal angle 
	@param x @f$ \phi@f$  */
    void SetPhi(Double_t x=0); //*MENU*
    /** Get the azimuthal angle 
	@return Get @f$ \phi @f$ */
    Double_t GetPhi() const { return fP.Phi(); }
    /** Set the transverse momentum in GeV/c
	@param x @f$ p_\perp@f$  */
    void SetPt(Double_t x=3); //*MENU*
    /** Get the transverse momentum in GeV/c
	@return Get @f$ p_\perp @f$ */
    Double_t GetPt() const { return fP.Pt(); }
    /** Set the momentum in GeV/c
	@param p Size of momentum vector */ 
    void SetP(Double_t p); //*MENU*
    /** Set the momentum in GeV/c
	@param px X-Momentum 
	@param py Y-Momentum
	@param pz Z-Momentum */ 
    void SetP(Double_t px, Double_t py, Double_t pz); //*MENU*
    /** Set the rapidity 
	@param y Rapidity 
	(@f$ y = 1/2 \log\left(\frac{E+p_z}{E-p_z}\right)@f$) */
    void SetY(Double_t y); //*MENU*
    
    /** Generate one event, and return it in @a a 
	@param a Return array 
	@param option Not used 
      @return  # of particles made */
    Int_t ImportParticles(TClonesArray* a, Option_t* option="");
    /** Generate one event, and return it
	@param option Not used 
	@return  particles made */
    TObjArray* ImportParticles(Option_t* option="");
    /** Get the mass in GeV/c2 of the PDG definition */
    Double_t GetMass() const;
    /** Get PDG particle */
    TParticlePDG* GetPdgParticle() const;
  private:
    /** Particle type to make */
    Int_t    fPdgCode;
    /** Vector that holds the momentum */ 
    TLorentzVector fP;
    
    ClassDef(Single, 0);
  };
}
#endif
//
// EOF
//
