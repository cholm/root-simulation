// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
// $Id: BlocSimulator.h,v 1.1 2005-12-13 18:56:08 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/BlocSimulator.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Example::BLOC
*/
#ifndef Example_BlocSimulator
#define Example_BlocSimulator
#include <simulation/Task.h>

namespace Example 
{
  /** This task takes care of storing hits in the tracker.  The
      absorber tracking medium and inner most volume are registered as
      belonging to this object (even if created elsewhere). */ 
  class BlocSimulator : public Simulation::Task 
  {
  public:
    /** Construct a BLOC task */
    BlocSimulator();
    /** Initialize the task.
	@param option Not used. */
    void Initialize(Option_t* option="");
    /** Register the task.
	@param option Not used. */
    void Register(Option_t* option="");
    /** Execute the task.
	@param option Not used. */
    void Step();
    /** Finish the event.
	@param option Not used. */
    void FinishEvent();
    ClassDef(BlocSimulator,0) //
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
