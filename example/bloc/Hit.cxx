//____________________________________________________________________ 
//  
// $Id: Hit.cxx,v 1.4 2005-12-05 15:19:17 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/Hit.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Example::Hit
*/
#include "Hit.h"
#include <TMath.h>
#include <TMarker3DBox.h>
#include <TROOT.h>
#include <TDatabasePDG.h>
#include <TString.h>
#include <iostream>
#include <iomanip>

//____________________________________________________________________
ClassImp(Example::Hit);

//____________________________________________________________________
void
Example::Hit::Print(Option_t* option) const
{
  std::cout << "Example::Hit: (" << fRing << "," << fTube << "): " 
	    << fEnergyLoss << "MeV" << std::endl;
  TString opt(option);
  if(opt.Contains("P", TString::kIgnoreCase)) {
    TDatabasePDG* pdgDb = TDatabasePDG::Instance();
    TParticlePDG* pdgP  = pdgDb->GetParticle(fPdg);
    std::cout << "  Particle: " << pdgP->GetName() 
	      << " (track # " << fNTrack << ") x=[" 
	      << std::setw(10) << X() << "," 
	      << std::setw(10) << Y() << "," 
	      << std::setw(10) << Z() << "," 
	      << std::setw(10) << T() << "] p=[" 
	      << std::setw(10) << Px() << "," 
	      << std::setw(10) << Py() << "," 
	      << std::setw(10) << Pz() << "," 
	      << std::setw(10) << E() << "]" << std::endl;
  }
}

    
//____________________________________________________________________
//
// EOF
//
