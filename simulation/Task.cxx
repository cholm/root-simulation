//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Task.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Simulation::Task
*/
#include "simulation/Task.h"
#include "simulation/Main.h"
#include "simulation/Application.h"
#include "simulation/DefaultHit.h"
#include <vmc/TVirtualMC.h>
#include <TGeoMedium.h>
#include <TGeoVolume.h>
#include <TClonesArray.h>
#include <TParticle.h>
#include <TBrowser.h>
#include <TFolder.h>
#include <TBranch.h>
#include <TROOT.h>

//____________________________________________________________________
ClassImp(Simulation::Task);

//____________________________________________________________________
Simulation::Task::Task(const Char_t* name, const Char_t* title) 
  : Framework::Task(name, title), fActive(kTRUE), fField(0)
{}

//____________________________________________________________________
Bool_t 
Simulation::Task::RegisterMedium(TGeoMedium* medium)
{
  Simulation::Main* main = Simulation::Main::Instance();
  return main->GetApplication()->RegisterMedium(medium,this);
}

//____________________________________________________________________
Bool_t 
Simulation::Task::RegisterMedium(Int_t medium)
{
  Simulation::Main* main = Simulation::Main::Instance();
  return main->GetApplication()->RegisterMedium(medium,this);
}

//____________________________________________________________________
void
Simulation::Task::RegisterVolume(Int_t id)
{
  Debug(10,"RegisterVolume","Register volume with id %d", id);
  fSensitive.Set(fSensitive.fN+1);
  fSensitive[fSensitive.fN-1] = id;
}

//____________________________________________________________________
void
Simulation::Task::RegisterVolume(TGeoVolume* volume)
{
  Debug(10,"RegisterVolume","Register volume %s with id %d",
	volume->GetName(), volume->GetNumber());
  RegisterVolume(volume->GetNumber());
}

//____________________________________________________________________
Bool_t
Simulation::Task::IsSensitive(Int_t vol, Bool_t idIsMC) 
{
  Simulation::Main* main = Simulation::Main::Instance();
  Int_t query = idIsMC ? main->GetApplication()->MCVol2Geo(vol) : vol;
  for (Int_t i = 0; i < fSensitive.fN; i++) 
    if (fSensitive[i] == query) return kTRUE;
  return kFALSE;
}

//____________________________________________________________________
void
Simulation::Task::CreateDefaultHitArray() 
{
  if (fCache) {
    Warning("CreateDefaultHitArray", "Cache already initialised");
    return;
  }
  fCache = new TClonesArray("Simulation::DefaultHit");
}

//____________________________________________________________________
Simulation::DefaultHit*
Simulation::Task::CreateDefaultHit(Bool_t keepParticle,
				   Bool_t keepParents) 
{
  if (!fCache) {
    Warning("CreateDefaultHit", "Cache not initialised");
    return 0;
  }
  
  TVirtualMC*   mc     = TVirtualMC::GetMC();
  TClonesArray* hits   = static_cast<TClonesArray*>(fCache);
  Int_t         n      = hits->GetEntriesFast();
  Int_t         nTrack = mc->GetStack()->GetCurrentTrackNumber();
  Int_t         pdg    = mc->TrackPid();
  Double_t      edep   = mc->Edep();
  TLorentzVector v, p;
  mc->TrackPosition(v);
  mc->TrackMomentum(p);
  DefaultHit*  hit     = new ((*hits)[n]) DefaultHit(nTrack, pdg, v, p, edep);
  hit->SetTitle(Form("%s: %f MeV", GetName(), edep));

  mc->GetStack()->GetCurrentTrack()->SetBit(kKeepBit,       keepParticle);
  mc->GetStack()->GetCurrentTrack()->SetBit(kKeepParentsBit,keepParents);
  
  return hit;
}

//____________________________________________________________________
void
Simulation::Task::Exec(Option_t* option) 
{
  if (option && option[0] != '\0') {
    switch (option[0]) {
    case kDefineParticles:
      Debug(34, "Exec", "Sending message DefineParticles() to self (%s)", 
	    GetName());
      this->DefineParticles();
      break;
    case kGeneratePrimaries:
      Debug(34, "Exec", "Sending message GeneratePrimaries() to self (%s)", 
	    GetName());
      this->GeneratePrimaries();
      break;
    case kBeginEvent:
      Debug(34, "Exec", "Sending message BeginEvent() to self (%s)", 
	    GetName());
      this->BeginEvent();
      break;
    case kBeginPrimary:
      Debug(34, "Exec", "Sending message BeginPrimary() to self (%s)", 
	    GetName());
      this->BeginPrimary();
      break;
    case kBeginTrack:
      Debug(34, "Exec", "Sending message BeginTrack() to self (%s)", 
	    GetName());
      this->BeginTrack();
      break;
    case kStep:
      Debug(34, "Exec", "Sending message Step() to self (%s)", 
	    GetName());
      this->Step();
      break;
    case kFinishTrack:
      Debug(34, "Exec", "Sending message FinishTrack() to self (%s)", 
	    GetName());
      this->FinishTrack();
      break;
    case kFinishPrimary:
      Debug(34, "Exec", "Sending message FinishPrimary() to self (%s)", 
	    GetName());
      this->FinishPrimary();
      break;
    case kFinishEvent:
      Debug(34, "Exec", "Sending message FinishEvent() to self (%s)", 
	    GetName());
      this->FinishEvent();
      break;
    case kField: 
      {
	const TVector3& x = 
	  Simulation::Main::Instance()->GetApplication()->GetFieldX();
	TVector3 b;
	Field(x, b);
	Simulation::Main::Instance()->GetApplication()->SetFieldB(b);
      }
      break;
    default:
      break;
    }
  }
}

//____________________________________________________________________
void
Simulation::Task::Field(const TVector3& pos, TVector3& b)
{
  // Check if we have a field 
  if (fField) {
    // If so, use it. 
    fField->B(pos,b);
    return;
  }
  // Check if we have a parent task
  Task* ptask = dynamic_cast<Task*>(fParent);
  if (ptask) {
    // If so, ask the parent task for the field 
    ptask->Field(pos,b);
    return;
  }
  // If parent is Main, ask that to resolve the field
  Simulation::Main::Instance()->Field(pos, b);
}

//____________________________________________________________________
void
Simulation::Task::BeginEvent()
{
  if (fCache) fCache->Clear();
}

//____________________________________________________________________
void
Simulation::Task::Browse(TBrowser* b) 
{
  if (!b) return;
  b->AddCheckBox(this, IsActive());
  fTasks->Browse(b); // b->Add(fTasks, "Tasks");
  if      (fCache)    b->Add(fCache, "Cache", -1);
  else if (fFolder)   b->Add(fFolder, "Folder", -1);
  if      (fBranch)   b->Add(fBranch);
}

//____________________________________________________________________
void
Simulation::Task::SetActive(Bool_t on)
{
  fActive = on;
  TSeqCollection *brlist = gROOT->GetListOfBrowsers();
  TIter next(brlist);
  TBrowser *browser = 0;
  while ((browser=(TBrowser*)next())) {
    browser->CheckObjectItem(this, on);
    browser->Refresh();
  }
}


//____________________________________________________________________
// 
// EOF
//
