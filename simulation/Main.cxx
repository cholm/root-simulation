//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Main.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Simulation::Main
*/
#include "simulation/Main.h"
#include <simulation/Application.h>
#include <TBrowser.h>
#include <TFolder.h>
#include <TBranch.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TEnv.h>
#include <TGeoManager.h>
#ifndef DATA_DIR
# define DATA_DIR = "."
#endif
#ifndef INC_DIR
# define INC_DIR = "."
#endif

//____________________________________________________________________
void SetupPaths(Simulation::Main* main)
{
  main->Verbose(0,"Main","Adding \"%s\" and \"%s/Geant4\" to macro path",
		DATA_DIR,INC_DIR);
  gROOT->SetMacroPath(Form("%s:%s:%s/Geant4",
			   gROOT->GetMacroPath(),
			   DATA_DIR,
			   INC_DIR));
  // Verbose(0,"Main","Adding \"%s/Geant4\" to include path",INC_DIR);
  // gSystem->AddIncludePath(Form("-I%s/Geant4",INC_DIR));
  // gEnv->SetValue("ACLiC.IncludePaths",
  // 		 Form("%s -I%s/Geant4",
  // 		      gEnv->GetValue("ACLiC.IncludePaths",""),
  // 		      INC_DIR));
}

//____________________________________________________________________
ClassImp(Simulation::Main);

//____________________________________________________________________
Simulation::Main* 
Simulation::Main::Instance() 
{
  return static_cast<Simulation::Main*>(fgInstance);
}

//____________________________________________________________________
Simulation::Main::Main() 
  : Framework::Main("main", "Simulation main task"),
    fApplication(0),
    fIsInitialized(kFALSE),
    fField(0),
    fVMCConfig("G3Config.C"), // Default 
    fMoreConfig("MoreConfig.C") // Default
{
  fApplication   = new Application(GetName(), GetTitle());
  fgInstance     = this;
  SetupPaths(this);
}

//____________________________________________________________________
Simulation::Main::Main(const Char_t* name, const Char_t* title) 
  : Framework::Main(name, title),
    fApplication(0),
    fIsInitialized(kFALSE),
    fField(0), 
    fVMCConfig("G3Config.C"), // Default 
    fMoreConfig("MoreConfig.C") // Default
{
  fApplication   = new Application(name, title);
  fgInstance     = this;
  SetupPaths(this);
}

//____________________________________________________________________
void
Simulation::Main::MoreConfig(TVirtualMC* mc)
{
  if (fMoreConfig.IsNull()) {
    Warning("MoreConfig", "No backend post-script defined");
    return;
  }

  Debug(15,"MoreConfig","Applying %s to VMC backend %p",
	fMoreConfig.Data(), mc);
  Int_t err = 0;
  gROOT->Macro(Form("%s((TVirtualMC*)%p)",fMoreConfig.Data(),mc),&err);
  if (err != 0)
    Fatal("MoreConfig", "Failed to execute %s",fMoreConfig.Data());
}

//____________________________________________________________________
void
Simulation::Main::ExecInitialize(Option_t* option) 
{
  Debug(15,"ExecInitialize", "Initialising main task");
  if (fIsInitialized) {
    Warning(GetName(), "Already initialized");
    return;
  }
  if (fVMCConfig.IsNull()) {
    Warning(GetName(), "No VMC configuration set");
    return;
  }
#if ROOT_VERSION_CODE >= ROOT_VERSION(6, 22, 8)
  if (TGeoManager::GetDefaultUnits() != TGeoManager::kRootUnits) 
    Fatal("ExecInitialize", "TGeo units are NOT cm,s,GeV - will fail!");
#elif ROOT_VERSION_CODE >= ROOT_VERSION(6, 18, 2)
  if (TGeoManager::GetDefaultUnits() != TGeoManager::kRootUnits) 
    Warning("ExecInitialize","TGeo units are NOT cm,s,GeV - may well fail!");
#endif

  Debug(15,GetName(),"Initialize all tasks");
  Framework::Main::ExecInitialize(option);
  
  Debug(15,GetName(), "Running VMC configuration macro %s",fVMCConfig.Data());
  gROOT->Macro(fVMCConfig.Data());// G4 calls ConstructGeometry, G3 does not
  
  Debug(15,"ExecInitialize", "Initialising application");
  fApplication->Init();
  fIsInitialized = kTRUE;

}
//____________________________________________________________________
void
Simulation::Main::Field(const TVector3& pos, TVector3& b)
{
  if (fField) {
    fField->B(pos,b);
    return;
  }
  b.SetXYZ(0,0,0);
}

//____________________________________________________________________
void
Simulation::Main::Event() 
{
  Char_t option[] = { Simulation::Task::kBeginEvent, '\0' };
  ExecuteTask(option);
  fApplication->Event();
  option[0] = Simulation::Task::kFinishEvent;
  ExecuteTask(option);
}

//____________________________________________________________________
void
Simulation::Main::Trigger()
{
  if (!fIsInitialized) {
    ExecInitialize();
    if (fStatus >= kStop) return;
  }
  Run(1);
}


//____________________________________________________________________
void
Simulation::Main::Run(Int_t n) 
{
  Bool_t ret = fApplication->Run(n);
  if (!ret) fStatus = kStop;
}

//____________________________________________________________________
Int_t
Simulation::Main::Loop(Int_t n) 
{
  ExecInitialize();
  if (fStatus < kStop) {
    Print("TBVDFCM");
    Run((n < 1 ? 1 : n));
    if (fStatus < kStop) 
      ExecFinish();
  }
  return (fStatus <= kStop ? 0 : fStatus);
}

//____________________________________________________________________
void
Simulation::Main::Print(Option_t* option) const 
{
  Framework::Main::Print(option);  
  if (fApplication) fApplication->Print(option);
}

//____________________________________________________________________
void
Simulation::Main::Browse(TBrowser* b)
{
  if (!b) return;
  // Framework::Main::Browse(b);
  fTasks->Browse(b);
  if      (fCache)       b->Add(fCache, "Cache");
  else if (fFolder)      b->Add(fFolder, "Folder");
  if      (fBranch)      b->Add(fBranch);  
  if      (fApplication) b->Add(fApplication, "MC Application");
}

//____________________________________________________________________
void
Simulation::Main::Draw(Option_t* option, Double_t theta, Double_t phi, 
		       Double_t psi, Double_t u0, Double_t v0, 
		       Double_t ul, Double_t vl) 
{
  if (!fIsInitialized) {
    ExecInitialize();
    if (fStatus >= kStop) return;
  }
  
  if (!fApplication) return;
  fApplication->Draw(option, theta, phi, psi, u0, v0, ul, vl);
}

//____________________________________________________________________
//
// EOF
//
