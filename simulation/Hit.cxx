//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/Hit.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Simulation::Hit
*/
#include "simulation/Hit.h"
#include <TMath.h>
#include <TMarker3DBox.h>

//____________________________________________________________________
ClassImp(Simulation::Hit);

//____________________________________________________________________
Simulation::Hit::Hit() 
  : TAttMarker(kWhite, kFullCircle, 1)
{}

//____________________________________________________________________
Simulation::Hit::Hit(const Hit& other) 
  : TAttMarker(other.fMarkerColor, other.fMarkerStyle, other.fMarkerSize)
{}

//____________________________________________________________________
Simulation::Hit&
Simulation::Hit::operator=(const Hit& other) 
{
  fMarkerColor = other.fMarkerColor;
  fMarkerStyle = other.fMarkerStyle;
  fMarkerSize  = other.fMarkerSize;
  return *this;
}

//____________________________________________________________________
void
Simulation::Hit::Draw(Option_t* option)
{
  TObject* marker = Marker(option);
  if (!marker) return;
  marker->Draw();
}

//____________________________________________________________________
TMarker3DBox*
Simulation::Hit::Marker3D(Double_t  x,
			  Double_t  y,
			  Double_t  z,
			  Double_t  dx,
			  Double_t  dy,
			  Double_t  dz,
			  Double_t  theta,
			  Double_t  phi,
			  Option_t* option)
{
  TMarker3DBox* marker = new  TMarker3DBox(x,y,z,dx,dy,dz,theta,phi);
  marker->SetLineColor(GetMarkerColor());
  marker->SetRefObject(this);
  // marker->Draw(option);
  return marker;
}

    
//____________________________________________________________________
//
// EOF
//
