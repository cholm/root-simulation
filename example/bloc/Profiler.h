// -*- mode: c++ -*- 
//____________________________________________________________________ 
//  
// $Id: Profiler.h,v 1.5 2005-12-01 14:48:52 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef Example_Profiler
#define Example_Profiler
#include <framework/Task.h>
class TH1F;

namespace Example 
{
  /** @class Profiler example/Profiler.h  <example/Profiler.h>
      @brief Framework::Task to make profile histograms of the
      particle showers. 

      Use this with a normal Framework::Main instance, like 
      @dontinclude example/Profile.C
      @skip Profile()
      @until }
   */
  class Profiler : public Framework::Task
  {
  private:
    /** Number of rings */
    Int_t fNL;
    /** Number of tubes */
    Int_t fNR;
    /** Total energy loss in calorimeter */
    Float_t fTotalEnergyLoss;
    /** Number of events processed */
    Int_t fN;
    /** Histogram of Longitudinal profile */
    TH1F* fLongitudinalProfile;
    /** Histogram of Longitudinal cumulative profile */
    TH1F* fCumulativeLongitudinalProfile;
    /** Histogram of Radial profile */
    TH1F* fRadialProfile;
    /** Histogram of Cumulative longitudinal profile */
    TH1F* fCumulativeRadialProfile; 
  public:
    /** Construct a profiler task. 
	@param nR Number of radial segments (tubes)
	@param nL Number of longitudinal segments (rings)  
	@param outfile Output file (default is none) */
    Profiler(Int_t nR=20, Int_t nL=20, const char* outfile="");
    virtual ~Profiler() {}

    /** @param n The number of radial segments (tubes) */
    void SetNR(Int_t n) { fNR = n; }
    /** @param n The number of longitudianl segments (rings) */
    void SetNL(Int_t n) { fNL = n; }

    /** Finish the event.  Loops over all primary particles and hits
	and updates the summed energy for the full event, the rings
	and the tubes. 
	@param option Not used. */
    virtual void Exec(Option_t* option);
    virtual void Register(Option_t* option="") {}
    /** Initialize the task 
	@param option Not used  */
    virtual void Initialize(Option_t* option=""); 
    /** Creates the final summary histograms and optionally displays
	them in a canvas 
	@param option Not used */
    virtual void Finish(Option_t* option=""); 
    
    ClassDef(Profiler,0) // Read Points into TFolder 
  };
}

#endif
//____________________________________________________________________ 
//  
// EOF
//
