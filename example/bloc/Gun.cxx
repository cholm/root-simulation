//____________________________________________________________________ 
//  
// $Id: Gun.cxx,v 1.7 2007-08-05 14:26:34 cholm Exp $ 
//
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    example/bloc/Gun.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Implementation file for Example::Gun
*/
#include "Gun.h"
#include "simulation/Main.h"
#include <TTree.h>
#include <TParticle.h>
#include <TLorentzVector.h>
#include <TDatabasePDG.h>
#include <TFolder.h>
#include <TVirtualMC.h>
#include <TVirtualMCStack.h>
#include <TGeoVolume.h>
#include <TGeoTube.h>
#include <TGeoMaterial.h>
#include <TGeoMedium.h>
#include <TGeoManager.h>
#include <TClonesArray.h>
#include <iostream>

//____________________________________________________________________
ClassImp(Example::Gun);

//____________________________________________________________________
Example::Gun::Gun(const char* pdg, Double_t p, 
		  Double_t vx, Double_t vy, Double_t vz,
		  Double_t phi, Double_t theta) 
  : Simulation::Generator("Gun", "The Gun")
{
  fRandom    = new TRandom;
  fCache     = new TClonesArray("TParticle");
  fSingle    = new Simulation::Single;
  fGenerator = fSingle;
  fSingle->SetPdg(pdg);
  fSingle->SetP(p);
  fSingle->SetTheta(theta);
  fSingle->SetPhi(phi);
  SetVertex(vx, vy, vz, 0);
}

//____________________________________________________________________
Example::Gun::Gun(Int_t pdg, Double_t p, 
		  Double_t vx, Double_t vy, Double_t vz,
		  Double_t phi, Double_t theta) 
  : Simulation::Generator("Gun", "The Gun")
{
  fRandom    = new TRandom;
  fCache     = new TClonesArray("TParticle");
  fSingle    = new Simulation::Single;
  fGenerator = fSingle;
  fSingle->SetPdgCode(pdg);
  fSingle->SetP(p);
  fSingle->SetTheta(theta);
  fSingle->SetPhi(phi);
  SetVertex(vx, vy, vz, 0);
}

//____________________________________________________________________
void
Example::Gun::Register(Option_t* option) 
{
  Debug(14, "Register", "Setting up generator");
  TTree* tree = 0;
  if ((tree = GetBaseTree("Hits"))) 
    fBranch = tree->Branch(GetName(), &fCache);

  TGeoMedium*   absorber   = gGeoManager->GetMedium("Absorber");
  TGeoMaterial* material   = absorber->GetMaterial();
  fX0                      = material->GetRadLen();
  if (fV.X() < 0 && fV.Y() < 0 && fV.Z() < 0) 
    fV.SetXYZT(0, 0, -10 * fX0 + .01, 0);
}

//____________________________________________________________________
//
// EOF
//
