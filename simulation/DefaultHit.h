// -*- mode: C++ -*- 
//____________________________________________________________________ 
//  
//  ROOT generic simulation framework 
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    simulation/DefaultHit.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec  2 01:12:31 2004
    @brief   Declaration file for Simulation::Hit
*/
#ifndef Simulation_DefaultHit
#define Simulation_DefaultHit
#include <TObject.h>
#include <TString.h>
#include <TAttMarker.h>
#include <TLorentzVector.h>
#include <simulation/Hit.h>

namespace Simulation 
{
  /** @defgroup sim_data Data classes. 
      @ingroup sim_steer 
      @brief Code in this group deals with the data */
  /** @class DefaultHit simulation/DefaultHit.h <Simulation/DefaaultHit.h>
      @ingroup data
      @brief A simple hit structure. 
   */
  class DefaultHit : public Hit
  {
  public:
    /** Constructor */
    DefaultHit();
    /** Constructor - create a hit 
	@param nTrack     Track number
	@param pdg        Particle ID
	@param x          X coordinate of hit in cm
	@param y          Y coordinate of hit in cm
	@param z          Z coordinate of hit in cm
	@param t          Time of hit in s
	@param px         Momentum of track in X in GeV/c
	@param py         Momentum of track in Y in GeV/c
	@param pz         Momentum of track in Z in GeV/c
	@param e          Total energy of track in GeV
	@param energyLoss Energy lost by track in GeV */
    DefaultHit(Int_t    nTrack,
	       Int_t    pdg,
	       Double_t x,
	       Double_t y,
	       Double_t z,
	       Double_t t,
	       Double_t px,
	       Double_t py,
	       Double_t pz,
	       Double_t e,
	       Double_t energyLoss);
    /** Create a hit. 
	@param nTrack Track number
	@param pdg    Track particle ID
	@param v      Coordinates of hit
	@param p      Momentum of track
	@param energyLoss Energy lost by track. */
    DefaultHit(Int_t                 nTrack,
	       Int_t                 pdg,
	       const TLorentzVector& v,
	       const TLorentzVector& p,
	       Double_t              energyLoss);
    /** Copy constructor 
	@param other Hit to copy from  */
    DefaultHit(const DefaultHit& other);
    /** Destructor */ 
    virtual ~DefaultHit() {}
    /** Assignment operator 
	@param other Hit to assign from
	@return Reference to this object */
    DefaultHit& operator=(const DefaultHit& other);
    
    /** @return Track number  */
    Int_t NTrack() const { return fNTrack; }
    /** @return Track particle id.  */
    Int_t Pdg() const { return fPdg; }
    /** @return Coordinates of hit  */
    const TLorentzVector& V() const { return fV; }
    /** @return Momentum of track  */
    const TLorentzVector& P() const { return fP; }
    /** @return Energy lost (in GeV) in this step  */
    Double_t EnergyLoss() const { return fEnergyLoss; }
    /** @return Energy lost (in GeV) in this step  */
    Double_t Edep() const { return fEnergyLoss; }
    /** @return The @f$ p_x@f$ (GeV/c) */ 
    Double_t Px() const { return fP.Px(); }
    /** @return The  @f$ p_y@f$ (GeV/c)  */ 
    Double_t Py() const { return fP.Py(); }
    /** @return The @f$ p_z@f$  (GeV/c) */ 
    Double_t Pz() const { return fP.Pz(); }
    /** @return The @f$ E = \sqrt{m^2 + p_x^2 + p_y^2 + p_z^2}@f$  (GeV) */ 
    Double_t E() const { return fP.E(); }
    /** @return The @f$ m = \sqrt{E^2 - p_x^2 - p_y^2 - p_z^2}@f$ (GeV/c2) */ 
    Double_t M() const { return fP.M(); }
    /** @return The @f$ p_\perp = \sqrt{p_x^2 + p_y^2}@f$ (GeV/c) */ 
    Double_t Pt() const { return fP.Pt(); }
    /** @return The @f$ \eta = - \log\left(\tan^1(\theta/2)\right)@f$ */ 
    Double_t Eta() const { return fP.Eta(); }
    /** @return The @f$ y = 1/2 \log\left(\frac{E+p_z}{E-p_z}\right)@f$ */ 
    Double_t Rapidity() const { return fP.Rapidity(); }
    /** @return The @f$ \theta @f$ (Radians) */
    Double_t Theta() const { return fP.Theta(); }
    /** @return The @f$ \phi @f$ (Radians) */
    Double_t Phi() const { return fP.Phi(); }
    /** @return @f$ x@f$ (cm) */
    Double_t X() const { return fV.X(); }
    /** @return @f$ y@f$ (cm) */
    Double_t Y() const { return fV.Y(); }
    /** @return @f$ z@f$ (cm) */
    Double_t Z() const { return fV.Z(); }
    /** @return @f$ t@f$ (s) */
    Double_t T() const { return fV.T(); }
    
    /** Set the name of the object 
	@param name Name */
    void SetName(const char* name) { fName = name; }
    /** @return Name of object */
    virtual const char* GetName() const;
    /** Set the title of the object 
	@param title Title */
    void SetTitle(const char* title) { fTitle = title; }
    /** @return Title of object */
    virtual const char* GetTitle() const;
    /** Draw this hit.  
	@param option Not used */
    virtual TObject* Marker(Option_t* option="");
    /** Print information about hit to standard out. 
	@param option Not used. */
    void Print(Option_t* option="P") const; //*MENU*
  protected:
    /** Track number */
    Int_t   fNTrack;
    /** Track particle id. */
    Int_t   fPdg;
    /** coordinates of hit (cm,cm,cm,s) */
    TLorentzVector fV;
    /** Momentum of track (GeV/c,GeV/c,GeV/c,GeV) */
    TLorentzVector fP;
    /** Energy lost in this step in GeV */
    Double_t fEnergyLoss;
    /** Name */
    mutable TString fName; //!
    /** Name */
    mutable TString fTitle; //!
    
    ClassDef(DefaultHit,3);
  };
}
#endif
//____________________________________________________________________
//
// EOF
//
